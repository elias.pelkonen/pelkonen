using System;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;

namespace InvCommon
{
	/// <summary>
	/// Summary description for CIniConfig.
	/// </summary>
	[Serializable]
	public class CIniConfig : CConfig
	{
		// public event EventHandler ConfigRefresh;

		#region Member Variables

		// private members
		private bool m_bInitialized;
		private bool m_bDisposed = false;
		[NonSerialized]
		private FileSystemWatcher m_Watcher;
		private DateTime m_LastModified;

		protected bool m_bActiveConfig = false;
		protected string m_sIniPath;

		private const string sParseError = "Invalid configuration value: \"{0}\" for key: \"{1}\" in section: \"{2}\"";

		#endregion

		#region Constructors

		public CIniConfig(string Path)
			: this(Path, false)
		{
		}

		public CIniConfig(string Path, bool ActiveConfig)
		{
			m_sIniPath = System.IO.Path.GetFullPath(System.IO.Path.Combine(CDefault.defaultLogLocation, Path));
			m_bActiveConfig = ActiveConfig;
		}

		#endregion

		#region Overridden Accessors

		public override bool ActiveConfig
		{
			get { return m_bActiveConfig; }
		}

		public override string Path
		{
			get
			{
				return m_sIniPath;
			}
			set
			{
				string absPath = System.IO.Path.GetFullPath( System.IO.Path.Combine( CDefault.defaultLogLocation, Path ) );
				if (m_sIniPath != absPath)
				{
					m_sIniPath = absPath;
				}
			}
		}

		#endregion

		#region Virtual Methods

		protected virtual bool Initialize()
		{
			if (!File.Exists(m_sIniPath))
			{
				string ExecutingAssembly = null;
				string ExecutingAssemblyVersion = null;
				string CodeBase = null;
				if (System.Reflection.Assembly.GetExecutingAssembly() != null)
				{
					ExecutingAssembly = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
					ExecutingAssemblyVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
					CodeBase = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
				}

				string sErrorMsg = "Configuration ini file not found. Make sure the following file exists:\r\n"
					+ "====================================================================\r\n\t"
					+ m_sIniPath
					+ "\r\n====================================================================\r\n"
					+ "\tExecutingAssembly = '" + ExecutingAssembly + "'\r\n"
					+ "\tExecutingAssemblyVersion = '" + ExecutingAssemblyVersion + "'\r\n"
					+ "\tCodeBase = '" + CodeBase + "'\r\n"
					+ "\tBaseDirectory = '" + CDefault.defaultLogLocation
					+ "\r\n====================================================================\r\n";
				throw new CException(sErrorMsg, eExceptionType.Config);
			}

			if (m_bActiveConfig)
			{
				//m_bActiveConfig = true;
				m_LastModified = File.GetLastWriteTimeUtc(m_sIniPath);
				m_Watcher = new FileSystemWatcher
				{
					Path = m_sIniPath.Substring( 0, m_sIniPath.LastIndexOf( "\\" ) ),
					Filter = m_sIniPath.Substring( m_sIniPath.LastIndexOf( "\\" ) + 1 ),
					NotifyFilter = NotifyFilters.LastWrite,
					IncludeSubdirectories = false
				};
				m_Watcher.Changed += new FileSystemEventHandler(OnChanged);
				m_Watcher.EnableRaisingEvents = true;
			}

			m_bInitialized = true;
			return m_bInitialized;
		}

		#endregion

		#region Overridden Methods

		/// <summary>
		/// GetValue function retrieves a value from ini file based on Section/Key pair
		/// This function will throw exception on error
		/// </summary>
		/// <param name="Section"></param>
		/// <param name="Key"></param>
		/// <param name="Result"></param>
		/// <returns></returns>
		public override bool GetValue(string Section, string Key, out string Result)
		{
			string ErrorMessage = "missing";
			// to avoid error loss this function will throw exception on error
			AssertNotDisposed();
			return GetValueGeneric(Section, Key, out Result, null, ref ErrorMessage);
		}

		/// <summary>
		/// GetValue function retrieves a value from ini file based on Section/Key pair
		/// This function will throw exception on error
		/// </summary>
		/// <param name="Section"></param>
		/// <param name="Key"></param>
		/// <param name="Result"></param>
		/// <returns>bool</returns>
		public override bool GetValue(string Section, string Key, out int Result)
		{
			bool bRet;
			string ErrorMessage = "missing";

			AssertNotDisposed();
			bRet = GetValueGeneric(Section, Key, out string sResult, null, ref ErrorMessage);
			if (bRet)
				try
				{
					Result = int.Parse(sResult);
				}
				catch (Exception ex)
				{
					throw new CException(String.Format(sParseError, sResult, Key, Section), eExceptionType.Config, ex);
				}
			else
				Result = int.MinValue;

			return bRet;
		}

		/// <summary>
		/// GetValue function retrieves a value from ini file based on Section/Key pair
		/// This function will throw exception on error
		/// </summary>
		/// <param name="Section"></param>
		/// <param name="Key"></param>
		/// <param name="Result"></param>
		/// <returns></returns>
		public override bool GetValue(string Section, string Key, out float Result)
		{
			bool bRet;
			string ErrorMessage = "missing";

			AssertNotDisposed();
			bRet = GetValueGeneric(Section, Key, out string sResult, null, ref ErrorMessage);
			if (bRet)
				try
				{
					Result = float.Parse(sResult);
				}
				catch (Exception ex)
				{
					throw new CException(String.Format(sParseError, sResult, Key, Section), eExceptionType.Config, ex);
				}
			else
				Result = float.NaN;

			return bRet;
		}

		/// <summary>
		/// GetValue function retrieves a value from ini file based on Section/Key pair
		/// This function will throw exception on error
		/// </summary>
		/// <param name="Section"></param>
		/// <param name="Key"></param>
		/// <param name="Result"></param>
		/// <returns></returns>
		public override bool GetValue(string Section, string Key, out bool Result)
		{
			bool bRet;
			string ErrorMessage = "missing";

			AssertNotDisposed();
			bRet = GetValueGeneric(Section, Key, out string sResult, null, ref ErrorMessage);
			if (bRet)
				try
				{
					Result = bool.Parse(sResult);
				}
				catch (Exception ex)
				{
					throw new CException(String.Format(sParseError, sResult, Key, Section), eExceptionType.Config, ex);
				}
			else
				Result = false;

			return bRet;
		}

		/// <summary>
		/// GetValue function retrieves a value from ini file based on Section/Key pair
		/// This function will return false on error
		/// </summary>
		/// <param name="Section"></param>
		/// <param name="Key"></param>
		/// <param name="Result"></param>
		/// <param name="Default"></param>
		/// <param name="ErrorMessage"></param>
		/// <returns></returns>
		public override bool GetValue(string Section, string Key, out string Result, object Default, ref string ErrorMessage)
		{
			AssertNotDisposed();
			return GetValueGeneric(Section, Key, out Result, Default, ref ErrorMessage);
		}

		/// <summary>
		/// GetValue function retrieves a value from ini file based on Section/Key pair
		/// This function will return false on error
		/// </summary>
		/// <param name="Section"></param>
		/// <param name="Key"></param>
		/// <param name="Result"></param>
		/// <param name="Default"></param>
		/// <param name="ErrorMessage"></param>
		/// <returns></returns>
		public override bool GetValue(string Section, string Key, out int Result, object Default, ref string ErrorMessage)
		{
			bool bRet;

			AssertNotDisposed();
			bRet = GetValueGeneric(Section, Key, out string sResult, Default, ref ErrorMessage);
			if (bRet)
				try
				{
					Result = int.Parse(sResult);
				}
				catch (Exception ex)
				{
					throw new CException(String.Format(sParseError, sResult, Key, Section), eExceptionType.Config, ex);
				}
			else
				Result = int.MinValue;

			return bRet;
		}

		/// <summary>
		/// GetValue function retrieves a value from ini file based on Section/Key pair
		/// This function will return false on error
		/// </summary>
		/// <param name="Section"></param>
		/// <param name="Key"></param>
		/// <param name="Result"></param>
		/// <param name="Default"></param>
		/// <param name="ErrorMessage"></param>
		/// <returns></returns>
		public override bool GetValue(string Section, string Key, out float Result, object Default, ref string ErrorMessage)
		{
			bool bRet;

			AssertNotDisposed();
			bRet = GetValueGeneric(Section, Key, out string sResult, Default, ref ErrorMessage);
			if (bRet)
				try
				{
					Result = float.Parse(sResult);
				}
				catch (Exception ex)
				{
					throw new CException(String.Format(sParseError, sResult, Key, Section), eExceptionType.Config, ex);
				}
			else
				Result = float.NaN;

			return bRet;
		}

		/// <summary>
		/// GetValue function retrieves a value from ini file based on Section/Key pair
		/// This function will return false on error
		/// </summary>
		/// <param name="Section"></param>
		/// <param name="Key"></param>
		/// <param name="Result"></param>
		/// <param name="Default"></param>
		/// <param name="ErrorMessage"></param>
		/// <returns></returns>
		public override bool GetValue(string Section, string Key, out bool Result, object Default, ref string ErrorMessage)
		{
			bool bRet;

			AssertNotDisposed();
			bRet = GetValueGeneric(Section, Key, out string sResult, Default, ref ErrorMessage);
			if (bRet)
				try
				{
					Result = bool.Parse(sResult);
				}
				catch (Exception ex)
				{
					throw new CException(String.Format(sParseError, sResult, Key, Section), eExceptionType.Config, ex);
				}
			else
				Result = false;

			return bRet;
		}

		#endregion

		#region Public Methods

		public bool GetValueGeneric(string Section, string Key, out string Result, object Default, ref string ErrorMessage)
		{
			StringBuilder sValue = new StringBuilder(255);
			Result = null;
			bool bThrowExOnError = false;
			Section = Section ?? "";

			if (Default == null & ErrorMessage == "missing")
			{
				bThrowExOnError = true;
				ErrorMessage = null;
			}

			if (!m_bInitialized) Initialize();

			int nRet = GetPrivateProfileString(Section, Key, null, sValue, 255, m_sIniPath);

			if (nRet == 0 && Default == null)
			{
				if (IsEmpty(ErrorMessage)) ErrorMessage = "The following entries are missing in the config file: \r\n";
				ErrorMessage += "Section: " + Section + "\t\tKey: " + Key + "\r\n";
				if (bThrowExOnError)
					throw new CException(ErrorMessage, eExceptionType.Config);
				else
					return false;
			}
			else if (nRet == 0 && Default != null)
			{
				Result = Default.ToString();
				//Result = Default;
				return true;
			}
			else
			{
				Result = sValue.ToString();
				//Result = Convert.ChangeType(sValue.ToString(), Result.GetType());
				return true;
			}
		}

		#endregion

		#region Private Methods

		// SDK Ini Functions
		[DllImport("Kernel32.dll")]
		private static extern int GetPrivateProfileString(
			[In] string sSection, [In] string sKey, [In] string sDefault,
			[Out] StringBuilder sValue, [In] int nSize, [In] string sIniFile);

		private string GetPrivateProfileString(string Section, string Key)
		{
			StringBuilder sValue = new StringBuilder(255);

			int nRet = GetPrivateProfileString(Section, Key, null, sValue, 255, m_sIniPath);
			if (nRet <= 0)
				return null;

			return sValue.ToString();
		}

		private void OnChanged(object source, FileSystemEventArgs e)
		{
			System.TimeSpan ts = File.GetLastWriteTimeUtc(m_sIniPath) - m_LastModified;

			if (ts.TotalMilliseconds > 500)
			{
				m_LastModified = File.GetLastWriteTimeUtc(m_sIniPath);
				OnConfigRefresh((EventArgs)e);
			}
		}

		private bool IsEmpty(string sValue)
		{
			return sValue == null || sValue.Length == 0;
		}

		private void VerifyPath(ref string sPath)
		{
			if (sPath != null)
				if (sPath.Substring(sPath.Length - 1, 1) != @"\") sPath += @"\";
		}

		#endregion

		#region IDisposable Members

		protected override void Dispose(bool disposing)
		{
			lock (this)
			{
				if (!this.m_bDisposed)
				{
					try
					{
						if (disposing)
						{
							// Release the managed resources added in this derived class
							if (m_Watcher != null)
							{
								m_Watcher.EnableRaisingEvents = false;
								m_Watcher.Dispose();
							}
						}
						// Release the native unmanaged resources
						this.m_bDisposed = true;
					}
					finally
					{
						// Call Dispose on the base class
						base.Dispose(disposing);
					}
				}
			}
		}

		#endregion

	}
}
