using System;
using System.Diagnostics;

namespace InvCommon
{
	/// <summary>
	/// Summary description for CEventLogLogger.
	/// </summary>
	[Serializable]
	public class CEventLogLogger : CLogger
	{
		#region Member Variables

		// private members
		private bool m_bDisposed = false;
		private bool m_bInitialized = false;
		private string m_sAppName;
		private EventLog m_EventLog;

		#endregion

		#region Constructors

		public CEventLogLogger()
			: this(System.Configuration.ConfigurationManager.AppSettings["ApplicationName"].Substring(0, AppDomain.CurrentDomain.FriendlyName.LastIndexOf(".")))
		{
		}

		public CEventLogLogger( string AppName )
			: base()
		{
			m_sAppName = AppName;
			// m_bInitialized = Initialize(); do not initialize until first call
		}

		#endregion

		#region Virtual Methods
		/*
		protected virtual string FormatBody( CLogEntry LogEntry )
		{
			string Text;

			Text = string.Format( "{0} |{1}|{2}| {3}",
				LogEntry.TimeStamp.ToString( "yyyy-MM-dd HH:mm:ss.fff" ),
				LogEntry.Win32ThreadId,
				(eSeverity) LogEntry.LoggedSeverity,
				LogEntry.Message );
			return Text;
		}
		*/
		#endregion

		#region Overridden Methods

		public override string LogPath
		{
			get
			{
				if( !m_bInitialized ) Initialize();
				return m_EventLog.LogDisplayName;
			}
			set {/*do nothing*/}
		}

		protected override void PrintOut( CLogEntry LogEntry )
		{
			if( !m_bInitialized ) Initialize();
			//string sText = FormatBody (LogEntry);
			try
			{
				EventLogEntryType LogType;

				switch( (eSeverity) LogEntry.LoggedSeverity )
				{
					case eSeverity.Trace:
						LogType = EventLogEntryType.Information;
						break;
					case eSeverity.Warning:
						LogType = EventLogEntryType.Warning;
						break;
					case eSeverity.Error:
						LogType = EventLogEntryType.Error;
						break;
					case eSeverity.Info:
						LogType = EventLogEntryType.Information;
						break;
					default:
						LogType = EventLogEntryType.Warning;
						break;
				}
				m_EventLog.WriteEntry(/*sText*/ LogEntry.Message, LogType );
			}
			catch( Exception ex )
			{
				throw ex;	// will add functionality later...
			}
		}

		#endregion

		#region Protected Methods

		protected bool Initialize()
		{
			//base.Initialize();

			m_EventLog = new EventLog();
			if( !EventLog.SourceExists( m_sAppName ) )
			{
				EventLog.CreateEventSource( m_sAppName, "Application" );
			}
			m_EventLog.Source = m_sAppName;

			m_bInitialized = true;
			return m_bInitialized;
		}

		#endregion

		#region IDisposable Members

		protected override void Dispose( bool disposing )
		{
			lock( this )
			{
				if( !this.m_bDisposed )
				{
					try
					{
						if( disposing )
						{
							// Release the managed resources added in this derived class
						}
						// Release the native unmanaged resources

						this.m_bDisposed = true;
					}
					finally
					{
						// Call Dispose on the base class
						base.Dispose( disposing );
					}
				}
			}
		}

		#endregion

	}
}
