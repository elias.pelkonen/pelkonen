using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Reflection;
using System.Web.Hosting;

namespace InvCommon
{
	/// <summary>
	/// Summary description for Common.
	/// </summary>

	#region Custom Attributes

	// Used to prevent an object to be copied when cloning CApp instance
	// e.g.  CApp childCApp = new CApp( parentCApp );
	[AttributeUsage(AttributeTargets.All)]
	public class InstanceAttribute : System.Attribute
	{
	}

	// Used to prevent an object to be copied when cloning CApp instance
	// based on condition bPassSoftInstance true / false
	// e.g.  CApp childCApp = new CApp( parentCApp, bPassSoftInstance );
	[AttributeUsage(AttributeTargets.All)]
	public class SoftInstanceAttribute : System.Attribute
	{
	}

	#endregion

	#region Enumerations

	// extra attribute for the custom Exceptions
	public enum eExceptionType
	{
		None = -1,
		Success = 0,
		Warning = 100,
		Cancel = 110,
		Retry = 120,
		XRef = 200,
		Data = 210,
		Config = 300,
		Process = 310,
		System = 400,
		Security = 500,
	};

	public enum eSeverity
	{
		None = 0,
		Error,
		Warning,
		Info,
		Trace,
		Debug,
		Deep
	};

	public enum eDetailType
	{
		None,
		Brief,
		Medium,
		Full
	};

	#endregion

	public struct ExceptionInfo
	{
		public System.Type Type;			// to identify original exception by type
		public int HResult;					// to identify original exception by HRESULT
		public eExceptionType NewType;		// exception will be replaced with this type
		public string NewMessage;			// optional description for the replacement
	}

	[Serializable]
	public class CDefault
	{
		#region Member Variables

		static private string BaseDirectory = AppDomain.CurrentDomain.BaseDirectory;
		public const eSeverity defaultSeverity = eSeverity.Info;
		public const eSeverity defaultLogLevel = eSeverity.Info;
		static public string defaultLogLocation = BaseDirectory;
		static public string defaultWebLogLocation = BaseDirectory;
		public const string defaultLogCategory = "Default Category";
		public const string defaultSMTPServer = "invisor.ca";
		public const string defaultMailFrom = "support@invisor.ca";
		public const string defaultMailTo = "Ryszard.Jarzecki@invisor.ca";
		public const string defaultMailCc = "";
		public const int LogFileTimeLimit = 24;		// log file is valid for 24 hours
		public const int LogFileSizeLimit = 10000;	// max size for log file is 10 000 kilobytes
		public const int defaultEventID = 100;
		public const int minimumPriority = 0;
		public const int defaultInvProcessId = -1;
		public const string dbServerIniSection = "Server";
		public const string dbServerAlternateIniSection = "ServerAlternate";

		#endregion
	}

	[Serializable]
	internal class NativeMethods
	{
		#region Constructors
		private NativeMethods()
		{
		}
		#endregion

		#region Constans

		// Constants for use with GetSecurityInfo
		internal const uint OWNER_SECURITY_INFORMATION = 0x00000001;
		internal const uint GROUP_SECURITY_INFORMATION = 0x00000002;
		internal const uint DACL_SECURITY_INFORMATION = 0x00000004;
		internal const uint SACL_SECURITY_INFORMATION = 0x00000008;

		// HRESULTS
		internal const int CONTEXT_E_NOCONTEXT = unchecked((int)(0x8004E004));
		internal const int E_NOINTERFACE = unchecked((int)(0x80004002));

		#endregion

		#region Interop API declarations

		[DllImport("kernel32.dll")]
		internal static extern int QueryPerformanceCounter(out Int64 lpPerformanceCount);

		[DllImport("kernel32.dll")]
		internal static extern int QueryPerformanceFrequency(out Int64 lpPerformanceCount);

		[DllImport("mtxex.dll", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int GetObjectContext([Out]
			[MarshalAs(UnmanagedType.Interface)] out IObjectContext pCtx);

		[DllImport("kernel32.dll")]
		internal static extern IntPtr GetCurrentProcess();

		[DllImport("kernel32.dll")]
		internal static extern int GetCurrentProcessId();

		[DllImport("kernel32.dll", SetLastError = true)]
		[PreserveSig]
		internal static extern int GetModuleFileName([In] IntPtr hModule, [Out] StringBuilder lpFilename, [In]
			[MarshalAs(UnmanagedType.U4)] int nSize);

		[DllImport("kernel32.dll")]
		internal static extern IntPtr GetModuleHandle(string moduleName);

		[DllImport("secur32.dll", CharSet = CharSet.Unicode, EntryPoint = "GetUserNameExW", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool GetUserNameEx([In] ExtendedNameFormat nameFormat, StringBuilder nameBuffer, ref uint size);

		[DllImport("advapi32.dll")]
		internal static extern int GetSecurityInfo(IntPtr handle, SE_OBJECT_TYPE objectType, uint securityInformation, ref IntPtr ppSidOwner, ref IntPtr ppSidGroup, ref IntPtr ppDacl, ref IntPtr ppSacl, out IntPtr ppSecurityDescriptor);

		[DllImport("advapi32.dll", CharSet = CharSet.Unicode)]
		internal static extern bool LookupAccountSid(
			IntPtr systemName, // name of local or remote computer
			IntPtr sid, // security identifier
			StringBuilder accountName, // account name buffer
			ref uint accountNameLength, // size of account name buffer
			StringBuilder domainName, // domain name
			ref uint domainNameLength, // size of domain name buffer
			out int sidType // SID type
			);

		[DllImport("kernel32.dll")]
		public static extern int GetCurrentThreadId();

		#endregion

		[ComImport]
		[Guid("51372AE0-CAE7-11CF-BE81-00AA00A2FA25")]
		[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
		internal interface IObjectContext
		{
			[return: MarshalAs(UnmanagedType.Interface)]
			Object CreateInstance([MarshalAs(UnmanagedType.LPStruct)] Guid rclsid, [MarshalAs(UnmanagedType.LPStruct)] Guid riid);

			void SetComplete();

			void SetAbort();

			void EnableCommit();

			void DisableCommit();

			[PreserveSig]
			[return: MarshalAs(UnmanagedType.Bool)]
			bool IsInTransaction();

			[PreserveSig]
			[return: MarshalAs(UnmanagedType.Bool)]
			bool IsSecurityEnabled();

			[return: MarshalAs(UnmanagedType.Bool)]
			bool IsCallerInRole([In]
				[MarshalAs(UnmanagedType.BStr)] String role);
		}

		#region Enumerations

		internal enum ExtendedNameFormat : int
		{
			// Examples for the following formats assume a fictitous company
			// which hooks into the global X.500 and DNS name spaces as follows.
			//
			// Enterprise root domain in DNS is
			//
			//      widget.com
			//
			// Enterprise root domain in X.500 (RFC 1779 format) is
			//
			//      O=Widget, C=US
			//
			// There exists the child domain
			//
			//      engineering.widget.com
			//
			// equivalent to
			//
			//      OU=Engineering, O=Widget, C=US
			//
			// There exists a container within the Engineering domain
			//
			//      OU=Software, OU=Engineering, O=Widget, C=US
			//
			// There exists the user
			//
			//      CN=John Doe, OU=Software, OU=Engineering, O=Widget, C=US
			//
			// And this user's downlevel (pre-ADS) user name is
			//
			//      Engineering\JohnDoe

			// unknown name type
			NameUnknown = 0,

			// CN=John Doe, OU=Software, OU=Engineering, O=Widget, C=US
			NameFullyQualifiedDN = 1,

			// Engineering\JohnDoe
			NameSamCompatible = 2,

			// Probably "John Doe" but could be something else.  I.e. The
			// display name is not necessarily the defining RDN.
			NameDisplay = 3,

			// String-ized GUID as returned by IIDFromString().
			// eg: {4fa050f0-f561-11cf-bdd9-00aa003a77b6}
			NameUniqueId = 6,

			// engineering.widget.com/software/John Doe
			NameCanonical = 7,

			// johndoe@engineering.com
			NameUserPrincipal = 8,

			// Same as NameCanonical except that rightmost '/' is
			// replaced with '\n' - even in domain-only case.
			// eg: engineering.widget.com/software\nJohn Doe
			NameCanonicalEx = 9,

			// www/srv.engineering.com/engineering.com
			NameServicePrincipal = 10,

			/// <summary>
			/// DNS domain name + SAM username 
			/// eg: engineering.widget.com\JohnDoe
			/// </summary>
			NameDnsDomain = 12
		}

		internal enum SE_OBJECT_TYPE
		{
			SE_UNKNOWN_OBJECT_TYPE = 0,
			SE_FILE_OBJECT,
			SE_SERVICE,
			SE_PRINTER,
			SE_REGISTRY_KEY,
			SE_LMSHARE,
			SE_KERNEL_OBJECT,
			SE_WINDOW_OBJECT,
			SE_DS_OBJECT,
			SE_DS_OBJECT_ALL,
			SE_PROVIDER_DEFINED_OBJECT,
			SE_WMIGUID_OBJECT
		}

		#endregion
	}

	[Serializable]
	public sealed class ArgumentValidation
	{
		#region Constructors

		private ArgumentValidation()
		{
		}

		#endregion

		#region Public Methods

		// Check if the parameter is an empty string.
		public static void CheckForEmptyString(string variable, string variableName)
		{
			CheckForNullReference(variable, variableName);
			CheckForNullReference(variableName, "variableName");
			if (variable.Length == 0)
			{
				throw new ArgumentException(String.Format("The value of '{0}' can not be an empty string.", variableName));
			}
		}

		// Check if the parameter is null
		public static void CheckForNullReference(object variable, string variableName)
		{
			if (variableName == null)
			{
				throw new ArgumentNullException("variableName");
			}

			if (null == variable)
			{
				throw new ArgumentNullException(variableName);
			}
		}

		// Validates that the input messageName is neither null nor empty
		public static void CheckForInvalidNullNameReference(string name, string messageName)
		{
			if ((null == name) || (name.Length == 0))
			{
				throw new InvalidOperationException(String.Format("The name for the '{0}' can not be null or an empty string.", messageName));
			}
		}

		// Checks the byte array for zero length and throw an ArgumentException if the length equals zero.
		public static void CheckForZeroBytes(byte[] bytes, string variableName)
		{
			CheckForNullReference(bytes, "bytes");
			CheckForNullReference(variableName, "variableName");
			if (bytes.Length == 0)
			{
				throw new ArgumentException("The value must be greater than 0 bytes.", variableName);
			}
		}

		// Check the parameter to determine if it matches the Type of type
		public static void CheckExpectedType(object variable, Type type)
		{
			CheckForNullReference(variable, "variable");
			CheckForNullReference(type, "type");
			if (!type.IsAssignableFrom(variable.GetType()))
			{
				throw new ArgumentException(String.Format("The type is invalid. Expected type '{0}'.", type.FullName));
			}
		}

		// Check the parameter to determine if it is a valid defined enumeration for enumType
		public static void CheckEnumeration(Type enumType, object variable, string variableName)
		{
			CheckForNullReference(variable, "variable");
			CheckForNullReference(enumType, "enumType");
			CheckForNullReference(variableName, "variableName");

			if (!Enum.IsDefined(enumType, variable))
			{
				throw new ArgumentException(String.Format("{0} is not a valid value for {1}.", variable.ToString(), enumType.FullName), variableName);
			}
		}

		#endregion
	}


	/// <summary>
	/// The following AsyncHelper class was taken from this link:
	///
	/// http://discuss.develop.com/archives/wa.exe?A2=ind0302B&L=ADVANCED-DOTNET&D=0&I=-3&P=2534
	///
	/// by Mike Woodring (DevelopMentor)
	/// AsyncHelper
	///
	/// This class provides a FireAndForget method that facilitates
	/// calling an arbitrary method asynchronously w/o worrying about
	/// or waiting for the return value.
	///
	/// The usage model is along these lines (example assumes
	/// the existence of void SomeMethod(string, double).
	///
	/// SomeDelegate sd = new SomeDelegate(SomeMethod);
	/// AsyncHelper.FireAndForget(sd, "pi", 3.1415927);
	///
	/// Note - there's an extra (IMO) level of indirection in the
	/// following code that I don't think should be necessary.  The
	/// goal is to call Delegate.DynamicInvoke using BeginInvoke.
	/// Using BeginInvoke gets us asynchrony, using DynamicInvoke
	/// makes this generically reusable for any delegate.  However,
	/// when I call DynamicInvoke directly I get an undetailed
	/// execution engine fatal error.  When I use BeginInvoke to
	/// call a shim method that just turns around and calls
	/// DynamicInvoke, things work.  Strange (and consistent on
	/// the 1.0 and 1.1 runtimes).
	///
	/// </summary>
	[Serializable]
	public class AsyncHelper
	{
		#region Member Variables

		delegate void DynamicInvokeShimProc(Delegate d, object[] args);
		static DynamicInvokeShimProc dynamicInvokeShim = new DynamicInvokeShimProc(DynamicInvokeShim);
		static AsyncCallback dynamicInvokeDone = new AsyncCallback(DynamicInvokeDone);

		#endregion

		#region Public Methods

		public static void FireAndForget(Delegate d, params object[] args)
		{
			dynamicInvokeShim.BeginInvoke(d, args, dynamicInvokeDone, null);
		}

		#endregion

		#region Private Methods

		private static void DynamicInvokeShim(Delegate d, object[] args)
		{
			d.DynamicInvoke(args);
		}

		private static void DynamicInvokeDone(IAsyncResult ar)
		{
			dynamicInvokeShim.EndInvoke(ar);
		}

		#endregion
	}

	[Serializable]
	public class CEnvironment
	{
		public static string Info()
		{
			StringBuilder sbText = new StringBuilder();
			sbText.Append("\r\n------------------------------------------------------------------------------------------------");
			sbText.AppendFormat("\r\nDateTime:\t\t{0}\r\nMachineName:\t\t{1}\r\nUserDomainName:\t\t{2}\r\nUserName:\t\t{3}\r\nThreadId:\t\t{4}\r\nCLR Version:\t\t{5}\r\nCurrentPrincipal:\t{6}\r\n",
				DateTime.Now.ToString("MM-dd-yyyy HH:mm:ss.fffffff"),
				Environment.MachineName,
				Environment.UserDomainName,
				Environment.UserName,
				Thread.CurrentThread.ManagedThreadId,
				Environment.Version,
				Thread.CurrentPrincipal.Identity);

			AppDomain currentDomain = AppDomain.CurrentDomain;
			Assembly[] assemblies = currentDomain.GetAssemblies();
			sbText.Append("Assemblies loaded in current appdomain:\r\n");
			foreach (Assembly assembly in assemblies)
			{
				sbText.AppendFormat("{0}\r\n", assembly.ToString());
			}
			sbText.Append("------------------------------------------------------------------------------------------------\r\n");
			return sbText.ToString();
		}
	}

	public static class GenericBackgroundInvoke
	{
		public static void QueueBackgroundWorkItem( Action<CancellationToken> workItem )
		{
			try
			{
				HostingEnvironment.QueueBackgroundWorkItem( workItem );
			}
			catch( InvalidOperationException )
			{
				workItem.Invoke( new CancellationToken() );
			}
		}
	}
}