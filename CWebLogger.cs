using System;
using System.Configuration;
using System.Collections;

namespace InvCommon
{
	/// <summary>
	/// The concrete class for a flat file Logger.
	/// </summary>
	[Serializable]
	public class CWebLogger : CFileLogger
	{
		#region Member Variables

		// private members
		private bool m_bDisposed = false;
		private bool m_bInitialized = false;
		private ArrayList m_LogEntries;

		#endregion

		#region Constructors

		public CWebLogger()
			: this(CDefault.defaultWebLogLocation + ConfigurationManager.AppSettings["ApplicationName"] + ".log",
			false,
			0 )
		{
		}

		public CWebLogger( string FullPath, bool Separate, int TimeLimit )
			: base( FullPath, Separate, TimeLimit )
		{
		}

		~CWebLogger()
		{
			Flush();
			Dispose( false );
		}
		#endregion

		#region Overridden Methods

		protected override bool Initialize()
		{
			base.Initialize();
			m_LogEntries = new ArrayList();

			m_bInitialized = true;
			return m_bInitialized;
		}

		public override bool Flush()
		{
			AssertNotDisposed();
			if( m_LogEntries != null )
			{
				foreach( object logEntry in m_LogEntries )
				{
					base.PrintOut( (CLogEntry) logEntry );
				}
				m_LogEntries.Clear();
			}
			return true;
		}

		protected override void PrintOut( CLogEntry LogEntry )
		{
			if( !m_bInitialized ) Initialize();

			m_LogEntries.Add( LogEntry );
		}

		#endregion

		#region IDisposable Members

		protected override void Dispose( bool disposing )
		{
			lock( this )
			{
				if( !this.m_bDisposed )
				{
					try
					{
						if( disposing )
						{
							// Release the managed resources added in this derived class
						}
						// Release the native unmanaged resources

						this.m_bDisposed = true;
					}
					finally
					{
						// Call Dispose on the base class
						base.Dispose( disposing );
					}
				}
			}
		}

		#endregion
	}
}