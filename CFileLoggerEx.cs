using System;
using System.Configuration;
using System.Threading;
using System.Collections;

namespace InvCommon
{
	/// <summary>
	/// The concrete class for a flat file Logger.
	/// </summary>
	[Serializable]
	public class CFileLoggerEx : CFileLogger
	{
		#region Member Variables

		// private members
		private bool m_bDisposed = false;
		private bool m_bInitialized = false;
		private Queue m_MessageQueue;
		private ManualResetEvent m_StopEvent;
		private ManualResetEvent m_QueueMsgEvent;
		private Thread t;
		private bool m_bInsideSecondThread = false;

		#endregion

		#region Constructors

		public CFileLoggerEx()
			: this(CDefault.defaultLogLocation + ConfigurationManager.AppSettings["ApplicationName"] + ".log",
								false,
								0 )
		{
		}

		public CFileLoggerEx( string FullPath, bool Separate, int TimeLimit )
			: base( FullPath, Separate, TimeLimit )
		{
		}

		#endregion

		#region Virtual Methods

		public virtual void MessagePump()
		{
			WaitHandle[] Events = new WaitHandle[2] { m_QueueMsgEvent, m_StopEvent };
			int iEventType;
			CLogEntry logEntry;

			while( true )
			{
				iEventType = WaitHandle.WaitAny( Events );
				switch( iEventType )
				{
					case 0:							// log event
						m_bInsideSecondThread = true;
						lock( m_MessageQueue.SyncRoot )
						{
							if( m_MessageQueue.Count == 0 )
							{
								m_QueueMsgEvent.Reset();
								m_bInsideSecondThread = false;
								break;
							}
							logEntry = (CLogEntry) m_MessageQueue.Dequeue();
							if( m_MessageQueue.Count == 0 )
							{
								m_QueueMsgEvent.Reset();
							}
							PrintOut( logEntry );
						}
						m_bInsideSecondThread = false;
						break;
					case 1:							// stop event;
						StopLogger();
						return;
				}
			}
		}

		protected virtual void StopLogger() 			// clean up code for graceful exit
		{
		}

		#endregion

		#region Overridden Methods

		public override void Write( CLogEntry LogEntry )
		{
			AssertNotDisposed();
			if( !m_bInitialized ) Initialize();

			if( m_bInsideSecondThread )
				t.Priority = ThreadPriority.Normal;

			lock( m_MessageQueue.SyncRoot )
			{
				if( LogEntry.Severity == eSeverity.Error )
				{
					CLogEntry LogEntry2;
					while( 0 != m_MessageQueue.Count )
					{
						LogEntry2 = (CLogEntry) m_MessageQueue.Dequeue();
						PrintOut( LogEntry2 );
					}
					PrintOut( LogEntry );
					m_QueueMsgEvent.Reset();
				}
				else
				{
					m_MessageQueue.Enqueue( LogEntry );
					m_QueueMsgEvent.Set();
				}
			}
			if( t.Priority == ThreadPriority.Normal )
				t.Priority = ThreadPriority.BelowNormal;
		}

		protected override bool Initialize()
		{
			base.Initialize();

			m_StopEvent = new ManualResetEvent( false );
			m_QueueMsgEvent = new ManualResetEvent( false );
			m_MessageQueue = new Queue();

			t = new Thread( new ThreadStart( this.MessagePump ) );
			t.Start();
			t.Priority = ThreadPriority.BelowNormal;

			m_bInitialized = true;
			return m_bInitialized;
		}

		protected override void StopLogging()
		{
			if( t.Priority == ThreadPriority.Normal )
				t.Priority = ThreadPriority.BelowNormal;

			m_StopEvent.Set();
		}

		#endregion

		#region IDisposable Members

		protected override void Dispose( bool disposing )
		{
			lock( this )
			{
				if( !this.m_bDisposed )
				{
					try
					{
						if( disposing )
						{
							// Release the managed resources added in this derived class
							this.StopLogging();
						}
						// Release the native unmanaged resources

						this.m_bDisposed = true;
					}
					finally
					{
						// Call Dispose on the base class
						base.Dispose( disposing );
					}
				}
			}
		}

		#endregion

	}
}