﻿using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace InvCommon
{
    public static class CFileInfoExtension
    {
        public static string CalculateMD5(this FileInfo fileInfo)
        {
            MD5 md5 = MD5.Create();
            FileStream stream = File.OpenRead(fileInfo.FullName);
            byte[] hash = md5.ComputeHash(stream);
            StringBuilder hashString = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                hashString.Append(hash[i].ToString("X2"));
            }

            return hashString.ToString();
        }
    }
}
