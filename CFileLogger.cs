using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Threading;

namespace InvCommon
{
	/// <summary>
	/// The concrete class for a flat file Logger.
	/// </summary>
	[Serializable]
	public class CFileLogger : CLogger
	{
		#region Member Variables

		// protected members
		protected string m_sOrigFullPath;
		protected DateTime m_MaxValidDate;
		protected bool m_bSeparate;
		protected int m_iTimeLimit;
		protected string m_LogPath;
		protected DateTime m_DateTimeStamp;

		// private members
		private bool m_bInitialized = false;
		private bool m_bDisposed = false;
		private Object Access = new Object();

		#endregion

		#region Constructors

		public CFileLogger()
			: this(CDefault.defaultLogLocation + ConfigurationManager.AppSettings["ApplicationName"] + ".log",
			false,
			0)
		{
		}

		public CFileLogger(string FullPath, bool Separate, int TimeLimit)
			: base()
		{
			if (!string.IsNullOrWhiteSpace(FullPath))
				if (FullPath.StartsWith(".."))
					FullPath = CDefault.defaultLogLocation.Substring(0, CDefault.defaultLogLocation.LastIndexOf(@"\", CDefault.defaultLogLocation.Length - 2)) + FullPath.Substring(2);
				else if (FullPath.StartsWith("."))
					FullPath = CDefault.defaultLogLocation + FullPath.Substring(2);
			m_sOrigFullPath = FullPath;
			m_LogPath = FullPath;
			m_bSeparate = Separate;
			//m_iTimeLimit = TimeLimit;
			m_DateTimeStamp = DateTime.Now;
			m_MaxValidDate = m_DateTimeStamp.Date.AddDays(1);
		}

		#endregion

		#region Virtual Methods

		protected virtual bool Initialize()
		{
			m_LogPath = GetLogFile(m_LogPath, m_bSeparate, m_iTimeLimit);

			m_bInitialized = true;
			return m_bInitialized;
		}

		protected virtual string FormatBody(CLogEntry LogEntry)
		{
			string Text;

			if (LogEntry.Message == string.Empty)
			{
				Text = LogEntry.Message;
			}
			else
			{
				Text = string.Format("{0}\t|{1}|\t{2}\t",
					LogEntry.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss.fff"),
					LogEntry.Win32ThreadId,
					LogEntry.Message);
			}
			return Text;
		}

		#endregion

		#region Overridden Methods

		public override string LogPath
		{
			get
			{
				AssertNotDisposed();
				if (!m_bInitialized) Initialize();
				return m_LogPath;
			}
			set
			{
				AssertNotDisposed();
				if (!m_bInitialized) Initialize();
				if (m_LogPath != value)
				{
					m_LogPath = value;
				}
			}
		}

		protected override void PrintOut(CLogEntry LogEntry)
		{
			string sText = FormatBody(LogEntry);
			if (!m_bInitialized) Initialize();

			if (DateTime.Compare(LogEntry.TimeStamp, m_MaxValidDate) > 0)
			{
				UpdateDateTime();
			}

			lock (Access)
			{
				try
				{
					using (StreamWriter sw = new StreamWriter(m_LogPath, true))
					{
						sw.Write(sText + "\r\n");
					}
				}
				catch (DirectoryNotFoundException)
				{
					// get the file name and try to log it log to app folder
					m_LogPath = CDefault.defaultLogLocation + Path.GetFileName(m_LogPath);
					try
					{
						using (StreamWriter sw = new StreamWriter(m_LogPath, true))
						{
							sw.Write(sText + "\r\n");
						}
					}
					catch (IOException)
					{
					}
				}
				catch (IOException)
				{
					// retry
					Thread.Sleep(5);
					try
					{
						using (StreamWriter sw = new StreamWriter(m_LogPath, true))
						{
							sw.Write(sText + "\r\n");
						}
					}
					catch (IOException)
					{
					}
				}
			}
		}

		#endregion

		#region Protected Methods

		protected string GetLogFile(string sFullPath, bool bSeparate, int iTimeLimit)
		{
			string sFileLocation = string.Empty;
			string sFileName = string.Empty;
			string sFileExtension = string.Empty;

			sFileLocation = sFullPath.Substring(0, sFullPath.LastIndexOf("\\"));
			sFileName = sFullPath.Substring(sFullPath.LastIndexOf("\\") + 1);
			sFileExtension = sFileName.Substring(sFileName.LastIndexOf("."));
			sFileName = sFileName.Remove(sFileName.LastIndexOf("."));
			sFileName = Regex.Replace(sFileName, @"[\\/:*?""<>|]", "_");

			if (!bSeparate)
			{
				sFileName = sFileName + "_" + m_DateTimeStamp.ToString("yyMMdd");
			}
			else
			{
				sFileName = sFileName + "_" + m_DateTimeStamp.ToString("yyMMdd") + "_" + m_DateTimeStamp.ToString("HHmmss");
			}

			// check if file exists. if so, add a version number 
			if (bSeparate && File.Exists(sFileLocation + "\\" + sFileName + sFileExtension))
			{
				int iMaxVer = 1;
				while (File.Exists(sFileLocation + "\\" + sFileName + "_" + iMaxVer++ + sFileExtension)) { }

				return sFileLocation + "\\" + sFileName + "_" + iMaxVer + sFileExtension;
			}

			return sFileLocation + "\\" + sFileName + sFileExtension;
		}

		protected string UpdateDateTime()
		{
			DateTime newDateTimeStamp = DateTime.Now;

			// the Time turned 24:00, create new file
			if (m_bSeparate)
			{
				string sDateTimeLong = "_" + m_DateTimeStamp.ToString("yyMMdd") + "_" + m_DateTimeStamp.ToString("HHmmss");
				m_LogPath = m_LogPath.Replace(sDateTimeLong, "_" + newDateTimeStamp.ToString("yyMMdd") + "_" + newDateTimeStamp.ToString("HHmmss"));
			}
			else
			{
				string sDateTimeShort = "_" + m_DateTimeStamp.ToString("yyMMdd");
				m_LogPath = m_LogPath.Replace(sDateTimeShort, "_" + newDateTimeStamp.ToString("yyMMdd"));
			}

			m_DateTimeStamp = newDateTimeStamp;
			m_MaxValidDate = m_DateTimeStamp.Date.AddDays(1);

			return m_LogPath;
		}

		#endregion

		#region IDisposable Members

		protected override void Dispose(bool disposing)
		{
			lock (Access)
			{
				if (!this.m_bDisposed)
				{
					try
					{
						if (disposing)
						{
							// Release the managed resources added in this derived class
						}
						// Release the native unmanaged resources

						this.m_bDisposed = true;
					}
					finally
					{
						// Call Dispose on the base class
						base.Dispose(disposing);
					}
				}
			}
		}

		#endregion

	}
}