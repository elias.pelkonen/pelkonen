using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq.Expressions;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

namespace InvCommon
{
	/// <summary>
	/// Summary description for CApp.
	/// </summary>

	[Serializable]
	public class CApp : IDisposable
	{
		#region Member Variables

		// variables annotated with [Instance] are _not_ passed to the cloned object
		// variables annotated with [SoftInstance] are _not_ passed to the cloned object,
		//	unless in the constructor you will set parameter bPassSoftInstance = true

		protected event EventHandler m_StateChange;
		protected EventHandler m_StateChangeEventHandler;

		[Instance]
		protected CApp m_ParentClass;
		protected CConfig m_Config;
		protected CLogger m_Log;
		protected CMail m_Mail;

		protected string m_sAppName;
		[Instance]
		protected ListDictionary m_StateInfo = null;
		protected eSeverity m_LogLevel = CDefault.defaultLogLevel;
		protected bool m_bSendExceptionMail = true;
		protected string m_sErrorMailFrom;
		protected string m_sErrorMailTo;
		protected string m_sErrorMailCc;
		protected bool m_bCanSendMail;
		protected bool m_bLogEntryWithMethodName = false;

		[Instance]
		private bool m_bDisposableComponents = true;
		[Instance]
		private bool m_bDisposed = false;

		[Instance]
		bool m_bPassSoftInstance = false;
		// exception objects may or may not be shared
		[SoftInstance]
		protected bool m_bFixedExPoolSize;
		[SoftInstance]
		protected int m_iExceptionPoolSize;
		[SoftInstance]
		protected ArrayList m_ExceptionPool;
		[SoftInstance]
		protected Exception m_LastException = null;
		[SoftInstance]
		protected ListDictionary m_ExceptionFormatters;

		protected bool? m_bStateInitialized = null;
		[Instance]
		protected bool m_bStateChanged = false;

		protected static byte[] BkgColor = { 15, 145, 66, 23, 231, 85, 195, 87, 209, 35, 77, 155, 149, 96, 8, 136 };

		private bool m_bShowContextInfoErrors = false;
		private bool? m_bIsHosted = null;
		private bool? m_bIsAuthenticated = null;
		private string m_LogonUser = null;
		private string m_UserHostName = null;
		private string m_DomainHostName = null;

		private static string m_NotificationServer = "";
		private static string m_NotificationFrom = "";
		private static string m_NotificationTo = "";
		private static string m_NotificationCC = null;
		private static string m_NotificationOperations = "";
		private static string m_NotificationService = "";
		private static string m_NotificationSupport = "";
		private static string m_NotificationSite = "";

		private int m_maxExecuteSqlRetryCount = 0;
		private int m_maxExecuteSqlRetryDelay = 0;

		#endregion

		#region Public Enumerations
		public enum RetryableSqlErrors { Timeout = -2, NoLock = 1204, Deadlock = 1205, WordbreakerTimeout = 30053 }
		#endregion

		#region Public Properties

		/// <summary> 
		/// Returns true if the application domain is hosted by an ASP.NET ApplicationManager object; otherwise, false.
		/// </summary> 
		public bool? IsHosted
		{
			get
			{
				if( m_bIsHosted == null )
					m_bIsHosted = HostingEnvironment.IsHosted;
				return m_bIsHosted;
			}
		}

		/// <summary> 
		/// Returns true when the user has been authenticated (e.g. in IIS); otherwise, false.
		/// </summary> 
		public bool? IsAuthenticated
		{
			get
			{
				if( m_bIsAuthenticated == null )
				{
					if( IsHosted == true )
						if( HttpContext.Current != null )
							m_bIsAuthenticated = HttpContext.Current.User.Identity.IsAuthenticated;
						else
							m_bIsAuthenticated = true;
					else
						m_bIsAuthenticated = false;
				}
				return m_bIsAuthenticated;
			}
		}

		/// <summary> 
		/// Returns the name of the current user.
		/// Sets the name.
		/// </summary> 
		public string LogonUser
		{
			get
			{
				try
				{
					if( m_LogonUser == null )
					{
						if( IsHosted == true )
							if( IsAuthenticated == true )
								if( HttpContext.Current != null )
									m_LogonUser = HttpContext.Current.User.Identity.Name.ToUpper();
								else
									m_LogonUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToUpper();
							else
								m_LogonUser = "ANONYMOUS";
						else
							m_LogonUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToUpper();
					}
					return m_LogonUser;
				}
				catch( Exception ex )
				{
					if( m_bShowContextInfoErrors )
						HandleException( new CException( "Retrieve LogonUser failed", eExceptionType.Warning, ex, eDetailType.Medium ) );
					return "error occurred";
				}
			}

			set { m_LogonUser = value; }
		}

		/// <summary> 
		/// Returns the name of the user's host machine if the application domain is hosted by an ASP.NET, otherwise returns local host.
		/// Sets the name.
		/// </summary> 
		public string UserHostName
		{
			get
			{
				string sUserHostAddress = null;
				try
				{
					if( m_UserHostName == null )
					{
						if( IsHosted == true )
						{
							if( HttpContext.Current != null )
							{
								sUserHostAddress = HttpContext.Current.Request.UserHostAddress;
								m_UserHostName = Dns.GetHostEntry( sUserHostAddress ).HostName.ToUpper();
								//m_UserHostName = GetNetBiosName( sUserHostAddress );
							}
							else
								m_UserHostName = Dns.GetHostEntry( "localhost" ).HostName.ToUpper();
						}
						else
							m_UserHostName = Dns.GetHostEntry( "localhost" ).HostName.ToUpper();
					}
					return m_UserHostName;
				}
				catch( Exception ex )
				{
					if( m_bShowContextInfoErrors )
						HandleException( new CException( "Retrieve UserHostName failed", eExceptionType.Warning, ex, eDetailType.Medium ) );
					else
						Log( eSeverity.Info, "Retrieve UserHostName failed\n" + ex.Message );
					return sUserHostAddress ?? "error occurred";
				}
			}

			set { m_UserHostName = value; }
		}

		/// <summary> 
		/// Returns domain name in hosted environment; otherwise, machine name.
		/// </summary> 
		public string DomainHostName
		{
			get
			{
				try
				{
					if( m_DomainHostName == null )
					{
						if( IsHosted == true )
							if( HttpContext.Current != null )
							{
								Uri url = HttpContext.Current.Request.Url;
								if( url.Port == 80 || url.Port == 443 )
									m_DomainHostName = url.Host;
								else
									m_DomainHostName = url.Authority;
							}
							else
								m_DomainHostName = UserHostName;
						else
							m_DomainHostName = Environment.MachineName;
					}
					return m_DomainHostName;
				}
				catch( Exception ex )
				{
					if( m_bShowContextInfoErrors )
						HandleException( new CException( "Retrieve DomainHostName failed", eExceptionType.Warning, ex, eDetailType.Medium ) );
					else
						Log( eSeverity.Info, "Retrieve DomainHostName failed\n" + ex.Message );
					return Environment.MachineName;
				}
			}
		}

		/// <summary> 
		/// Returns domain name in hosted environment corresponding to the current request; otherwise, machine name.
		/// </summary> 
		public string DomainHostNameCurrent
		{
			get
			{
				string domainHostName = "";
				try
				{
					if( IsHosted == true )
						if( HttpContext.Current != null )
						{
							Uri url = HttpContext.Current.Request.Url;
							if( url.Port == 80 || url.Port == 443 )
								domainHostName = url.Host;
							else
								domainHostName = url.Authority;
						}
						else
							domainHostName = UserHostName;
					else
						domainHostName = Environment.MachineName;
					return domainHostName;
				}
				catch( Exception ex )
				{
					if( m_bShowContextInfoErrors )
						HandleException( new CException( "Retrieve current DomainHostName failed", eExceptionType.Warning, ex, eDetailType.Medium ) );
					else
						Log( eSeverity.Info, "Retrieve current DomainHostName failed\n" + ex.Message );
					return Environment.MachineName;
				}
			}
		}


		#endregion

		#region Constructors/Destructors

		// constructors
		protected CApp()
		{
		}

		public CApp( string ApplicationName )
			: this( ApplicationName, new ListDictionary() )
		{
		}

		public CApp( CApp cApp )
			: this( null, cApp, false )
		{
		}

		public CApp( CApp cApp, bool bPassSoftInstance )
			: this( null, cApp, bPassSoftInstance )
		{
		}

		public CApp( string ApplicationName, CApp cApp )
			: this( ApplicationName, cApp, false )
		{
		}

		public CApp( string ApplicationName, CApp cApp, bool bPassSoftInstance )
		{
			try
			{
				m_StateInfo = new ListDictionary();
				if( !string.IsNullOrEmpty( ApplicationName ) )
				{
					m_sAppName = ApplicationName;
					m_StateInfo.Add( "ApplicationName", ApplicationName );
					ConfigurationManager.AppSettings["ApplicationName"] = ApplicationName;
				}
				m_StateChangeEventHandler = new EventHandler( OnStateChange );
				m_bPassSoftInstance = bPassSoftInstance;
				if( cApp != null )
				{
					m_ParentClass = cApp;
					m_ParentClass.StateChange += m_StateChangeEventHandler;
				}
				InitFromInstance( cApp );
			}
			catch( Exception ex )
			{
				try
				{
					HandleException( ex, true, true );
				}
				finally
				{
					Dispose();
				}
			}
		}

		public CApp( string ApplicationName, ListDictionary AdditionalInfo )
		{
			try
			{
				m_StateInfo = new ListDictionary();
				if( !string.IsNullOrEmpty( ApplicationName ) )
				{
					m_sAppName = ApplicationName;
					m_StateInfo.Add( "ApplicationName", ApplicationName );
					ConfigurationManager.AppSettings["ApplicationName"] = ApplicationName;
				}
				m_StateChangeEventHandler = new EventHandler( OnStateChange );

				// copy all other entries from AdditionalInfo
				foreach( object key in AdditionalInfo.Keys )
				{
					if( !m_StateInfo.Contains( key )
						|| ( m_StateInfo.Contains( key ) && (string) key != "ApplicationName" && m_StateInfo[key] != AdditionalInfo[key] ) )
					{
						m_StateInfo.Add( key, AdditionalInfo[key] );
					}
				}

				Initialize( false );
			}
			catch( Exception ex )
			{
				try
				{
					HandleException( ex, true, true );
				}
				finally
				{
					Dispose();
				}
			}
		}

		~CApp()
		{
			Dispose( false );
		}

		#endregion

		#region Protected Events

		protected event EventHandler StateChange
		{
			add { lock( this ) { m_StateChange = m_StateChange + value; } }
			remove { lock( this ) { m_StateChange = m_StateChange - value; } }
		}

		#endregion

		#region Accessors

		public string ApplicationName
		{
			get { AssertNotDisposed(); return m_sAppName; }
		}

		public int ExceptionPoolSize
		{
			get { AssertNotDisposed(); return m_iExceptionPoolSize; }
			set
			{
				AssertNotDisposed();
				m_iExceptionPoolSize = value;
				if( m_ExceptionPool != null && m_iExceptionPoolSize == 0 )
				{
					m_ExceptionPool.Clear();
					m_ExceptionPool = null;
				}
				else if( m_iExceptionPoolSize > 0 )
				{
					if( m_ExceptionPool == null )
						m_ExceptionPool = new ArrayList( ExceptionPoolSize );
					else
						m_ExceptionPool.Capacity = m_iExceptionPoolSize;
				}
			}
		}

		public ListDictionary StateInfo
		{
			get
			{
				AssertNotDisposed();
				if( m_bStateInitialized == null || m_Log == null || m_Mail == null || m_Config == null )
				{
					Initialize( true ); // since we pass the state info to another object, everything should be initialized
				}
				return m_StateInfo;
			}
		}

		public ArrayList Exceptions
		{
			get
			{
				AssertNotDisposed();
				return m_ExceptionPool;
			}
		}

		public Exception LastException
		{
			get
			{
				AssertNotDisposed();
				return m_LastException;
			}
		}

		#endregion

		#region virtualClass Factories

		protected virtual CConfig CreateConfigReader( Type type, params object[] args )
		{
			CConfig _ConcreteConfigReader = null;
			if( type == null ) type = typeof( CIniConfig );

			_ConcreteConfigReader = Activator.CreateInstance( type, args ) as CConfig;
			if( _ConcreteConfigReader == null )
				_ConcreteConfigReader = Activator.CreateInstance( typeof( CIniConfig ), null ) as CConfig;

			return _ConcreteConfigReader;
		}

		protected virtual CLogger CreateLogger( Type type, params object[] args )
		{
			CLogger _ConcreteLogger = null;
			if( type == null ) type = typeof( CFileLogger );

			_ConcreteLogger = Activator.CreateInstance( type, args ) as CLogger;
			if( _ConcreteLogger == null )
			{
				_ConcreteLogger = Activator.CreateInstance( typeof( CFileLogger ), CDefault.defaultLogLocation, false, 0 ) as CLogger;
				//if (_ConcreteLogger == null) _ConcreteLogger = Activator.CreateInstance(typeof(CFileLogger), null) as CLogger;
			}

			return _ConcreteLogger;
		}

		protected virtual CMail CreateMailer( Type type, params object[] args )
		{
			CMail _ConcreteMailer = null;
			if( type == null ) type = typeof( CMail );

			_ConcreteMailer = Activator.CreateInstance( type, args ) as CMail;
			if( _ConcreteMailer == null )
				_ConcreteMailer = Activator.CreateInstance( typeof( CMail ), null ) as CMail;

			return _ConcreteMailer;
		}

		#endregion

		#region Virtual Methods

		public virtual bool Initialize( bool InitializeAll )
		{
			bool bRet = false;
			try
			{
				if( InitializeAll )
				{
					if( m_Config == null ) bRet |= InitConfig();
					if( m_bStateInitialized == null )
					{
						bRet |= InitCommon();
						//m_bStateInitialized = true;
					}
					if( m_Log == null ) bRet |= InitLogger();
					if( m_Mail == null ) bRet |= InitMail();
				}
				else
				{
					if( m_bStateInitialized == null )
					{
						bRet = InitCommon();
						//m_bStateInitialized = true;
					}
				}
			}
			catch( Exception ex )
			{
				m_bStateInitialized = false;
				m_LastException = ex;
				throw;
			}
			return bRet;
		}

		protected virtual bool InitConfig()
		{
			//bool bStateChanged = false;
			string sIniFileName = "";

			if( m_StateInfo != null && m_StateInfo.Contains( "IniFileName" ) )  // from AdditionalInfo
				sIniFileName = (string) m_StateInfo["IniFileName"];
			else
				sIniFileName = ConfigurationManager.AppSettings["IniFileName"];

			if( IsEmpty( sIniFileName ) )
			{
				string sAppName = ConfigurationManager.AppSettings["ApplicationName"];
				string sErrorMsg = "No parameter is provided to identify configuration file. Make sure the following file exists:\r\n"
					+ "====================================================================\r\n\t"
					+ CDefault.defaultLogLocation + sAppName.Replace( ".exe", "'DOT'exe" ) + ".config\r\n"
					+ "====================================================================\r\n\r\n"
					+ "Note that the above configuration file should contain a location of the ini file.\r\n"
					+ "For example the contents of the configuration file may look like this:\r\n\r\n"
					+ "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\r\n"
					+ "<configuration>\r\n"
					+ "\t<appSettings>\r\n"
					+ "\t\t<add key='IniFileName' value='\\filepath\\InvHubConfig.ini'/>\r\n"
					+ "\t</appSettings>\r\n"
					+ "</configuration>\r\n";

				throw new CException( sErrorMsg, eExceptionType.Config );
			}

			bool bActiveConfig = false;
			if( m_StateInfo != null && m_StateInfo.Contains( "ActiveConfig" ) )                     // from AdditionalInfo
				bActiveConfig = (bool) m_StateInfo["ActiveConfig"];
			else if( !IsEmpty( ConfigurationManager.AppSettings["ActiveConfig"] ) )
				bActiveConfig = bool.Parse( ConfigurationManager.AppSettings["ActiveConfig"] );

			Type tConfigType;
			string sConfigType = null;

			//			if (m_StateInfo.Contains("ConfigType"))
			//				sConfigType = (string)m_StateInfo["ConfigType"];
			//			else 
			sConfigType = ConfigurationManager.AppSettings["ConfigType"];

			tConfigType = !IsEmpty( sConfigType ) ? Type.GetType( sConfigType ) : typeof( CIniConfig );

			if( m_Config == null /*|| (m_Config!=null && m_Config.GetType()!=tConfigType)*/)    // 
			{
				m_Config = CreateConfigReader( tConfigType, sIniFileName, bActiveConfig );

				if( m_Config.ActiveConfig )
				{
					m_Config.ConfigRefresh += new EventHandler( OnConfigRefresh );
				}

				m_bStateChanged |= UpdateStateInfo( "Config", m_Config );
				m_bStateChanged |= UpdateStateInfo( "IniFileName", m_Config.Path );
			}

			return m_bStateChanged;
		}

		protected virtual bool InitCommon()
		{
			if( m_bStateInitialized == null || m_bStateInitialized == false )
			{
				string sErrorMsg = "";
				// exception related config entries
				if( !ReadConfig( m_sAppName, "SendExceptionMail", out m_bSendExceptionMail, ref sErrorMsg ) )
					ReadConfig( "General", "SendExceptionMail", out m_bSendExceptionMail, true );
				if( !ReadConfig( m_sAppName, "ExceptionPoolSize", out m_iExceptionPoolSize, ref sErrorMsg ) )
					ReadConfig( "General", "ExceptionPoolSize", out m_iExceptionPoolSize, 0 );
				ExceptionPoolSize = m_iExceptionPoolSize;
				if( !ReadConfig( m_sAppName, "FixedExPoolSize", out m_bFixedExPoolSize, ref sErrorMsg ) )
					ReadConfig( "General", "FixedExPoolSize", out m_bFixedExPoolSize, true );
				if( !ReadConfig( m_sAppName, "ShowContextInfoErrors", out m_bShowContextInfoErrors, ref sErrorMsg ) )
					ReadConfig( "General", "ShowContextInfoErrors", out m_bShowContextInfoErrors, false );
				// method execution with retries
				if( !ReadConfig( m_sAppName, "MaxExecuteSqlRetryCount", out m_maxExecuteSqlRetryCount, ref sErrorMsg ) )
					ReadConfig( "General", "MaxExecuteSqlRetryCount", out m_maxExecuteSqlRetryCount, 5 );
				if( !ReadConfig( m_sAppName, "MaxExecuteSqlRetryDelay", out m_maxExecuteSqlRetryDelay, ref sErrorMsg ) )
					ReadConfig( "General", "MaxExecuteSqlRetryDelay", out m_maxExecuteSqlRetryDelay, 5000 );
				m_bStateInitialized = true;
			}
			return m_bStateChanged;
		}

		protected virtual bool InitLogger()
		{
			string sLogPath = CDefault.defaultLogLocation;
			Type tLoggerType = typeof( CFileLogger );
			string sErrorMsg = null;

			if( m_bStateInitialized != null && m_bStateInitialized == false )
			{
				m_Log = CreateLogger( null, null ); // Logger with default location
				UpdateStateInfo( "Log", m_Log );
				m_bDisposableComponents = true;
			}

			if( !ReadConfig( m_sAppName, "LogPath", out sLogPath, ref sErrorMsg )
				&& !ReadConfig( "General", "LogPath", out sLogPath, ref sErrorMsg )
				&& m_Log == null )
			{
				throw new CException( sErrorMsg );
			}

			VerifyPath( ref sLogPath );
			m_bStateChanged |= UpdateStateInfo( "LogPath", sLogPath );

			sLogPath = sLogPath + m_sAppName + ".log";

			if( !ReadConfig( m_sAppName, "TraceLevel", out int iLogLevel, ref sErrorMsg ) )
				ReadConfig( "General", "TraceLevel", out iLogLevel, (int) CDefault.defaultLogLevel );
			m_LogLevel = (eSeverity) iLogLevel;
			m_bStateChanged |= UpdateStateInfo( "LogLevel", m_LogLevel );

			if( !ReadConfig( m_sAppName, "LogSeparate", out bool bLogSeparate, ref sErrorMsg ) )
				ReadConfig( "General", "LogSeparate", out bLogSeparate, false );
			if( !ReadConfig( m_sAppName, "LogTimeLimit", out int iLogTimeLimit, ref sErrorMsg ) )
				ReadConfig( "General", "LogTimeLimit", out iLogTimeLimit, 0 );
			if( !ReadConfig( m_sAppName, "LoggerType", out string sLoggerType, ref sErrorMsg ) )
				ReadConfig( "General", "LoggerType", out sLoggerType, null );
			tLoggerType = !IsEmpty( sLoggerType ) ? Type.GetType( sLoggerType ) : typeof( CFileLogger );
			if( !ReadConfig( m_sAppName, "LogEntryWithMethodName", out m_iExceptionPoolSize, ref sErrorMsg ) )
				ReadConfig( "General", "LogEntryWithMethodName", out m_bLogEntryWithMethodName, false );

			if( m_Log == null || ( m_Log != null && m_Log.GetType() != tLoggerType ) )
			{
				m_Log = CreateLogger( tLoggerType, sLogPath, bLogSeparate, iLogTimeLimit );
				m_bStateChanged |= UpdateStateInfo( "Log", m_Log );
			}

			string sConfig = "Config file:\r\n" +
							"\t\t\t\t\t\t\t\t\tDefined: '" + ConfigurationManager.AppSettings["IniFileName"] + "'";
			if( m_Config.GetType() == typeof( CIniConfig ) )
				sConfig += "\r\n\t\t\t\t\t\t\t\t\tUsed:    '" + ( m_Config as CIniConfig ).Path + "'";
			Log( eSeverity.Trace, sConfig );

			string ExecutingAssembly = null;
			string ExecutingAssemblyVersion = null;
			string CodeBase = null;
			if( System.Reflection.Assembly.GetExecutingAssembly() != null )
			{
				ExecutingAssembly = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
				ExecutingAssemblyVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
				CodeBase = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
			}
			string sBases = ""
				+ "\tExecutingAssembly = '" + ExecutingAssembly + "'\r\n"
				+ "\t\t\t\t\t\t\t\t\tExecutingAssemblyVersion = '" + ExecutingAssemblyVersion + "'\r\n"
				+ "\t\t\t\t\t\t\t\t\tCodeBase = '" + CodeBase + "'\r\n"
				+ "\t\t\t\t\t\t\t\t\tBaseDirectory = '" + CDefault.defaultLogLocation;
			Log( eSeverity.Trace, sBases );

			return m_bStateChanged;
		}

		protected virtual bool InitMail()
		{
			string sErrorMsg = null;

			// in all cases below sErrorMsg is ignored as next statement provides at least a default value
			if( !ReadConfig( m_sAppName, "MailTo", out m_sErrorMailTo, ref sErrorMsg ) )
				ReadConfig( "Mail", "MailTo", out m_sErrorMailTo, CDefault.defaultMailTo );
			m_bStateChanged |= UpdateStateInfo( "ErrorMailTo", m_sErrorMailTo );

			if( !ReadConfig( m_sAppName, "MailFrom", out m_sErrorMailFrom, ref sErrorMsg ) )
				ReadConfig( "Mail", "MailFrom", out m_sErrorMailFrom, CDefault.defaultMailFrom );
			m_bStateChanged |= UpdateStateInfo( "ErrorMailFrom", m_sErrorMailFrom );

			if( !ReadConfig( m_sAppName, "MailCC", out m_sErrorMailCc, ref sErrorMsg ) )
				ReadConfig( "Mail", "MailCC", out m_sErrorMailCc, CDefault.defaultMailCc );
			m_bStateChanged |= UpdateStateInfo( "ErrorMailCc", m_sErrorMailCc );

			if( !ReadConfig( m_sAppName, "MailServerName", out string sSmtpServer, ref sErrorMsg ) )
				ReadConfig( "Mail", "MailServerName", out sSmtpServer, CDefault.defaultSMTPServer );

			if( !ReadConfig( m_sAppName, "MailTemporaryFolder", out string sMailTemporaryFolder, ref sErrorMsg ) )
				ReadConfig( "Mail", "MailTemporaryFolder", out sMailTemporaryFolder, CDefault.defaultLogLocation );

			if( !ReadConfig( m_sAppName, "MailType", out string sMailType, ref sErrorMsg ) )
				ReadConfig( "Mail", "MailType", out sMailType, ref sErrorMsg );

			if( !ReadConfig( m_sAppName, "CanSendMail", out m_bCanSendMail, ref sErrorMsg ) )
				ReadConfig( "Mail", "CanSendMail", out m_bCanSendMail, true );

			Type tMailerType;
			tMailerType = !IsEmpty( sMailType ) ? Type.GetType( sMailType ) : typeof( CMail );

			if( m_Mail == null /*|| (m_Mail!=null && m_Mail.GetType()!=tMailerType)*/ )
			{
				m_Mail = CreateMailer( tMailerType, sSmtpServer, this, m_bCanSendMail );
				m_bStateChanged |= UpdateStateInfo( "Mail", m_Mail );
				m_Mail.MailTemporaryFolder = sMailTemporaryFolder;
			}

			return m_bStateChanged;
		}

		protected virtual bool InitFromInstance( object capp )
		{
			// deep copy of m_StateInfo
			foreach( DictionaryEntry de in ( (CApp) capp ).StateInfo )
				UpdateStateInfo( (string) de.Key, de.Value );

			// shallow copy of all class fields
			FieldInfo[] cappFields = capp.GetType().GetFields( BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance );
			foreach( FieldInfo fi in cappFields )
			{
				// preserve instance-specific fields
				if( !( fi.GetCustomAttributes( typeof( InstanceAttribute ), true ).Length > 0 ||
					 ( fi.GetCustomAttributes( typeof( SoftInstanceAttribute ), true ).Length > 0 && !m_bPassSoftInstance ) )
					&& this.GetType().GetField( fi.Name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance ) != null
					&& ( fi.DeclaringType == this.GetType() || this.GetType().IsSubclassOf( fi.DeclaringType )/*fi.DeclaringType.IsSubclassOf(this.GetType())*/) )
				{
					fi.SetValue( this, fi.GetValue( capp ) );
				}
			}

			if( this.GetType() != capp.GetType() )
				InitCommon();

			return true;
		}


		public virtual bool LogFlush()
		{
			AssertNotDisposed();
			if( m_Log != null )
				return m_Log.Flush();
			else
				return false;
		}

		protected virtual void OnStateChange( object sender, EventArgs args )
		{
			try
			{
				InitFromInstance( (CApp) sender );
				Log( eSeverity.Trace, this.GetType().ToString() + ".OnStateChange() Success!" );
				m_bStateChanged = false;
			}
			catch( Exception ex )
			{
				HandleException( ex );
			}
		}

		protected virtual void OnConfigRefresh( object sender, EventArgs args )
		{
			try
			{
				Log( eSeverity.Trace, "The config file has been changed." );
				Thread.Sleep( 250 );  // wait until file handle is released
				m_bStateChanged = false;
				m_bStateChanged |= InitCommon();
				m_bStateChanged |= InitLogger();
				m_bStateChanged |= InitMail();
				lock( this )
				{
					if( m_bStateChanged && m_StateChange != null ) m_StateChange( this, null ); // fire StateChangedEvent
				}
				m_bStateChanged = false;
			}
			catch( Exception ex )
			{
				// Downgrade the error to warning
				CException cex = new CException( "Error in OnConfigRefresh", eExceptionType.Warning, ex, eDetailType.Full );
				HandleException( cex );
			}
		}

		#endregion

		#region Public ReadConfig wrappers

		public bool ReadConfig( string Section, string Key, out string Result )
		{
			AssertNotDisposed();
			if( m_Config == null ) InitConfig();
			string Value = "", EncryptedResult = "";
			bool bRet = m_Config.GetValue( Section, Key, out Result );
			if( Result == null )
				Value = "null";
			else
				if( Key.ToLower().EndsWith( "*" ) )
			{
				EncryptedResult = Result;
				Result = DecryptWithHubKey( Result );
				Value = EncryptedResult;
			}
			else
				Value = Result;
			if( m_Log != null ) Log( eSeverity.Deep, "Read Config by class: " + GetType().FullName + ", Section: '" + Section + "', Key: '" + Key + "', Value: '" + Value + "'" );
			m_bStateChanged |= UpdateStateInfo( Key, Result );
			return bRet;
		}

		public bool ReadConfig( string Section, string Key, out int Result )
		{
			AssertNotDisposed();
			if( m_Config == null ) InitConfig();
			bool bRet = m_Config.GetValue( Section, Key, out Result );
			if( m_Log != null ) Log( eSeverity.Deep, "Read Config by class: " + GetType().FullName + ", Section: '" + Section + "', Key: '" + Key + "', Value: '" + Result.ToString() + "'" );
			m_bStateChanged |= UpdateStateInfo( Key, Result );
			return bRet;
		}

		public bool ReadConfig( string Section, string Key, out float Result )
		{
			AssertNotDisposed();
			if( m_Config == null ) InitConfig();
			bool bRet = m_Config.GetValue( Section, Key, out Result );
			if( m_Log != null ) Log( eSeverity.Deep, "Read Config by class: " + GetType().FullName + ", Section: '" + Section + "', Key: '" + Key + "', Value: '" + Result.ToString() + "'" );
			m_bStateChanged |= UpdateStateInfo( Key, Result );
			return bRet;
		}

		public bool ReadConfig( string Section, string Key, out bool Result )
		{
			AssertNotDisposed();
			if( m_Config == null ) InitConfig();
			bool bRet = m_Config.GetValue( Section, Key, out Result );
			if( m_Log != null ) Log( eSeverity.Deep, "Read Config by class: " + GetType().FullName + ", Section: '" + Section + "', Key: '" + Key + "', Value: '" + Result.ToString() + "'" );
			m_bStateChanged |= UpdateStateInfo( Key, Result );
			return bRet;
		}

		public bool ReadConfig( string Section, string Key, out string Result, object Default )
		{
			AssertNotDisposed();
			string sErrorMessage = null;
			string Value = "", EncryptedResult = "";
			if( m_Config == null ) InitConfig();
			bool bRet = m_Config.GetValue( Section, Key, out Result, Default, ref sErrorMessage );
			if( Result == null )
				Value = "null";
			else
				if( Key.ToLower().EndsWith( "*" ) && !string.IsNullOrEmpty( Result ) )
			{
				EncryptedResult = Result;
				Result = DecryptWithHubKey( Result );
				Value = EncryptedResult;
			}
			else
				Value = Result;
			if( m_Log != null ) Log( eSeverity.Deep, "Read Config by class: " + GetType().FullName + ", Section: '" + Section + "', Key: '" + Key + "', Value: '" + Value + "'" );
			m_bStateChanged |= UpdateStateInfo( Key, Result );
			return bRet;
		}

		public bool ReadConfig( string Section, string Key, out int Result, object Default )
		{
			AssertNotDisposed();
			string sErrorMessage = null;
			if( m_Config == null ) InitConfig();
			bool bRet = m_Config.GetValue( Section, Key, out Result, Default, ref sErrorMessage );
			if( m_Log != null ) Log( eSeverity.Deep, "Read Config by class: " + GetType().FullName + ", Section: '" + Section + "', Key: '" + Key + "', Value: '" + Result.ToString() + "'" );
			m_bStateChanged |= UpdateStateInfo( Key, Result );
			return bRet;
		}

		public bool ReadConfig( string Section, string Key, out float Result, object Default )
		{
			AssertNotDisposed();
			string sErrorMessage = null;
			if( m_Config == null ) InitConfig();
			bool bRet = m_Config.GetValue( Section, Key, out Result, Default, ref sErrorMessage );
			if( m_Log != null ) Log( eSeverity.Deep, "Read Config by class: " + GetType().FullName + ", Section: '" + Section + "', Key: '" + Key + "', Value: '" + Result.ToString() + "'" );
			m_bStateChanged |= UpdateStateInfo( Key, Result );
			return bRet;
		}

		public bool ReadConfig( string Section, string Key, out bool Result, object Default )
		{
			AssertNotDisposed();
			string sErrorMessage = null;
			if( m_Config == null ) InitConfig();
			bool bRet = m_Config.GetValue( Section, Key, out Result, Default, ref sErrorMessage );
			if( m_Log != null ) Log( eSeverity.Deep, "Read Config by class: " + GetType().FullName + ", Section: '" + Section + "', Key: '" + Key + "', Value: '" + Result.ToString() + "'" );
			m_bStateChanged |= UpdateStateInfo( Key, Result );
			return bRet;
		}

		public bool ReadConfig( string Section, string Key, out string Result, ref string ErrorMessage )
		{
			AssertNotDisposed();
			if( m_Config == null ) InitConfig();
			string Value = "", EncryptedResult = "";
			bool bRet = m_Config.GetValue( Section, Key, out Result, null, ref ErrorMessage );
			if( Result == null )
				Value = "null";
			else
				if( Key.ToLower().EndsWith( "*" ) )
			{
				EncryptedResult = Result;
				Result = DecryptWithHubKey( Result );
				Value = EncryptedResult;
			}
			else
				Value = Result;
			if( m_Log != null ) Log( eSeverity.Deep, "Read Config by class: " + GetType().FullName + ", Section: '" + Section + "', Key: '" + Key + "', Value: '" + Value + "'" );
			m_bStateChanged |= UpdateStateInfo( Key, Result );
			return bRet;
		}

		public bool ReadConfig( string Section, string Key, out int Result, ref string ErrorMessage )
		{
			AssertNotDisposed();
			if( m_Config == null ) InitConfig();
			bool bRet = m_Config.GetValue( Section, Key, out Result, null, ref ErrorMessage );
			if( m_Log != null ) Log( eSeverity.Deep, "Read Config by class: " + GetType().FullName + ", Section: '" + Section + "', Key: '" + Key + "', Value: '" + Result.ToString() + "'" );
			m_bStateChanged |= UpdateStateInfo( Key, Result );
			return bRet;
		}

		public bool ReadConfig( string Section, string Key, out float Result, ref string ErrorMessage )
		{
			AssertNotDisposed();
			if( m_Config == null ) InitConfig();
			bool bRet = m_Config.GetValue( Section, Key, out Result, null, ref ErrorMessage );
			if( m_Log != null ) Log( eSeverity.Deep, "Read Config by class: " + GetType().FullName + ", Section: '" + Section + "', Key: '" + Key + "', Value: '" + Result.ToString() + "'" );
			m_bStateChanged |= UpdateStateInfo( Key, Result );
			return bRet;
		}

		public bool ReadConfig( string Section, string Key, out bool Result, ref string ErrorMessage )
		{
			AssertNotDisposed();
			if( m_Config == null ) InitConfig();
			bool bRet = m_Config.GetValue( Section, Key, out Result, null, ref ErrorMessage );
			if( m_Log != null ) Log( eSeverity.Deep, "Read Config by class: " + GetType().FullName + ", Section: '" + Section + "', Key: '" + Key + "', Value: '" + Result.ToString() + "'" );
			m_bStateChanged |= UpdateStateInfo( Key, Result );
			return bRet;
		}

		#endregion

		#region Public HandleException wrappers

		public void HandleSystemException( string sMsg, Exception ex )
		{
			// HandleSystemException is top level handler, does not re-throw
			if( ex is CException )
			{
				Log( sMsg );
				HandleException( ex, false );
			}
			else
			{
				CException cex = new CException( sMsg + "\r\n" + ex.Message, eExceptionType.System, ex );
				HandleException( cex, false );
			}
		}

		public void HandleException( Exception ex, object RethrowRequired = null, object SendEmail = null, object Log = null, ExceptionInfo[] Filter = null )
		{
			HandleExceptionImpl( ex, RethrowRequired, SendEmail, Log, Filter );
		}

		public void HandleExceptionAsync( Exception ex, object RethrowRequired = null, object SendEmail = null, object Log = null, ExceptionInfo[] Filter = null )
		{
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
			GenericBackgroundInvoke.QueueBackgroundWorkItem( async cancellationToken =>
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
			{
				HandleExceptionImpl( ex, RethrowRequired, SendEmail, Log, Filter );
			} );
		}

		// actual implementation

		protected virtual void HandleExceptionImpl( Exception ex, object RethrowRequired, object SendEmail, object Log, ExceptionInfo[] Filter )
		{
			bool bPooled = false;
			eExceptionType exceptiontype;

			AssertNotDisposed();
			try
			{
				if( Filter != null && Filter.Length > 0 )
				{
					for( int i = 0; i < Filter.Length; i++ )
					{
						int HResult = Convert.ToInt32( ex.GetType().GetProperty( "HResult", BindingFlags.NonPublic | BindingFlags.Instance ).GetValue( ex, null ) );
						if( HResult == Filter[i].HResult || ex.GetType() == Filter[i].Type )
						{
							ex = new CException( IsEmpty( Filter[i].NewMessage ) ? ex.Message : Filter[i].NewMessage, Filter[i].NewType, ex );
						}
					}
				}

				exceptiontype = ex.GetType() == typeof( CException ) ? ( (CException) ex ).ExceptionType : eExceptionType.System;
				CLogEntry logEntry = BuildEntry( ex );

				// log exception if required 
				if( ( Log != null && (bool) Log ) || ( Log == null && CExceptionMap.LogRequired( exceptiontype ) ) )
				{
					if( m_Log == null ) InitLogger();
					m_Log.Write( logEntry );
					LogFlush();
					///if (m_Log!=null) m_Log.Write(logEntry);
				}

				// keep exception in the pool
				if( m_ExceptionPool != null )
				{
					lock( m_ExceptionPool.SyncRoot )
					{
						if( m_iExceptionPoolSize > 0 && m_bFixedExPoolSize
							&& m_ExceptionPool.Count == m_iExceptionPoolSize )
							m_ExceptionPool.RemoveAt( 0 );  // if the limit exceeded, reuse items

						m_ExceptionPool.Add( ex );
						bPooled = true;
					}
				}
				//else	// save last exception
				//{
				m_LastException = ex;
				//}

				if( !bPooled )
				{
					// email exception if required
					if( ( SendEmail != null && (bool) SendEmail ) ||
						( SendEmail == null && CExceptionMap.EmailRequired( exceptiontype ) && m_bSendExceptionMail ) )
					{
						if( m_Log == null ) InitLogger();
						if( m_Mail == null ) InitMail();
						//logEntry.Message = logEntry.Message + "\r\n\r\nCheck log file: <" + m_Log.LogPath + ">\r\n";
						logEntry.Message = logEntry.Message + GetFormatter( exceptiontype ).GetFooter( ex );
						m_Mail.Send( m_sErrorMailFrom, m_sErrorMailTo, m_sErrorMailCc, logEntry );
					}
				}
			}

			catch( Exception newEx )
			{
				m_LastException = ex;
				CLogEntry logEntry;

				if( m_Log == null ) // what if it's just not initialized yet?
				{
					m_Log = CreateLogger( null, null ); // Logger with default location
					m_bStateChanged |= UpdateStateInfo( "Log", m_Log );
					m_bDisposableComponents = true;
				}
				// handle manually
				if( m_LastException == null )
				{
					// use local exception
					exceptiontype = newEx.GetType() == typeof( CException ) ? ( (CException) newEx ).ExceptionType : eExceptionType.System;
					logEntry = BuildEntry( newEx );
				}
				else
				{
					// use original exception
					exceptiontype = ex.GetType() == typeof( CException ) ? ( (CException) ex ).ExceptionType : eExceptionType.System;
					logEntry = BuildEntry( ex );
				}
				m_Log.Write( logEntry );

				if( SendEmail == null || ( SendEmail != null && (bool) SendEmail ) )
				{
					if( m_Mail == null )
					{
						m_Mail = new CMail( this ); // mailer with default SMTP server
						m_bStateChanged |= UpdateStateInfo( "Mail", m_Mail );
						m_bDisposableComponents = true;
					}
					if( IsEmpty( m_sErrorMailFrom ) ) m_sErrorMailFrom = CDefault.defaultMailFrom;
					if( IsEmpty( m_sErrorMailTo ) ) m_sErrorMailTo = CDefault.defaultMailTo;
					if( m_sErrorMailCc == null ) m_sErrorMailCc = CDefault.defaultMailCc;
					if( m_sErrorMailFrom == null ) m_sErrorMailFrom = CDefault.defaultMailFrom;

					logEntry.Message = logEntry.Message + GetFormatter( exceptiontype ).GetFooter( ex );
					m_Mail.Send( m_sErrorMailFrom, m_sErrorMailTo, m_sErrorMailCc, logEntry );
				}
			}

			// re-throw exception if required
			if( !bPooled )
			{
				if( ( RethrowRequired != null && (bool) RethrowRequired )
					|| ( RethrowRequired == null && CExceptionMap.RethrowRequired( exceptiontype ) ) )
				{
					//throw new CException( "Exception re-throw. Please see inner exception for details.", exceptiontype, ex );
					throw ex;
				}
			}

		}

		protected virtual CLogEntry BuildEntry( Exception ex )
		{
			CLogEntry logEntry = new CLogEntry();
			CExceptionFormatter formatter;

			logEntry.Priority = CDefault.minimumPriority;
			logEntry.EventId = CDefault.defaultEventID;

			if( ex is CException )
				logEntry.Severity = CExceptionMap.Severity( ( (CException) ex ).ExceptionType );
			else
				logEntry.Severity = eSeverity.Error;

			eExceptionType exType = ex.GetType() == typeof( CException ) ? ( (CException) ex ).ExceptionType : eExceptionType.System;
			formatter = GetFormatter( exType );

			logEntry.Title = formatter != null ? formatter.GetCaption( ex, DomainHostNameCurrent ) : ex.ToString();
			logEntry.Message = formatter != null ? formatter.GetMessage( ex ) : ex.Message;

			return logEntry;
		}

		protected virtual CExceptionFormatter GetFormatter( eExceptionType exceptionType )
		{
			CExceptionFormatter concreteFormatter = null;
			if( m_ExceptionFormatters == null ) m_ExceptionFormatters = new ListDictionary();

			lock( m_ExceptionFormatters.SyncRoot )
			{
				if( m_ExceptionFormatters.Contains( exceptionType ) )
					concreteFormatter = (CExceptionFormatter) m_ExceptionFormatters[exceptionType];
				else
				{
					object[] args;
					args = new object[1] { (object) m_StateInfo };
					concreteFormatter = Activator.CreateInstance( CExceptionMap.Formatter( exceptionType ), args ) as CExceptionFormatter;
					m_ExceptionFormatters.Add( exceptionType, concreteFormatter );
				}
			}
			return concreteFormatter;
		}

		#endregion

		#region Public Log Wrappers

		public void Log( params string[] Messages )
		{
			AssertNotDisposed();
			if( m_Log == null )
				InitLogger();

			if( Messages.Length == 1 )
			{
				m_Log.Write( Messages[0] );
			}
			else
			{
				StringBuilder text = new StringBuilder();
				foreach( string message in Messages )
				{
					text.Append( message );
					text.Append( "\t" );
				}
				m_Log.Write( text.ToString() );
			}
		}

		public void Log( eSeverity LogLevel, params string[] Messages )
		{
			AssertNotDisposed();
			if( m_Log == null )
				InitLogger();

			if( LogLevel > m_LogLevel ) return;
			if( m_bLogEntryWithMethodName && LogLevel >= eSeverity.Trace )
			{
				StackFrame stFrame = new StackFrame( 1, true );
				string sMethodName = stFrame.GetMethod().DeclaringType + "." + stFrame.GetMethod().Name + "()";
				if( Messages.Length > 0 )
					Messages[0] = sMethodName + "\t" + Messages[0];
			}
			Log( Messages );
		}

		#endregion

		#region Public SendMail Wrappers
		public void SendMailNotification( string Subject, string Body, string Division = null, bool IsHtml = false, List<string> Attachments = null, string recipient = null, string[] attachmentsInlineIDs = null )
		{
			// no need to await
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
			GenericBackgroundInvoke.QueueBackgroundWorkItem( ct => SendMailNotificationImpl( Subject, Body, Division, IsHtml, Attachments, recipient, attachmentsInlineIDs ) );
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
		}

		// no need to await
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
		private async Task SendMailNotificationImpl( string Subject, string Body, string Division = null, bool IsHtml = false, List<string> Attachments = null, string recipient = null, string[] attachmentsInlineIDs = null )
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
		{
			try
			{
				string errMessage = "";
				string division = null;

				if( string.IsNullOrEmpty( m_NotificationServer ) )
					if( !ReadConfig( ApplicationName, "Server", out m_NotificationServer, ref errMessage ) )
						if( !ReadConfig( "Notification", "Server", out m_NotificationServer, ref errMessage ) )
							throw new CException( errMessage, eExceptionType.Config, eDetailType.Brief );
				if( string.IsNullOrEmpty( m_NotificationFrom ) )
					if( !ReadConfig( ApplicationName, "From", out m_NotificationFrom, ref errMessage ) )
						if( !ReadConfig( "Notification", "From", out m_NotificationFrom, ref errMessage ) )
							throw new CException( errMessage, eExceptionType.Config, eDetailType.Brief );
				if( string.IsNullOrEmpty( m_NotificationTo ) )
					if( !ReadConfig( ApplicationName, "To", out m_NotificationTo, ref errMessage ) )
						if( !ReadConfig( "Notification", "To", out m_NotificationTo, ref errMessage ) )
							throw new CException( errMessage, eExceptionType.Config, eDetailType.Brief );
				if( m_NotificationCC == null )
					if( !ReadConfig( ApplicationName, "CC", out m_NotificationCC, ref errMessage ) )
						ReadConfig( "Notification", "CC", out m_NotificationCC, "" );
				if( string.IsNullOrEmpty( m_NotificationOperations ) )
					if( !ReadConfig( ApplicationName, "Operations", out m_NotificationOperations, ref errMessage ) )
						if( !ReadConfig( "Notification", "Operations", out m_NotificationOperations, ref errMessage ) )
							throw new CException( errMessage, eExceptionType.Config, eDetailType.Brief );
				if( string.IsNullOrEmpty( m_NotificationService ) )
					if( !ReadConfig( ApplicationName, "Service", out m_NotificationService, ref errMessage ) )
						if( !ReadConfig( "Notification", "Service", out m_NotificationService, ref errMessage ) )
							throw new CException( errMessage, eExceptionType.Config, eDetailType.Brief );
				if( string.IsNullOrEmpty( m_NotificationSupport ) )
					if( !ReadConfig( ApplicationName, "Support", out m_NotificationSupport, ref errMessage ) )
						if( !ReadConfig( "Notification", "Support", out m_NotificationSupport, ref errMessage ) )
							throw new CException( errMessage, eExceptionType.Config, eDetailType.Brief );
				if( string.IsNullOrEmpty( m_NotificationSite ) )
					if( !ReadConfig( ApplicationName, "Site*", out m_NotificationSite, ref errMessage ) )
						if( !ReadConfig( "Notification", "Site*", out m_NotificationSite, ref errMessage ) )
							throw new CException( errMessage, eExceptionType.Config, eDetailType.Brief );

				if( Division != null )
				{
					switch( Division )
					{
						case "Operations":
							division = m_NotificationOperations;
							break;
						case "Service":
							division = m_NotificationService;
							break;
						case "Support":
							division = m_NotificationSupport;
							break;
					}
				}
				string attachments = "", delimiter = "";
				if( Attachments != null && Attachments.Count > 0 )
				{
					foreach( string attach in Attachments )
					{
						string file = attach.Replace( "/", @"\" );
						FileInfo oFile = new FileInfo( file );
						if( oFile.Exists )
						{
							if( !string.IsNullOrEmpty( attachments ) )
								delimiter = ",";
							attachments += delimiter + file;
						}
						else
							Log( "Attachment file '" + file + "' doesn't exist" );
					}
				}

				string To = Division == null ? m_NotificationTo : division + ";" + m_NotificationTo;

				if( recipient != null )
					To = recipient;

				Log( eSeverity.Debug, "SendMailNotification to: " + To );
				SendMail( Division == null ? m_NotificationFrom : division,
								To, m_NotificationCC, Subject, Body, attachments, m_NotificationServer, IsHtml, attachmentsInlineIDs );
			}
			catch( Exception ex )
			{
				HandleException( new CException( "SendMailNotification failed. Mail Subject:\t" + Subject + "\r\nBody:\t" + Body + "\r\nError Message\t" + ex.Message, eExceptionType.Process, eDetailType.Brief ) );
			}
			return;
		}

		public void SendMail( Exception ex )
		{
			AssertNotDisposed();
			if( m_Log == null ) InitLogger();
			if( m_Mail == null ) InitMail();
			CLogEntry logEntry = BuildEntry( ex );
			eExceptionType exceptiontype = ex.GetType() == typeof( CException ) ? ( (CException) ex ).ExceptionType : eExceptionType.System;
			logEntry.Message = logEntry.Message + GetFormatter( exceptiontype ).GetFooter( ex );
			m_Mail.Send( m_sErrorMailFrom, m_sErrorMailTo, m_sErrorMailCc, logEntry );
		}

		public void SendMail( string subject, string messageText, string attachments )
		{
			AssertNotDisposed();
			if( m_Mail == null ) InitMail();
			m_Mail.Send( m_sErrorMailFrom, m_sErrorMailTo, m_sErrorMailCc, subject, messageText, attachments );
		}

		public void SendMail( string from, string to, string cc, string subject, string messageText, string attachments )
		{
			AssertNotDisposed();
			if( m_Mail == null ) InitMail();
			m_Mail.Send( from, to, cc, subject, messageText, attachments );
		}

		public void SendMail( string from, string to, string cc, string subject, string messageText, string attachments, string serverName )
		{
			AssertNotDisposed();
			if( m_Mail == null ) InitMail();
			m_Mail.SmtpServer = serverName;
			m_Mail.Send( from, to, cc, subject, messageText, attachments );
		}

		public void SendMail( string from, string to, string cc, string subject, string messageText, string attachments, string serverName, bool bodyhtml )
		{
			AssertNotDisposed();
			if( m_Mail == null ) InitMail();
			m_Mail.SmtpServer = serverName;
			m_Mail.Send( from, to, cc, subject, messageText, attachments, bodyhtml );
		}

		public void SendMail( string from, string to, string cc, string subject, string messageText, string attachments, string serverName, bool bodyhtml, string[] attachmentsInlineIDs )
		{
			AssertNotDisposed();
			if( m_Mail == null ) InitMail();
			m_Mail.SmtpServer = serverName;
			m_Mail.Send( from, to, cc, subject, messageText, attachments, bodyhtml, attachmentsInlineIDs );
		}

		public void SendMail( string from, string to, string cc, string bcc, string subject, string messageText, string attachments, string serverName, bool bodyhtml, System.Net.Mail.MailPriority priority )
		{
			AssertNotDisposed();
			if( m_Mail == null ) InitMail();
			m_Mail.SmtpServer = serverName;
			m_Mail.Send( from, to, cc, bcc, subject, messageText, attachments, bodyhtml, priority );
		}

		public virtual void SendMail( ArrayList Exceptions )
		{
			AssertNotDisposed();
			if( m_Log == null ) InitLogger();
			if( m_Mail == null ) InitMail();
			if( Exceptions != null && Exceptions.Count > 0 )
			{
				Exception exWorst = null;
				eExceptionType exType;
				eExceptionType exWorstType = eExceptionType.None;
				StringBuilder sExceptionsMsg = new StringBuilder();
				ArrayList allExceptions = Exceptions;
				sExceptionsMsg.Append( "The following errors occured:\r\n" );

				foreach( Exception ex in allExceptions )
				{
					sExceptionsMsg.Append( "\t" + ex.Message + "\r\n" );
					exType = ex.GetType() == typeof( CException ) ? ( (CException) ex ).ExceptionType : eExceptionType.System;
					if( exType > exWorstType )
					{
						exWorstType = exType;
						exWorst = ex;
					}
				}

				CExceptionFormatter exFormatter = GetFormatter( exWorstType );
				sExceptionsMsg.Append( exFormatter.GetFooter( exWorst ) );

				m_Mail.Send( m_sErrorMailFrom, m_sErrorMailTo, m_sErrorMailCc, exFormatter.GetCaption( exWorst ) + " - " + m_sAppName, sExceptionsMsg.ToString(), null );
			}
		}

		#endregion

		#region Public Methods

		public void Close()
		{
			Dispose();
		}

		public void SetExceptionsType( eExceptionType eType )
		{
			if( Exceptions != null && Exceptions.Count > 0 )
			{
				// downgrade exception in its severity to no more then eType
				for( int i = 0; i < Exceptions.Count; i++ )
				{
					Exception ex = (Exception) Exceptions[i];
					if( ex is CException )
					{
						if( ( (CException) ex ).ExceptionType > eType )
							( (CException) ex ).ExceptionType = eType;
					}
					else
					{
						// package standard Exception into CException
						CException cex = new CException( ex.Message, eType, ex, eDetailType.Full );
						Exceptions.RemoveAt( i );
						Exceptions.Insert( i, cex );
					}
				}
			}
		}

		public string EncryptWithKey( string sInput, byte[] Key, byte[] InitVector )
		{
			try
			{
				byte[] byInput = Encoding.UTF8.GetBytes( sInput );
				MemoryStream MemStream = new MemoryStream();
				using( SymmetricAlgorithm Algorithm = Rijndael.Create() )
				using( ICryptoTransform Encryptor = Algorithm.CreateEncryptor( Key, InitVector ) )
				using( CryptoStream CryptoStream = new CryptoStream( MemStream, Encryptor, CryptoStreamMode.Write ) )
					CryptoStream.Write( byInput, 0, byInput.Length );
				return Convert.ToBase64String( MemStream.ToArray() );
			}
			catch( Exception ex )
			{
				throw ( new CException( "Encryption of string '" + sInput + "' failed.", eExceptionType.Config, ex, eDetailType.Brief ) );
			}
		}

		public string DecryptWithKey( string sInput, byte[] Key, byte[] InitVector )
		{
			try
			{
				byte[] byInput = Convert.FromBase64String( sInput );
				MemoryStream MemStream = new MemoryStream();
				using( SymmetricAlgorithm Algorithm = Rijndael.Create() )
				using( ICryptoTransform Decryptor = Algorithm.CreateDecryptor( Key, InitVector ) )
				using( CryptoStream CryptoStream = new CryptoStream( MemStream, Decryptor, CryptoStreamMode.Write ) )
					CryptoStream.Write( byInput, 0, byInput.Length );
				return Encoding.UTF8.GetString( MemStream.ToArray() );
			}
			catch( Exception ex )
			{
				throw ( new CException( "Decryption of string '" + sInput + "' failed.", eExceptionType.Config, ex, eDetailType.Brief ) );
			}
		}

		public string EncryptWithHubKey( string sInput )
		{
			return EncryptWithKey( sInput, BkgColor, BkgColor );
		}

		public string DecryptWithHubKey( string sInput )
		{
			return DecryptWithKey( sInput, BkgColor, BkgColor );
		}

		public void ExecuteSqlWithRetry( Action action )
		{
			var retryCount = 0;

			for(; ; )
			{
				try
				{
					action();
					break;
				}
				catch( SqlException ex )
				{
					if( Enum.IsDefined( typeof( RetryableSqlErrors ), ex.Number ) )
						Log( string.Format( "Retrying {0} #{1}) after SQL error:  Number = {2},  Message = {3},  GetType = {4}", action.Method.Name, retryCount, ex.Number, ex.Message, ex.GetType() ) );
					else
						throw;

					if( ++retryCount > m_maxExecuteSqlRetryCount )
						throw;

					Thread.Sleep( m_maxExecuteSqlRetryDelay );
				}
				catch( CException ex )
				{
					if( ex.InnerException != null && ex.InnerException is SqlException )
					{
						if( Enum.IsDefined( typeof( RetryableSqlErrors ), ex.HResult ) )
							Log( string.Format( "Retrying {0} {1} after SQL error:  HResult = {2},  Message = {3},  GetType = {4}", action.Method.Name, retryCount, ex.HResult, ex.InnerException.Message + "\r\n" + ex.Message, ex.GetType() ) );
						else
							throw;

						if( ++retryCount > m_maxExecuteSqlRetryCount )
							throw;

						Thread.Sleep( m_maxExecuteSqlRetryDelay );
					}
					else
						throw;
				}
			}
		}

		public void ExecuteSqlWithRetry<T>( Action<T> action, T t )
		{
			var retryCount = 0;

			for(; ; )
			{
				try
				{
					action( t );
					break;
				}
				catch( SqlException ex )
				{
					if( Enum.IsDefined( typeof( RetryableSqlErrors ), ex.Number ) )
						Log( string.Format( "Retrying {0} #{1}) after SQL error:  Number = {2},  Message = {3},  GetType = {4}", action.Method.Name, retryCount, ex.Number, ex.Message, ex.GetType() ) );
					else
						throw;

					if( ++retryCount > m_maxExecuteSqlRetryCount )
						throw;

					Thread.Sleep( m_maxExecuteSqlRetryDelay );
				}
				catch( CException ex )
				{
					if( ex.InnerException != null && ex.InnerException is SqlException )
					{
						if( Enum.IsDefined( typeof( RetryableSqlErrors ), ex.HResult ) )
							Log( string.Format( "Retrying {0} {1} after SQL error:  HResult = {2},  Message = {3},  GetType = {4}", action.Method.Name, retryCount, ex.HResult, ex.InnerException.Message + "\r\n" + ex.Message, ex.GetType() ) );
						else
							throw;

						if( ++retryCount > m_maxExecuteSqlRetryCount )
							throw;

						Thread.Sleep( m_maxExecuteSqlRetryDelay );
					}
					else
						throw;
				}
			}
		}

		public void ExecuteSqlWithRetry<T1, T2>( Action<T1, T2> action, T1 t1, T2 t2 )
		{
			var retryCount = 0;

			for(; ; )
			{
				try
				{
					action( t1, t2 );
					break;
				}
				catch( SqlException ex )
				{
					if( Enum.IsDefined( typeof( RetryableSqlErrors ), ex.Number ) )
						Log( string.Format( "Retrying {0} #{1}) after SQL error:  Number = {2},  Message = {3},  GetType = {4}", action.Method.Name, retryCount, ex.Number, ex.Message, ex.GetType() ) );
					else
						throw;

					if( ++retryCount > m_maxExecuteSqlRetryCount )
						throw;

					Thread.Sleep( m_maxExecuteSqlRetryDelay );
				}
				catch( CException ex )
				{
					if( ex.InnerException != null && ex.InnerException is SqlException )
					{
						if( Enum.IsDefined( typeof( RetryableSqlErrors ), ex.HResult ) )
							Log( string.Format( "Retrying {0} {1} after SQL error:  HResult = {2},  Message = {3},  GetType = {4}", action.Method.Name, retryCount, ex.HResult, ex.InnerException.Message + "\r\n" + ex.Message, ex.GetType() ) );
						else
							throw;

						if( ++retryCount > m_maxExecuteSqlRetryCount )
							throw;

						Thread.Sleep( m_maxExecuteSqlRetryDelay );
					}
					else
						throw;
				}
			}
		}

		public void ExecuteSqlWithRetry<T1, T2, T3>( Action<T1, T2, T3> action, T1 t1, T2 t2, T3 t3 )
		{
			var retryCount = 0;

			for(; ; )
			{
				try
				{
					action( t1, t2, t3 );
					break;
				}
				catch( SqlException ex )
				{
					if( Enum.IsDefined( typeof( RetryableSqlErrors ), ex.Number ) )
						Log( string.Format( "Retrying {0} #{1}) after SQL error:  Number = {2},  Message = {3},  GetType = {4}", action.Method.Name, retryCount, ex.Number, ex.Message, ex.GetType() ) );
					else
						throw;

					if( ++retryCount > m_maxExecuteSqlRetryCount )
						throw;

					Thread.Sleep( m_maxExecuteSqlRetryDelay );
				}
				catch( CException ex )
				{
					if( ex.InnerException != null && ex.InnerException is SqlException )
					{
						if( Enum.IsDefined( typeof( RetryableSqlErrors ), ex.HResult ) )
							Log( string.Format( "Retrying {0} {1} after SQL error:  HResult = {2},  Message = {3},  GetType = {4}", action.Method.Name, retryCount, ex.HResult, ex.InnerException.Message + "\r\n" + ex.Message, ex.GetType() ) );
						else
							throw;

						if( ++retryCount > m_maxExecuteSqlRetryCount )
							throw;

						Thread.Sleep( m_maxExecuteSqlRetryDelay );
					}
					else
						throw;
				}
			}
		}

		#endregion

		#region Protected Methods

		protected bool IsEmpty( string sValue )
		{
			return sValue == null || sValue.Length == 0;
		}

		protected bool IsEmpty( object oValue )
		{
			return ( oValue == null || oValue == DBNull.Value || oValue.ToString() == string.Empty );
		}

		protected object HandleStringValue( object oValue )
		{
			if( oValue == null || oValue == DBNull.Value )
				return string.Empty;
			else
				return oValue.ToString().Trim();
		}

		protected void VerifyPath( ref string sPath )
		{
			if( sPath != null )
				if( sPath.Substring( sPath.Length - 1, 1 ) != @"\" ) sPath += @"\";
		}

		protected bool UpdateStateInfo( string key, object value )
		{
			bool bStateChanged = false;
			if( m_StateInfo == null ) m_StateInfo = new ListDictionary();

			lock( m_StateInfo.SyncRoot )
			{
				if( !m_StateInfo.Contains( key ) )
				{
					m_StateInfo.Add( key, value );
					bStateChanged = true;
				}
				else if( ( value == null && m_StateInfo[key] != null )
					|| ( value != null && !value.Equals( m_StateInfo[key] ) ) )
				{
					m_StateInfo.Remove( key );
					m_StateInfo.Add( key, value );
					bStateChanged = true;
				}
			}

			return bStateChanged;
		}

		#endregion

		#region Private Methods


		/// <summary> 
		// Function requests the NetBios name of the device over UDP
		/// </summary> 
		private string GetNetBiosName( string sMachineIP )
		{
			IPAddress MachineIP = IPAddress.Parse( sMachineIP );
			byte[] receiveBuffer = new byte[1024];
			string sNetBiosName = "";
			byte[] btNetBiosNameRequest = new byte[]{
			0x80, 0x94, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x20, 0x43, 0x4b, 0x41,
			0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
			0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
			0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
			0x41, 0x41, 0x41, 0x41, 0x41, 0x00, 0x00, 0x21,
			0x00, 0x01 };

			Socket requestSocket = new Socket( AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp );
			requestSocket.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.SendTimeout, 50 );
			requestSocket.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 50 );
			EndPoint remoteEndpoint = new IPEndPoint( MachineIP, 137 );
			IPEndPoint originEndpoint = new IPEndPoint( IPAddress.Any, 0 );
			requestSocket.Bind( originEndpoint );

			requestSocket.SendTo( btNetBiosNameRequest, remoteEndpoint );
			try
			{
				int btCnt = requestSocket.ReceiveFrom( receiveBuffer, ref remoteEndpoint );
				if( btCnt >= 130 )
				{
					// Taken from http://www.faqs.org/rfcs/rfc1002.html
					// 18 bytes per name, starting from offset 56
					// followed by flags in two bytes,
					// the higest bit of the first byte indicates Group Name Flag:
					//		if one (1) then the name is a GROUP NetBIOS name
					//		if zero (0) then it is a UNIQUE NetBIOS name
					Int32 NoOfEntries = Convert.ToInt32( receiveBuffer[56] );
					Encoding enc = new ASCIIEncoding();
					char[] charsToTrim = { '\0', ' ' };
					for( int n = 0; n <= NoOfEntries - 1; n++ )
					{
						// find the name
						sNetBiosName = enc.GetString( receiveBuffer, 57 + n * 18, 16 ).Trim( charsToTrim );
						// break if it is UNIQUE NetBIOS name
						if( ( receiveBuffer[73 + n * 18] & 128 ) == 0 ) break;
					}
				}
			}
			catch( SocketException sex )
			{
				if( m_bShowContextInfoErrors )
					HandleException( new CException( "GetNetBiosName error: the machine with IP: " + sMachineIP + " could not be identified by a NetBios name", eExceptionType.Warning, sex, eDetailType.Medium ) );
				else
					Log( eSeverity.Info, "GetNetBiosName error: the machine with IP: " + sMachineIP + " could not be identified by a NetBios name\n" + sex.Message );
				return sMachineIP;
			}
			return sNetBiosName;
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			Dispose( true );
			GC.SuppressFinalize( this );
		}

		protected virtual void Dispose( bool disposing )
		{
			lock( this )
			{
				if( !this.m_bDisposed )
				{
					if( disposing )
					{
						// Dispose managed resources.
						if( m_ParentClass != null && m_StateChangeEventHandler != null )
						{
							m_ParentClass.StateChange -= m_StateChangeEventHandler;
						}
						if( this.m_bDisposableComponents )  // make sure we only dispose components created in this instance
						{
							if( m_Log != null ) m_Log.Dispose();
							if( m_Mail != null ) m_Mail.Dispose();
							if( m_Config != null ) m_Config.Dispose();
						}
					}
					// Release unmanaged resources.
				}
				m_bDisposed = true;
			}
		}

		[Conditional( "DEBUG" )]
		protected void AssertNotDisposed()
		{
			if( this.m_bDisposed ) throw new ObjectDisposedException( this.GetType().ToString() );
		}

		#endregion

	}
}
