using System;
using System.Collections.Specialized;
using System.Reflection;
using System.Text;

namespace InvCommon
{
	[Serializable]
	public class CExceptionFormatter
	{
		#region Member Variables

		protected const string LineSeparator = "======================================";
		protected ListDictionary m_AppInfo;

		#endregion

		#region Constructors

		public CExceptionFormatter( ListDictionary AppInfo )
		{
			this.m_AppInfo = AppInfo;
		}

		#endregion

		#region Virtual Methods

		public virtual string GetCaption( Exception ex, string hostName = null )
		{
			eExceptionType exceptiontype = ex is CException ? ( (CException) ex ).ExceptionType : eExceptionType.System;
			return GetCaption( exceptiontype, hostName );
		}

		public virtual string GetCaption( eExceptionType exceptiontype, string hostName = null )
		{
			string sEnv, sMachineName = hostName == null ? Environment.MachineName : hostName;
			string sSubject;

			if( String.Compare( sMachineName, "invisor.ca", true ) == 0 ||
				 String.Compare( sMachineName, "invisor.ca", true ) == 0 )
				sEnv = "Production";
			else if( String.Compare( sMachineName, "invisor.ca", true ) == 0 )
				sEnv = "Production DRP";
			else if( String.Compare( sMachineName, "invisor.ca", true ) == 0 )
				sEnv = "Test & QA";
			else if( String.Compare( sMachineName, "invisor.ca", true ) == 0 )
				sEnv = "Development";
			else
				sEnv = sMachineName;

			if( m_AppInfo != null )
			{
				if( m_AppInfo["ApplicationId"] != null )
					sSubject = string.Format( "< {0} | {1} | {2} | ProcessId={3} >  :  {4}", sEnv, CExceptionMap.Severity( exceptiontype ), exceptiontype, m_AppInfo["ApplicationId"], m_AppInfo["ApplicationName"] );
				else
					sSubject = string.Format( "< {0} | {1} | {2} >  :  {3}", sEnv, CExceptionMap.Severity( exceptiontype ), exceptiontype, m_AppInfo["ApplicationName"] );
			}
			else
				sSubject = string.Format( "< {0} | {1} | {2} >  :  {3}", sEnv, CExceptionMap.Severity( exceptiontype ), exceptiontype, CDefault.defaultLogLocation + System.Configuration.ConfigurationManager.AppSettings["ApplicationName"] );

			return sSubject;
		}

		public virtual string GetMessage( Exception exception )
		{
			StringBuilder eventInformation = new StringBuilder();

			if( exception != null )
			{
				Exception currException = exception;
				int exceptionCount = 1;

				do
				{
					if( currException is CException ) //&& ( (CException) currException ).ExceptionNumber == 0 )
					{
						//eventInformation.AppendFormat( "{0}\r\n*** {1} Error Occured ***", currException.Message, ( (CException) currException ).ExceptionType );
						CException currCException = (CException) currException;
						if( eventInformation.Length == 0 )
						{
							// top exception
							if( currCException.ExceptionDetail == eDetailType.None && currCException.ExceptionType == eExceptionType.None )
							{
								break;
							}
							else if( currCException.ExceptionDetail == eDetailType.None )
							{
								eventInformation.AppendFormat( "*** {0}", currCException.Message );
								break;
							}
							else if( currCException.ExceptionDetail == eDetailType.Brief )
							{
								eventInformation.AppendFormat( "*** {0} Error Occurred ***\r\n{1}", currCException.ExceptionType, currCException.Message );
								break;
							}
							else if( currCException.ExceptionDetail == eDetailType.Medium )
							{
								eventInformation.AppendFormat( "*** {0} Error Occurred ***\r\n{1}", currCException.ExceptionType, currCException.Message );
								eventInformation.AppendFormat( "\r\nError source: {0}()  Line: {1}  Col: {2}", currCException.MethodName, currCException.FileLineNumber, currCException.FileColumnNumber );
								if( currCException.InnerException != null )
								{
									eventInformation.AppendFormat( "\r\nOriginal Error:\r\n{0}", currCException.InnerException.Message );
								}
								break;
							}
							else if( currCException.ExceptionDetail == eDetailType.Full )
							{
								eventInformation.AppendFormat( "*** {0} Error Occurred ***\r\n{1}", currCException.ExceptionType, currCException.Message );
								eventInformation.AppendFormat( "\r\nError source: {0}()  Line: {1}  Col: {2}", currCException.MethodName, currCException.FileLineNumber, currCException.FileColumnNumber );
								if( currCException.InnerException != null )
								{
									eventInformation.AppendFormat( "\r\nOriginal Error:\r\n{0}", currCException.InnerException.Message );
									Exception innerException = currCException.InnerException;
									while( innerException.InnerException != null )
									{
										eventInformation.AppendFormat( "\r\n\t{0}", currCException.InnerException.InnerException.Message );
										innerException = innerException.InnerException;
									}
								}
								break;
							}
							else
							{
								eventInformation.AppendFormat( "*** {0} Error Occurred ***\r\n{1}", currCException.ExceptionType, currCException.Message );
								eventInformation.AppendFormat( "\r\nError source: {0}()  Line: {1}  Col: {2}", currCException.MethodName, currCException.FileLineNumber, currCException.FileColumnNumber );
							}
						}
					}
					else
					{
						if( eventInformation.Length == 0 )
							eventInformation.AppendFormat( "*** {0} Error Occurred ***\r\n{1}", eExceptionType.System, currException.Message );

						eventInformation.AppendFormat( "\r\n\r\n{0}\r\n{1}", "Exception Information Details:", LineSeparator );

						ReflectException( currException, eventInformation );
						eventInformation.AppendFormat( "\r\n{0}: {1}", "Exception Type", currException.GetType().FullName );

						// Record the StackTrace with separate label.
						if( currException.StackTrace != null )
						{
							eventInformation.AppendFormat( "\r\n\r\n{0} \r\n{1}",
								"StackTrace Information Details:", LineSeparator );
							eventInformation.AppendFormat( "\r\n{0}", currException.StackTrace );
						}
					}

					// Reset the temp exception object and iterate the counter.
					currException = currException.InnerException;
					exceptionCount++;
				} while( currException != null );
			}

			return eventInformation.ToString();
		}

		public virtual string GetMessageOld( Exception exception )
		{
			StringBuilder eventInformation = new StringBuilder();

			if( exception != null )
			{
				Exception currException = exception;
				int exceptionCount = 1;

				do
				{
					if( currException is CException ) //&& ( (CException) currException ).ExceptionNumber == 0 )
					{
						//eventInformation.AppendFormat( "{0}\r\n*** {1} Error Occured ***", currException.Message, ( (CException) currException ).ExceptionType );
						CException currCException = (CException) exception;
						if( eventInformation.Length == 0 )
						{
							// top exception
							if( currCException.ExceptionDetail == eDetailType.Brief )
							{
								eventInformation.AppendFormat( "{0}\r\n*** {1} Error Occurred ***\r\n", currCException.Message, currCException.ExceptionType );
								break;
							}
							else if( currCException.ExceptionDetail == eDetailType.Medium )
							{
								eventInformation.AppendFormat( "{0}\r\n*** {1} Error Occurred ***\r\n", currCException.Message, currCException.ExceptionType );
								currException = currException.InnerException;
								eventInformation.AppendFormat( "\r\nOriginal Error:\r\n{0}", currException.Message );
								//eventInformation.AppendFormat( "\r\n{0}  Line: {1}  Col: {2}", sMethodName, stFrame.GetFileLineNumber(), stFrame.GetFileColumnNumber() );
							}
							else
							{
								eventInformation.AppendFormat( "{0}\r\n*** {1} Error Occurred ***\r\n", currCException.Message, currCException.ExceptionType );
							}
						}
						//if( eventInformation.Length > 0 && eventInformation.ToString().Substring( eventInformation.Length - 1, 1 ) != "\n" )
						//    eventInformation.AppendFormat( "\r\n{0}\r\n", currException.Message );
						//else
						//    eventInformation.AppendFormat( "{0}\r\n", currException.Message );
					}
					else
					{
						eventInformation.AppendFormat( "\r\n{0}\r\n{1}", "Exception Information Details:", LineSeparator );

						ReflectException( currException, eventInformation );
						eventInformation.AppendFormat( "\r\n{0}: {1}", "Exception Type", currException.GetType().FullName );

						// Record the StackTrace with separate label.
						if( currException.StackTrace != null )
						{
							eventInformation.AppendFormat( "\r\n\r\n{0} \r\n{1}",
								"StackTrace Information Details:", LineSeparator );
							eventInformation.AppendFormat( "\r\n{0}", currException.StackTrace );
						}
					}

					// Reset the temp exception object and iterate the counter.
					currException = currException.InnerException;
					exceptionCount++;
				} while( currException != null );
			}

			return eventInformation.ToString();
		}

		public virtual string GetFooter( Exception ex )
		{
			if( m_AppInfo != null && m_AppInfo["Log"] != null )
				return "\r\n\r\nCheck log file: <" + ( (CLogger) m_AppInfo["Log"] ).LogPath.Replace( ".exe", "'DOT'exe" ) + ">\r\n";
			else
				return "\r\n\r\nCheck log file: <" + CDefault.defaultLogLocation + System.Configuration.ConfigurationManager.AppSettings["ApplicationName"].Replace( ".exe", "'DOT'exe" ) + ".log>\r\n";
		}

		#endregion

		#region Protected Methods

		protected static void ReflectException( Exception currException, StringBuilder strEventInfo )
		{
			PropertyInfo[] arrPublicProperties = currException.GetType().GetProperties();
			foreach( PropertyInfo propinfo in arrPublicProperties )
			{
				if( propinfo.Name != "InnerException" && propinfo.Name != "StackTrace" )
				{
					if( propinfo.GetValue( currException, null ) == null )
					{
						//						strEventInfo.AppendFormat("\r\n{0}: NULL", propinfo.Name);
					}
					else
					{
						ProcessAdditionalInfo( propinfo, currException, strEventInfo );
					}
				}
			}
		}

		protected static void ProcessAdditionalInfo( PropertyInfo propinfo, Exception currException, StringBuilder stringBuilder )
		{
			NameValueCollection currAdditionalInfo;

			if( propinfo.Name == "AdditionalInformation" )
			{
				if( propinfo.GetValue( currException, null ) != null )
				{
					currAdditionalInfo = (NameValueCollection) propinfo.GetValue( currException, null );

					if( currAdditionalInfo.Count > 0 )
					{
						stringBuilder.AppendFormat( "\r\nAdditionalInformation:" );

						for( int i = 0; i < currAdditionalInfo.Count; i++ )
						{
							stringBuilder.AppendFormat( "\r\n{0}: {1}", currAdditionalInfo.GetKey( i ), currAdditionalInfo[i] );
						}
					}
				}
			}
			else
			{
				stringBuilder.AppendFormat( "\r\n{0}: {1}", propinfo.Name, propinfo.GetValue( currException, null ) );
			}
		}

		#endregion
	}

	[Serializable]
	public class CProcessExceptionFormatter : CExceptionFormatter
	{
		public CProcessExceptionFormatter( ListDictionary AppInfo )
			: base( AppInfo )
		{
		}

		public override string GetMessage( Exception exception )
		{
			return base.GetMessage( exception );
		}
	}

	[Serializable]
	public class CDataExceptionFormatter : CExceptionFormatter
	{
		public CDataExceptionFormatter( ListDictionary AppInfo )
			: base( AppInfo )
		{
		}

		public override string GetMessage( Exception exception )
		{
			return base.GetMessage( exception );
			//if( (eSeverity) m_AppInfo["LogLevel"] >= eSeverity.Trace )
			//{
			//    return base.GetMessage( exception );
			//}

			//StringBuilder eventInformation = new StringBuilder();
			//eventInformation.Append( exception.Message );
			//return eventInformation.ToString();
		}
	}

	[Serializable]
	public class CXRefExceptionFormatter : CExceptionFormatter
	{
		public CXRefExceptionFormatter( ListDictionary AppInfo )
			: base( AppInfo )
		{
		}

		public override string GetMessage( Exception exception )
		{
			return base.GetMessage( exception );
		}
	}

	[Serializable]
	public class CConfigExceptionFormatter : CExceptionFormatter
	{
		public CConfigExceptionFormatter( ListDictionary AppInfo )
			: base( AppInfo )
		{
		}

		public override string GetMessage( Exception exception )
		{
			return base.GetMessage( exception );
		}
	}

}