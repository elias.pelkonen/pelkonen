using System;
using System.Runtime.Serialization;
using System.Diagnostics;

namespace InvCommon
{
	/// <summary>
	/// Summary description for CException.
	/// </summary>
	[Serializable]
	public class CException : ApplicationException //Exception
	{
		#region Member Variables

		private eExceptionType m_ExceptionType;
		private eDetailType m_DetailType;
		private int m_ExceptionNumber;
		private string m_sMethodName;
		private string m_sFileLineNumber;
		private string m_sFileColumnNumber;

		#endregion

		#region Constructors
		public CException()
		{
			m_ExceptionType = eExceptionType.Process;
			m_DetailType = CExceptionMap.ExceptionDetailType( m_ExceptionType );
			SaveCallerStackFrame();
		}

		public CException( string message )
			: base( message )
		{
			m_ExceptionType = eExceptionType.Process;
			m_DetailType = CExceptionMap.ExceptionDetailType( m_ExceptionType );
			SaveCallerStackFrame();
		}

		public CException( string message, eDetailType ExceptionDetail )
			: base( message )
		{
			m_ExceptionType = eExceptionType.Process;
			m_DetailType = ExceptionDetail;
			SaveCallerStackFrame();
		}

		public CException( string message, Exception inner, int hResult = 0 )
			: base( message, inner )
		{
			if( inner is CException )
				m_ExceptionType = ( (CException) inner ).ExceptionType;
			else
				m_ExceptionType = eExceptionType.System;
			m_DetailType = CExceptionMap.ExceptionDetailType( m_ExceptionType );
			HResult = hResult == 0 ? HResult : hResult;
			SaveCallerStackFrame();
		}

		public CException( string message, Exception inner, eDetailType ExceptionDetail, int hResult = 0 )
			: base( message, inner )
		{
			if( inner is CException )
				m_ExceptionType = ( (CException) inner ).ExceptionType;
			else
				m_ExceptionType = eExceptionType.System;
			m_DetailType = ExceptionDetail;
			HResult = hResult == 0 ? HResult : hResult;
			SaveCallerStackFrame();
		}

		public CException( string message, eExceptionType ExceptionType )
			: base( message )
		{
			m_ExceptionType = ExceptionType;
			m_DetailType = eDetailType.Brief;
			SaveCallerStackFrame();
		}

		public CException( string message, eExceptionType ExceptionType, eDetailType ExceptionDetail )
			: base( message )
		{
			m_ExceptionType = ExceptionType;
			m_DetailType = ExceptionDetail;
			SaveCallerStackFrame();
		}

		public CException( string message, eExceptionType ExceptionType, Exception inner, int hResult = 0 )
			: base( message, inner )
		{
			m_ExceptionType = ExceptionType;
			m_DetailType = CExceptionMap.ExceptionDetailType( m_ExceptionType );
			HResult = hResult == 0 ? HResult : hResult;
			SaveCallerStackFrame();
		}

		public CException( string message, eExceptionType ExceptionType, Exception inner, eDetailType ExceptionDetail, int hResult = 0 )
			: base( message, inner )
		{
			m_ExceptionType = ExceptionType;
			m_DetailType = ExceptionDetail;
			HResult = hResult == 0 ? HResult : hResult;
			SaveCallerStackFrame();
		}

		#endregion

		#region Accessors
		public int ExceptionNumber
		{
			get { return ( m_ExceptionNumber ); }
			set { m_ExceptionNumber = value; }
		}

		public eExceptionType ExceptionType
		{
			get { return ( m_ExceptionType ); }
			set { m_ExceptionType = value; }
		}

		public eDetailType ExceptionDetail
		{
			get { return ( m_DetailType ); }
			set { m_DetailType = value; }
		}

		public string MethodName
		{
			get { return ( m_sMethodName ); }
			set { m_sMethodName = value; }
		}

		public string FileLineNumber
		{
			get { return ( m_sFileLineNumber ); }
			set { m_sFileLineNumber = value; }
		}

		public string FileColumnNumber
		{
			get { return ( m_sFileColumnNumber ); }
			set { m_sFileColumnNumber = value; }
		}
		#endregion

		#region Public Methods

		public override void GetObjectData( SerializationInfo info, StreamingContext context )
		{
			// add the custom property into the serialization stream
			info.AddValue( "ExceptionType", m_ExceptionType, typeof( eExceptionType ) );
			info.AddValue( "ExceptionNumber", m_ExceptionNumber, typeof( int ) );
			info.AddValue( "ExceptionDetailType", m_DetailType, typeof( eDetailType ) );
			// call the base exception class to ensure proper serialization
			base.GetObjectData( info, context );
		}

		#endregion

		#region Protected Methods

		// ISerializable Implementation
		protected CException( SerializationInfo info, StreamingContext context )
			: base( info, context )
		{
			// get the custom property out of the serialization stream and set the object's property
			m_ExceptionType = (eExceptionType) info.GetValue( "ExceptionType", typeof( eExceptionType ) );
			m_ExceptionNumber = (int) info.GetValue( "ExceptionNumber", typeof( int ) );
			m_DetailType = (eDetailType) info.GetValue( "ExceptionDetailType", typeof( eDetailType ) );
		}

		#endregion

		#region Private Methods

		private void SaveCallerStackFrame()
		{
			StackFrame stFrame = new StackFrame( 2, true );
			m_sMethodName = stFrame.GetMethod().DeclaringType + "." + stFrame.GetMethod().Name;
			m_sFileLineNumber = stFrame.GetFileLineNumber().ToString();
			m_sFileColumnNumber = stFrame.GetFileColumnNumber().ToString();
		}

		#endregion
	}

	/// <summary>
	/// Summary description for CExceptionMap.
	/// </summary>
	[Serializable]
	public class CExceptionMap
	{
		//protected ListDictionary m_AppInfo;
		//protected CConfig m_Config;

		#region Constructors

		private CExceptionMap()
		{
		}

		//private CExceptionMap(ListDictionary AppInfo)
		//{
		//	this.m_AppInfo = AppInfo;
		//}

		#endregion

		#region Public Methods

		public static eSeverity Severity( Exception ex )
		{
			eExceptionType ExceptionType = ex.GetType() == typeof( CException ) ? ( (CException) ex ).ExceptionType : eExceptionType.System;
			return Severity( ExceptionType );
		}

		public static eSeverity Severity( eExceptionType ExceptionType )
		{
			switch( ExceptionType )
			{
				case ( eExceptionType.None ): return eSeverity.None;
				case ( eExceptionType.Success ): return eSeverity.Info;
				case ( eExceptionType.Warning ): return eSeverity.Warning;
				case ( eExceptionType.Cancel ): return eSeverity.Warning;
				case ( eExceptionType.Retry ): return eSeverity.Warning;
				case ( eExceptionType.XRef ): return eSeverity.Error;
				case ( eExceptionType.Data ): return eSeverity.Error;
				case ( eExceptionType.Config ): return eSeverity.Error;
				case ( eExceptionType.Process ): return eSeverity.Error;
				case ( eExceptionType.System ): return eSeverity.Error;
				case ( eExceptionType.Security ): return eSeverity.Error;
				default: return CDefault.defaultSeverity;
			}
		}

		public static eDetailType ExceptionDetailType( eExceptionType ExceptionType )
		{
			switch( ExceptionType )
			{
				case ( eExceptionType.None ): return eDetailType.None;
				case ( eExceptionType.Success ): return eDetailType.None;
				case ( eExceptionType.Warning ): return eDetailType.None;
				case ( eExceptionType.Cancel ): return eDetailType.Brief;
				case ( eExceptionType.Retry ): return eDetailType.Brief;
				case ( eExceptionType.XRef ): return eDetailType.Brief;
				case ( eExceptionType.Data ): return eDetailType.Medium;
				case ( eExceptionType.Config ): return eDetailType.Medium;
				case ( eExceptionType.Process ): return eDetailType.Medium;
				case ( eExceptionType.System ): return eDetailType.Full;
				case ( eExceptionType.Security ): return eDetailType.Brief;
				default: return eDetailType.Full;
			}
		}

		public static bool RethrowRequired( Exception ex )
		{
			eExceptionType ExceptionType = ex.GetType() == typeof( CException ) ? ( (CException) ex ).ExceptionType : eExceptionType.System;
			return RethrowRequired( ExceptionType );
		}

		public static bool RethrowRequired( eExceptionType ExceptionType )
		{
			switch( ExceptionType )
			{
				//case ( eExceptionType.System ): return true;
				default: return false;
			}
		}

		public static bool EmailRequired( Exception ex )
		{
			eExceptionType ExceptionType = ex.GetType() == typeof( CException ) ? ( (CException) ex ).ExceptionType : eExceptionType.System;
			return EmailRequired( ExceptionType );
		}

		public static bool EmailRequired( eExceptionType ExceptionType )
		{
			switch( ExceptionType )
			{
				case ( eExceptionType.None ): return false;
				case ( eExceptionType.Success ): return false;
				default: return true;
			}
		}

		public static bool LogRequired( Exception ex )
		{
			eExceptionType ExceptionType = ex.GetType() == typeof( CException ) ? ( (CException) ex ).ExceptionType : eExceptionType.System;
			return LogRequired( ExceptionType );
		}

		public static bool LogRequired( eExceptionType ExceptionType )
		{
			switch( ExceptionType )
			{
				case ( eExceptionType.None ): return true;
				default: return true;
			}
		}

		public static Type Formatter( Exception ex )
		{
			eExceptionType ExceptionType = ex.GetType() == typeof( CException ) ? ( (CException) ex ).ExceptionType : eExceptionType.System;
			return Formatter( ExceptionType );
		}

		public static Type Formatter( eExceptionType ExceptionType )
		{
			switch( ExceptionType )
			{
				case ( eExceptionType.System ): return typeof( CExceptionFormatter );
				case ( eExceptionType.Process ): return typeof( CProcessExceptionFormatter );
				case ( eExceptionType.Data ): return typeof( CDataExceptionFormatter );
				case ( eExceptionType.XRef ): return typeof( CXRefExceptionFormatter );
				case ( eExceptionType.Config ): return typeof( CConfigExceptionFormatter );
				default: return typeof( CExceptionFormatter );
			}
		}

		#endregion
	}
}
