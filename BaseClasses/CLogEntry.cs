using System;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.Management.Instrumentation;
using System.Text;
using System.Threading;
using System.Xml.Serialization;

namespace InvCommon
{
	/// <summary>
	/// Represents a log message.  Contains the common properties that are required for all log messages.
	/// </summary>
	[XmlRoot( "logEntry" )]
	[Serializable]
	[InstrumentationClass( InstrumentationType.Event )]
	public class CLogEntry : ICloneable
	{
		#region Member Variables

		private string message = string.Empty;
		private string title = string.Empty;
		private string category = string.Empty;
		private int priority = -1;
		private int eventId = 0;
		private eSeverity severity = CDefault.defaultSeverity;

		private string machineName = string.Empty;
		private DateTime timeStamp = DateTime.MaxValue;

		private StringBuilder errorMessages;
		private IDictionary extendedProperties;

		private string appDomainName;
		private string systemProcessId;
		private string systemProcessName;
		//private string applicationId;			// Inv process ID
		//private string applicationName;		// Inv process name
		private string threadName;
		private string win32ThreadId;

		#endregion

		#region Constructors

		/// <summary>
		/// Initialize a new instance of a <see cref="LogEntry"/> class.
		/// </summary>
		public CLogEntry()
		{
			/*			this.Priority = Default.minimumPriority;
						this.EventId = Default.defaultEventID;
						this.Severity = Default.defaultSeverity;	*/

			CollectIntrinsticProperties();
		}

		/// <summary>
		/// Create a new instance of <see cref="LogEntry"/> with a full set of constructor parameters
		/// </summary>
		/// <param name="message">Message body to log.  Value from ToString() method from message object.</param>
		/// <param name="category">Category name used to route the log entry to a one or more sinks.</param>
		/// <param name="priority">Only messages must be above the minimum priority are processed.</param>
		/// <param name="eventId">Event number or identifier.</param>
		/// <param name="severity">Log entry severity as a <see cref="Severity"/> enumeration. (Unspecified, Information, Warning or Error).</param>
		/// <param name="title">Additional description of the log entry message.</param>
		/// <param name="properties">Dictionary of key/value pairs to record.</param>
		public CLogEntry( object message, string category, int priority, int eventId, eSeverity severity,/* string applicationId, string applicationName,*/ string title, IDictionary properties )
		{
			this.Message = message.ToString();
			this.Priority = priority;
			this.Category = category;
			this.EventId = eventId;
			this.Severity = severity;
			//this.ApplicationId = applicationId;
			//this.ApplicationName = applicationName;
			this.Title = title;
			this.ExtendedProperties = properties;

			CollectIntrinsticProperties();
		}

		#endregion

		#region Accessors

		/// <summary>
		/// Message body to log.  Value from ToString() method from message object.
		/// </summary>
		public string Message
		{
			get { return this.message; }
			set { this.message = value; }
		}

		/// <summary>
		/// Category name used to route the log entry to a one or more sinks.
		/// </summary>
		public string Category
		{
			get
			{
				if( this.category == null )
				{
					return "";
				}
				else
				{
					return this.category;
				}
			}
			set { this.category = value; }
		}

		/// <summary>
		/// Importance of the log message.  Only messages must be above the minimum priority are processed.
		/// </summary>
		public int Priority
		{
			get { return this.priority; }
			set { this.priority = value; }
		}

		/// <summary>
		/// Event number or identifier.
		/// </summary>
		public int EventId
		{
			get { return this.eventId; }
			set { this.eventId = value; }
		}

		/// <summary>
		/// Log entry severity as a <see cref="Severity"/> enumeration. (Unspecified, Information, Warning or Error).
		/// </summary>
		[IgnoreMember]
		public eSeverity Severity
		{
			get { return this.severity; }
			set { this.severity = value; }
		}

		/// <summary>
		/// <para>Gets or sets the <see cref="System.Int32"/> value of the <see cref="Severity"/> enumeration.</para>
		/// </summary>
		/// <value>
		/// <para>The <see cref="System.Int32"/> value of the <see cref="Severity"/> enumeration.</para>
		/// </value>
		/// <summary>
		/// <para>Gets or sets the <see cref="System.Int32"/> value of the <see cref="Severity"/> enumeration.</para>
		/// </summary>
		/// <value>
		/// <para>The <see cref="System.Int32"/> value of the <see cref="Severity"/> enumeration.</para>
		/// </value>
		public int LoggedSeverity
		{
			get { return (int) this.severity; }
			set
			{
				if( Enum.IsDefined( typeof( eSeverity ), value ) )
				{
					throw new Exception( "The value provided is not defined for Severity" );
				}
				severity = (eSeverity) Enum.Parse( typeof( eSeverity ), value.ToString( CultureInfo.InvariantCulture ) );
			}
		}

		/// <summary>
		/// Additional description of the log entry message.
		/// </summary>
		public string Title
		{
			get { return this.title; }
			set { this.title = value; }
		}

		/// <summary>
		/// Date and time of the log entry message.
		/// </summary>
		public DateTime TimeStamp
		{
			get { return this.timeStamp; }
			set { this.timeStamp = value; }
		}

		/// <summary>
		/// Name of the computer.
		/// </summary>
		public string MachineName
		{
			get { return this.machineName; }
			set { this.machineName = value; }
		}

		/// <summary>
		/// The AppDomain in which we are running
		/// </summary>
		public string AppDomainName
		{
			get { return this.appDomainName; }
			set { this.appDomainName = value; }
		}

		/// <summary>
		/// The Win32 process ID for the current running process.
		/// </summary>
		public string SystemProcessId
		{
			get { return systemProcessId; }
			set { systemProcessId = value; }
		}

		/// <summary>
		/// Inv Process ID from tblProcess table
		/// </summary>
		//public string ApplicationId
		//{
		//	get { return applicationId; }
		//	set { applicationId = value; }
		//}

		/// <summary>
		/// The name of the current running process.
		/// </summary>
		public string SystemProcessName
		{
			get { return systemProcessName; }
			set { systemProcessName = value; }
		}

		/// <summary>
		/// Inv Process name from tblProcess table
		/// </summary>
		//public string ApplicationName
		//{
		//	get { return applicationName; }
		//	set { applicationName = value; }
		//}

		/// <summary>
		/// The name of the .NET thread. <seealso cref="Win32ThreadId"/>
		/// </summary>
		public string ManagedThreadName
		{
			get { return threadName; }
			set { threadName = value; }
		}

		/// <summary>
		/// The Win32 Thread ID for the current thread.
		/// </summary>
		public string Win32ThreadId
		{
			get { return win32ThreadId; }
			set { win32ThreadId = value; }
		}

		/// <summary>
		/// Dictionary of key/value pairs to record.
		/// </summary>
		[IgnoreMember]
		public IDictionary ExtendedProperties
		{
			get
			{
				if( extendedProperties == null )
				{
					extendedProperties = new Hashtable();
				}
				return this.extendedProperties;
			}
			set { this.extendedProperties = value; }
		}

		/// <summary>
		/// Read-only property that returns the timeStamp formatted using the current culture.
		/// </summary>
		public string TimeStampString
		{
			get { return TimeStamp.ToString( CultureInfo.CurrentCulture ); }
		}

		/// <summary>
		/// Gets the error message with the <see cref="LogEntry"></see>
		/// </summary>
		public string ErrorMessages
		{
			get
			{
				if( errorMessages == null )
					return null;
				else
					return errorMessages.ToString();
			}
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Creates a new <see cref="LogEntry"/> that is a copy of the current instance.
		/// </summary>
		/// <remarks>
		/// If the dictionary contained in <see cref="ExtendedProperties"/> implements <see cref="ICloneable"/>, the resulting
		/// <see cref="LogEntry"/> will have its ExtendedProperties set by calling <c>Clone()</c>. Otherwise the resulting
		/// <see cref="LogEntry"/> will have its ExtendedProperties set to null.
		/// </remarks>
		/// <implements>ICloneable.Clone</implements>
		/// <returns>A new <c>LogEntry</c> that is a copy of the current instance.</returns>
		public object Clone()
		{
			CLogEntry result = new CLogEntry
			{
				Message = this.Message,
				Category = this.Category,
				EventId = this.EventId,
				Title = this.Title,
				Severity = this.Severity,
				Priority = this.Priority,

				TimeStamp = this.TimeStamp,
				MachineName = this.MachineName,
				AppDomainName = this.AppDomainName,
				SystemProcessId = this.SystemProcessId,
				SystemProcessName = this.SystemProcessName,
				ManagedThreadName = this.ManagedThreadName
			};
			//result.ApplicationId = this.ApplicationId;
			//result.ApplicationName = this.ApplicationName;

			if( this.extendedProperties is ICloneable )
			{
				result.ExtendedProperties = (IDictionary) ( (ICloneable) extendedProperties ).Clone();
			}

			return result;
		}

		/// <summary>
		/// Add an error or warning message to the start of the messages string builder.
		/// Used by the distributor to record problems.
		/// </summary>
		/// <param name="message">Message to be added to this instance</param>
		public virtual void AddErrorMessage( string message )
		{
			if( errorMessages == null )
			{
				errorMessages = new StringBuilder();
			}
			//errorMessages.Insert(0, Environment.NewLine);
			//errorMessages.Insert(0, Environment.NewLine);
			errorMessages.Insert( 0, message );
		}

		#endregion

		#region Internal Methods

		internal static string GetProcessName()
		{
			StringBuilder buffer = new StringBuilder( 1024 );
			int length = NativeMethods.GetModuleFileName( NativeMethods.GetModuleHandle( null ), buffer, buffer.Capacity );
			Debug.Assert( length > 0 );
			return buffer.ToString();
		}

		#endregion

		#region Private Methods
		/// <summary>
		/// Set the intrinsic properties such as MachineName and UserIdentity.
		/// </summary>
		private void CollectIntrinsticProperties()
		{
			this.TimeStamp = DateTime.Now;

			try
			{
				MachineName = Environment.MachineName;
			}
			catch( Exception e )
			{
				this.MachineName = e.Message;
			}

			try
			{
				appDomainName = AppDomain.CurrentDomain.FriendlyName;
			}
			catch( Exception e )
			{
				appDomainName = e.Message;
			}

			try
			{
				systemProcessId = NativeMethods.GetCurrentProcessId().ToString( NumberFormatInfo.InvariantInfo );
			}
			catch( Exception e )
			{
				systemProcessId = e.Message;
			}

			try
			{
				systemProcessName = GetProcessName();
			}
			catch( Exception e )
			{
				systemProcessName = e.Message;
			}

			try
			{
				threadName = Thread.CurrentThread.Name;
			}
			catch( Exception e )
			{
				threadName = e.Message;
			}

			try
			{
				win32ThreadId = NativeMethods.GetCurrentThreadId().ToString( NumberFormatInfo.InvariantInfo );
			}
			catch( Exception e )
			{
				win32ThreadId = e.Message;
			}
		}

		#endregion
	}
}
