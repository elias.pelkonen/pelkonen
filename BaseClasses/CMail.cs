using System;
using System.IO;
using System.Net.Mail;
using System.Diagnostics;
using System.Net.Mime;

namespace InvCommon
{
	/// <summary>
	/// Summary description for CMail.
	/// </summary>
	[Serializable]
	public class CMail : IDisposable
	{
		#region Member Variables

		private bool m_bDisposed = false;
		protected string m_sSmtpServer;
		private string m_sMailTemporaryFolder;
		private bool m_bCanSend = true;
		private CApp m_app = null;

		#endregion

		#region Constructors

		public CMail()
		{
			m_sSmtpServer = CDefault.defaultSMTPServer;
			m_sMailTemporaryFolder = CDefault.defaultLogLocation;
		}

		public CMail( CApp app )
		{
			m_app = app;
			m_sSmtpServer = CDefault.defaultSMTPServer;
			m_sMailTemporaryFolder = CDefault.defaultLogLocation;
		}

		public CMail( string SmtpServer )
		{
			ArgumentValidation.CheckForEmptyString( SmtpServer, "SmtpServer" );
			m_sSmtpServer = SmtpServer;
			m_sMailTemporaryFolder = CDefault.defaultLogLocation;
		}

		public CMail( string SmtpServer, CApp app )
		{
			m_app = app;
			ArgumentValidation.CheckForEmptyString( SmtpServer, "SmtpServer" );
			m_sSmtpServer = SmtpServer;
			m_sMailTemporaryFolder = CDefault.defaultLogLocation;
		}

		public CMail( string SmtpServer, CApp app, bool CanSend )
		{
			m_app = app;
			ArgumentValidation.CheckForEmptyString( SmtpServer, "SmtpServer" );
			m_sSmtpServer = SmtpServer;
			m_sMailTemporaryFolder = CDefault.defaultLogLocation;
			m_bCanSend = CanSend;
		}

		#endregion

		#region Accessors

		public string SmtpServer
		{
			get { return m_sSmtpServer; }
			set { m_sSmtpServer = value; }
		}

		public string MailTemporaryFolder
		{
			get { return m_sMailTemporaryFolder; }
			set { m_sMailTemporaryFolder = value; }
		}

		public bool CanSend
		{
			get { return m_bCanSend; }
			set { m_bCanSend = value; }
		}

		#endregion

		#region Public Methods

		public void Send( string from, string to, string cc, CLogEntry logEntry )
		{
			SendImplEx( from, to, cc, "", logEntry.Title, logEntry.Message, null, false );
		}

		public void Send( string from, string to, string cc, string bcc, CLogEntry logEntry )
		{
			SendImplEx( from, to, cc, bcc, logEntry.Title, logEntry.Message, null, false );
		}

		public void Send( string from, string to, string cc, string subject, string messageText, string attachments )
		{
			SendImplEx( from, to, cc, "", subject, messageText, attachments, false );
		}

		public void Send( string from, string to, string cc, string bcc, string subject, string messageText, string attachments )
		{
			SendImplEx( from, to, cc, bcc, subject, messageText, attachments, false );
		}

		public void Send( string from, string to, string cc, string subject, string messageText, string attachments, bool bodyhtml )
		{
			SendImplEx( from, to, cc, "", subject, messageText, attachments, bodyhtml );
		}

		public void Send( string from, string to, string cc, string bcc, string subject, string messageText, string attachments, bool bodyhtml )
		{
			SendImplEx( from, to, cc, bcc, subject, messageText, attachments, bodyhtml );
		}

		public void Send( string from, string to, string cc, string subject, string messageText, string attachments, bool bodyhtml, string[] attachmentsInlineIDs )
		{
			SendImplEx( from, to, cc, "", subject, messageText, attachments, bodyhtml, attachmentsInlineIDs );
		}

		public void Send( string from, string to, string cc, string bcc, string subject, string messageText, string attachments, bool bodyhtml, MailPriority priority )
		{
			SendImplEx( from, to, cc, bcc, subject, messageText, attachments, bodyhtml, null, priority );
		}

		public void Close()
		{
			Dispose();
		}

		#endregion

		#region Protected Methods

		/// <summary>
		/// This method uses the MS recommended System.Net.Mail namespace
		/// </summary>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="cc"></param>
		/// <param name="subject"></param>
		/// <param name="messageText"></param>
		/// <param name="attachments"></param>
		protected virtual void SendImplEx( string from, string to, string cc, string bcc, string subject, string messageText, string attachments, bool bodyhtml )
		{
			SendImplEx( from, to, cc, bcc, subject, messageText, attachments, bodyhtml, null, MailPriority.Normal );
		}

		protected virtual void SendImplEx( string from, string to, string cc, string bcc, string subject, string messageText, string attachments, bool bodyhtml, string[] attachmentsInlineIDs )
		{
			SendImplEx( from, to, cc, bcc, subject, messageText, attachments, bodyhtml, attachmentsInlineIDs, MailPriority.Normal );
		}

		protected virtual void SendImplEx( string from, string to, string cc, string bcc, string subject, string messageText, string attachments, bool bodyhtml, string[] attachmentsInlineIDs, MailPriority priority )
		{
			if( m_app != null )
				m_app.Log( eSeverity.Debug, "SendImplEx: m_bCanSendMail = " + m_bCanSend.ToString() );
			if( !m_bCanSend ) return;

			AssertNotDisposed();

			ArgumentValidation.CheckForEmptyString( from, "from" );
			ArgumentValidation.CheckForEmptyString( to, "to" );
			SmtpClient _Client = new SmtpClient( m_sSmtpServer );

			// replace all semicolons with a coma
			from = from.Replace( ";", "," );
			to = to.Replace( ";", "," );
			// remove a leading or trailing coma
			if( from[0] == ',' ) from = from.Remove( 0, 1 );
			if( from[from.Length - 1] == ',' ) from = from.Remove( from.Length - 1, 1 );
			if( to[0] == ',' ) to = to.Remove( 0, 1 );
			if( to[to.Length - 1] == ',' ) to = to.Remove( to.Length - 1, 1 );

			// Specify the message content.
			MailMessage mailMessage = new MailMessage( from, to );
			// Set CC for the e-mail message if it's not empty.
			if( !( cc == null || cc.Equals( string.Empty ) ) )
			{
				cc = cc.Replace( ";", "," );
				if( cc[0] == ',' ) cc = cc.Remove( 0, 1 );
				if( cc[cc.Length - 1] == ',' ) cc = cc.Remove( cc.Length - 1, 1 );
				mailMessage.CC.Add( cc );
			}
			// Set BCC for the e-mail message if it's not empty.
			if( !( bcc == null || bcc.Equals( string.Empty ) ) )
			{
				bcc = bcc.Replace( ";", "," );
				if( bcc[0] == ',' ) bcc = bcc.Remove( 0, 1 );
				if( bcc[bcc.Length - 1] == ',' ) bcc = bcc.Remove( bcc.Length - 1, 1 );
				mailMessage.Bcc.Add( bcc );
			}
            
            mailMessage.Subject = subject + (to.ToLower().Contains("support") ? " - " + m_app.DomainHostName : "");
            mailMessage.Body = messageText + "\n\r\t" + (to.ToLower().Contains("support") ? "Domain: " + m_app.DomainHostName : "");
            mailMessage.IsBodyHtml = bodyhtml;
			mailMessage.Priority = priority;

			string[] arrAttachTemp = null;

			if( attachments != null && attachments.Length > 0 )
			{
				char[] delim = new char[] { ',' };
				string[] arrAttachment = attachments.Split( delim );
				arrAttachTemp = new string[arrAttachment.Length];

				DirectoryInfo objDir = new DirectoryInfo( m_sMailTemporaryFolder );

				if( objDir.Exists )
				{
					for( int i = 0; i <= arrAttachment.Length - 1; i++ )
					{
						try
						{
							FileInfo oFile = new FileInfo( arrAttachment[i] );

							if( oFile.Exists )
							{
								arrAttachTemp[i] = Path.Combine( m_sMailTemporaryFolder, oFile.Name );
								oFile.CopyTo( arrAttachTemp[i], true );
								if( bodyhtml && attachmentsInlineIDs != null && !string.IsNullOrWhiteSpace( attachmentsInlineIDs[i] ) )
								{
									mailMessage.AlternateViews.Add( GetInlineImage( arrAttachTemp[i], attachmentsInlineIDs[i], messageText ) );
								}
								else
								{
									mailMessage.Attachments.Add( new Attachment( arrAttachTemp[i] ) );
								}
							}
						}
						catch( Exception ex )
						{
							m_app.HandleException( ex, false );
						}
					}
				}
				else
					throw new CException( "Directory: '" + m_sMailTemporaryFolder + "' Temporary Mailer folder doesn't exist", eExceptionType.Config );
			}
            try
            {
                if( m_app != null )
                {
                    m_app.Log( eSeverity.Debug, "SendImplEx: Host = " + _Client.Host + ", Port = " + _Client.Port + ", UseDefaultCredentials = " + _Client.UseDefaultCredentials.ToString() );
                    m_app.Log( eSeverity.Debug, "SendImplEx: From = " + mailMessage.From.Address + ", To = " + mailMessage.To[0].Address + ", subject = " + subject, ", no of attachments = " + ( arrAttachTemp == null ? "0" : arrAttachTemp.Length.ToString() ) );
                }
                _Client.Send( mailMessage );
                m_app.Log( eSeverity.Debug, "SendImplEx: mail with subject = " + subject + " sent successfully" );
            }
            catch( Exception ex )
            {
                // m_bCanSend = false;
                if( m_app != null )
                {
                    m_app.Log( "SendImplEx: mail with subject = " + subject + " failed" );
                    m_app.HandleException( ex, false, false, true );
                }
                throw ex;
            }
            finally
            {
                mailMessage.Dispose();

                if( arrAttachTemp != null )
                {
                    foreach( string sFile in arrAttachTemp )
                    {
                        if( sFile != null )
                        {
                            FileInfo oFile = new FileInfo( sFile );

                            if( oFile.Exists ) oFile.Delete();
                        }
                    }
                }
            }
        }

        protected bool IsEmpty( string subjectLineMarker )
		{
			return subjectLineMarker == null || subjectLineMarker.Length == 0;
		}

		protected AlternateView GetInlineImage( string fileLocation, string attachmentsInlineID, string body )
		{
			LinkedResource resource = new LinkedResource( fileLocation, "image/jpeg" )
			{
				ContentId = attachmentsInlineID
			};
			AlternateView alternateView = AlternateView.CreateAlternateViewFromString( body, null, MediaTypeNames.Text.Html );
			alternateView.LinkedResources.Add( resource );
			return alternateView;
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			Dispose( true );
			GC.SuppressFinalize( this );
		}

		protected virtual void Dispose( bool disposing )
		{
			if( !this.m_bDisposed )
			{
				if( disposing )
				{
					// Dispose managed resources.
				}
				// Release unmanaged resources.
			}
			m_bDisposed = true;
		}

		[Conditional( "DEBUG" )]
		protected void AssertNotDisposed()
		{
			if( this.m_bDisposed ) throw new ObjectDisposedException( this.GetType().ToString() );
		}

		#endregion

	}

    public static class TextTool
    {
        /// <summary>
        /// Count occurrences of strings.
        /// </summary>
        public static int CountStringOccurrences(string text, string pattern)
        {
            // Loop through all instances of the string 'text'.
            int count = 0;
            int i = 0;
            while ((i = text.IndexOf(pattern, i)) != -1)
            {
                i += pattern.Length;
                count++;
            }
            return count;
        }
    }
}
