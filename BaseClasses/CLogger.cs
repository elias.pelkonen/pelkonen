using System;
using System.Diagnostics;

namespace InvCommon
{
	/// <summary>
	/// This is the base class for all Logger classes. 
	/// </summary>

	[Serializable]
	public abstract class CLogger : IDisposable
	{
		#region Member Variables

		private bool m_bDisposed = false;

		#endregion

		#region Constructors/Destructors

		~CLogger() { Dispose( false ); }

		#endregion

		#region Abstract Methods

		public abstract string LogPath { get; set; }

		#endregion

		#region Virtual Methods

		public virtual void Write( CLogEntry LogEntry )
		{
			AssertNotDisposed();
			try
			{
				//if( m_bCanLog )
				PrintOut( LogEntry );
				//else
				//    LogToEventlog( null, LogEntry );
			}
			catch( Exception ex )
			{
				//m_bCanLog = false;
				// we don't want to crush the app merely because write to log failed
				LogToEventlog( ex.Message, LogEntry );
			}
		}

		public virtual void Write( string Message )
		{
			AssertNotDisposed();
			CLogEntry logEntry = new CLogEntry(
				Message,
				null,
				CDefault.minimumPriority,
				CDefault.defaultEventID,
				eSeverity.None,
				null,
				null );

			Write( logEntry );
		}

		public virtual bool Flush()
		{
			AssertNotDisposed();
			return true;
		}

		#endregion

		#region Protected Methods

		// protected methods
		protected abstract void PrintOut( CLogEntry msg );
		protected virtual void StopLogging() { }
		//		protected virtual bool Initialize() { return true; }

		protected bool IsEmpty( string sValue )
		{
			return sValue == null || sValue.Length == 0;
		}

		#endregion

		#region Private Methods

		private void LogToEventlog( string sExeptionMessage, CLogEntry LogEntry )
		{
			try
			{
				string sErr;
				// try EventLog
				EventLog evLog = new EventLog { Source = LogEntry.SystemProcessName };
				if( sExeptionMessage != null )
					sErr = "*** Write Log Entry Failed ***\r\n" + sExeptionMessage + "\r\n\r\n*** Original Log Entry ***\r\n" + LogEntry.Message;
				else
					sErr = "*** Write Log Entry Failed ***\r\n\r\n*** Original Log Entry ***\r\n" + LogEntry.Message;
				evLog.WriteEntry( sErr, EventLogEntryType.Error );
			}
			catch
			{
				// not much we can do...
			}
		}

		#endregion

		#region IDisposable Members

		public void Close()
		{
			Dispose();
		}

		public void Dispose()
		{
			Dispose( true );
			GC.SuppressFinalize( this );
		}

		protected virtual void Dispose( bool disposing )
		{
			if( !this.m_bDisposed )
			{
				if( disposing )
				{
					// Dispose managed resources.
				}
				// Release unmanaged resources.
			}
			m_bDisposed = true;
		}

		[Conditional( "DEBUG" )]
		protected void AssertNotDisposed()
		{
			if( this.m_bDisposed ) throw new ObjectDisposedException( this.GetType().ToString() );
		}

		#endregion
	}
}
