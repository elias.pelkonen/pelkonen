using System;

namespace InvCommon
{
	/// <summary>
	/// Config class for reading configuration settings.
	/// </summary>

    [Serializable]
    public abstract class CConfig : IDisposable
	{
		#region Member Variables

		public event EventHandler ConfigRefresh;
		private bool m_bDisposed = false;

		#endregion

		#region Abstract Methods

		// string
		public abstract bool GetValue( string Section, string Key, out string Result );
		public abstract bool GetValue( string Section, string Key, out string Result, object Default, ref string ErrorMessage );
		// int
		public abstract bool GetValue( string Section, string Key, out int Result );
		public abstract bool GetValue( string Section, string Key, out int Result, object Default, ref string ErrorMessage );
		// float
		public abstract bool GetValue( string Section, string Key, out float Result );
		public abstract bool GetValue( string Section, string Key, out float Result, object Default, ref string ErrorMessage );
		// bool
		public abstract bool GetValue( string Section, string Key, out bool Result );
		public abstract bool GetValue( string Section, string Key, out bool Result, object Default, ref string ErrorMessage );

		// accessors
		public abstract string Path { get; set; }
		public abstract bool ActiveConfig { get;}

		#endregion

		#region Virtual Methods

		protected virtual void OnConfigRefresh( EventArgs args )
		{
			lock( this )
			{
				ConfigRefresh?.Invoke( this, args );
			}
		}

		#endregion

		#region IDisposable Members

		public void Close()
		{
			Dispose();
		}

		public void Dispose()
		{
			Dispose( true );
			GC.SuppressFinalize( this );
		}

		protected virtual void Dispose( bool disposing )
		{
			if( !this.m_bDisposed )
			{
				if( disposing )
				{
					// Dispose managed resources.
				}
				// Release unmanaged resources.
			}
			m_bDisposed = true;
		}

		protected void AssertNotDisposed()
		{
			if( this.m_bDisposed ) throw new ObjectDisposedException( this.GetType().ToString() );
		}

		#endregion
	}
}
