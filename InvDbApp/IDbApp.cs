﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using static InvCommon.CDBApp;

namespace InvCommon
{
    public interface IDbApp
    {
        char ParameterToken { get; }
        SqlErrorCollection GetSqlErrorCollection { get; }
        int DBCommandTimeout { get; set; }
        string TableToken { get; }

        void CloseConnection(IDbConnection connection);
        DataTable CreateDataTable(string TableTypeName);
        DataSet ExecuteDataSet(IDbConnection connection, string storedProcedureName, out IDataParameterCollection parameterReturn, SqlTransaction tran = null, params SqlParameterInfo[] parameterInfo);
        DataSet ExecuteDataSet(IDbConnection connection, string storedProcedureName, SqlTransaction tran = null, params object[] parameterValues);
        DataSet ExecuteDataSet(IDbConnection connection, string storedProcedureName, params object[] parameterValues);
        DataSet ExecuteDataSet(IDbConnection connection, string commandText, SqlTransaction tran);
        DataSet ExecuteDataSet(IDbConnection connection, string commandText);
        int ExecuteNonQuery(IDbConnection connection, string storedProcedureName, out IDataParameterCollection parameterReturn, params SqlParameterInfo[] parameterInfo);
        int ExecuteNonQuery(IDbConnection connection, string commandText);
        int ExecuteNonQuery(IDbConnection connection, string storedProcedureName, params object[] parameterValues);
        int ExecuteNonQuery(IDbConnection connection, string storedProcedureName, SqlTransaction tran = null, params object[] parameterValues);
        IDataReader ExecuteReader(IDbConnection connection, string storedProcedureName, params SqlParameterInfo[] Parameters);
        IDataReader ExecuteReader(IDbConnection connection, string storedProcedureName, params object[] parameterValues);
        IDataReader ExecuteReader(IDbConnection connection, string commandText, SqlTransaction tran = null);
        IDataReader ExecuteReader(IDbConnection connection, string storedProcedureName, SqlTransaction tran = null, params object[] parameterValues);
        IDataReader ExecuteReader(IDbConnection connection, IDbCommand command);
        IDataReader ExecuteReader(IDbConnection connection, IDbCommand command, CommandBehavior cmdBehavior);
        object ExecuteScalar(IDbConnection connection, string storedProcedureName, params object[] parameterValues);
        string GetConnectionString(string IniFileSection);
        IDbConnection OpenConnection(string sIniFileSection);
        IDbConnection OpenConnection();
        IDbCommand PrepareCommand(IDbConnection connection, CommandType commandType, string commandText, params SqlParameterInfo[] ParameterInfo);
        Tuple<string[], string[]> ReadCharArrFromData(string field1, string field2, DbDataReader Data, int length);
        string[] ReadCharArrFromData(string field, DbDataReader Data, int length);
        Dictionary<string, T> ReadDictionaryFromDB<T>(SqlConnection con, string procedure, object[] param);
        Dictionary<string, T> ReadDictionaryFromDB<T>(string procedure, object[] param);
        List<T> ReadListFromData<T>(DbDataReader Data);
        List<T> ReadListFromDB<T>(SqlConnection con, string procedure, SqlParameterInfo[] paramInfo);
        List<T> ReadListFromDB<T>(string procedure, object[] param);
        List<T> ReadListFromDB<T>(string procedure, object[] param, out string message);
        List<T> ReadListFromDB<T>(SqlConnection con, string procedure, object[] param);
        List<T> ReadListFromDB<T>(SqlConnection con, string procedure, object[] param, out string message);
        List<T> ReadListFromDB<T>(string procedure, SqlParameterInfo[] paramInfo);
        Dictionary<string, List<T>> ReadListFromDBWithKey<T>(SqlConnection con, string procedure, object[] param, string key, string stringValue = null);
        Dictionary<string, List<T>> ReadListFromDBWithKey<T>(string procedure, object[] param, string key, string stringValue = null);
        T ReadRecordFromDB<T>(string procedure, object[] param, out string message, string fieldNames = null);
        Tuple<T1, T2> ReadRecordFromDB<T1, T2>(SqlConnection con, string procedure, object[] param);
        T ReadRecordFromDB<T>(SqlConnection con, string procedure, object[] param, out string message, string fieldNames = null);
        T ReadRecordFromDB<T>(SqlConnection con, string procedure, object[] param, string fieldNames = null);
        Tuple<T1, T2> ReadRecordFromDB<T1, T2>(string procedure, object[] param);
        T ReadRecordFromDB<T>(string procedure, object[] param, string fieldNames = null);
        T ReadRecordFromDBWithStatus<T>(SqlConnection con, string procedure, object[] param, ref DBResponse dbResponse, string fieldNames = null);
        T ReadRecordFromDBWithStatus<T>(string procedure, object[] param, ref DBResponse dbResponse, string fieldNames = null);
        Tuple<List<T1>, List<T2>, List<T3>> ReadTupleListFromDB<T1, T2, T3>(SqlConnection con, string procedure, object[] param);
        Tuple<List<T1>, List<T2>, List<T3>> ReadTupleListFromDB<T1, T2, T3>(string procedure, object[] param);
        Tuple<List<T1>, List<T2>> ReadTupleListFromDB<T1, T2>(string procedure, object[] param);
        Tuple<List<T1>, List<T2>> ReadTupleListFromDB<T1, T2>(SqlConnection con, string procedure, object[] param);
        bool SaveDataToDB(SqlConnection con, SqlTransaction tran, string storedProc, object[] Params, (string, string, string) inData, out List<(string, string, string)> outData, ref string message, ref string validation);
        bool SaveDataToDB(SqlConnection con, string storedProc, object[] Params, ref string message, ref string validation);
        bool SaveDataToDB(SqlConnection con, string storedProc, object[] Params, ref Tuple<string, string, string> outData, ref string message);
        bool SaveDataToDB(SqlConnection con, string storedProc, object[] Params, ref Tuple<string, string, string> outData, ref string message, ref string validation);
        bool SaveDataToDB(SqlConnection con, SqlTransaction tran, string storedProc, object[] Params);
        bool SaveDataToDB(SqlConnection con, string storedProc, object[] Params, ref string message);
        bool SaveDataToDB(SqlConnection con, SqlTransaction tran, string storedProc, object[] Params, ref Tuple<string, string, string> outData, ref string message);
        bool SaveDataToDB(SqlConnection con, SqlTransaction tran, string storedProc, object[] Params, ref Tuple<string, string, string> outData, ref string message, ref string validation);
        bool SaveDataToDB(SqlConnection con, SqlTransaction tran, string storedProc, object[] Params, (string, string, string) inData, out List<(string, string, string)> outData, ref string message);
        bool SaveDataToDB(SqlConnection con, SqlTransaction tran, string storedProc, object[] Params, ref string message);
        bool SaveDataToDB(SqlConnection con, SqlTransaction tran, string storedProc, object[] Params, ref string message, ref string validation);
        bool SaveDataToDB(SqlConnection con, string storedProc, object[] Params);
        bool SaveDataToDB(string storedProc, object[] Params, ref Tuple<string, string, string> outData, ref string message);
        bool SaveDataToDB(string storedProc, object[] Params, ref string message, ref string validation);
        bool SaveDataToDB(string storedProc, object[] Params, ref string message);
        bool SaveDataToDB(string storedProc, object[] Params);
        bool SaveDataToDB(string storedProc, object[] Params, ref Tuple<string, string, string> outData, ref string message, ref string validation);
        void SwitchConnection(string sIniFileSection, IDbConnection connection);
        int UpdateDataSet(IDbConnection connection, DataSet dataSet, string tableName, UpdateBehavior behavior);
        int UpdateDataSet(DataSet dataSet, string tableName, IDbCommand insertCommand, IDbCommand updateCommand, IDbCommand deleteCommand, UpdateBehavior behavior);
        int UpdateDataSet(IDbConnection connection, DataSet dataSet, UpdateBehavior behavior);
    }
}
