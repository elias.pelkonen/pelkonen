﻿using System;
using System.Data.Common;
using System.Collections.Generic;

namespace InvCommon
{
    public static class DbDataReaderExtensions
    {
        private const int VarBinaryMax = 8000;

        private static readonly Dictionary<string, Type> DataTypeMap = new Dictionary<string, Type>()
        {
            { "int", typeof(int)},
            { "bigint", typeof(long)},
            { "smallint", typeof(short)},
            { "char", typeof(string) },
            { "varchar", typeof(string) },
            { "nvarchar", typeof(string) },
            { "date", typeof(DateTime) },
            { "datetime", typeof(DateTime) },
            { "decimal", typeof(decimal) },
            { "bit", typeof(bool)},
            { "float", typeof(double)},
            { "double", typeof(double) },
            { "varbinary", typeof(byte[]) }
        };

        public static short GetInt16Null(this DbDataReader reader, string columnName, bool returnDefaultOnMissing = false) => GetTNull(reader, (r, i) => r.GetInt16(i), columnName, returnDefaultOnMissing);
        public static int GetInt32Null(this DbDataReader reader, string columnName, bool returnDefaultOnMissing = false) => GetTNull(reader, (r, i) => r.GetInt32(i), columnName, returnDefaultOnMissing);
        public static long GetInt64Null(this DbDataReader reader, string columnName, bool returnDefaultOnMissing = false) => GetTNull(reader, (r, i) => r.GetInt64(i), columnName, returnDefaultOnMissing);
        public static string GetStringNull(this DbDataReader reader, string columnName, bool returnDefaultOnMissing = false) => GetTNull(reader, (r, i) => r.GetString(i), columnName, returnDefaultOnMissing);
        public static DateTime GetDateTimeNull(this DbDataReader reader, string columnName, bool returnDefaultOnMissing = false) => GetTNull(reader, (r, i) => r.GetDateTime(i), columnName, returnDefaultOnMissing);
        public static decimal GetDecimalNull(this DbDataReader reader, string columnName, bool returnDefaultOnMissing = false) => GetTNull(reader, (r, i) => r.GetDecimal(i), columnName, returnDefaultOnMissing);
        public static bool GetBooleanNull(this DbDataReader reader, string columnName, bool returnDefaultOnMissing = false) => GetTNull(reader, (r, i) => r.GetBoolean(i), columnName, returnDefaultOnMissing);
        public static float GetFloatNull(this DbDataReader reader, string columnName, bool returnDefaultOnMissing = false) => GetTNull(reader, (r, i) => r.GetFloat(i), columnName, returnDefaultOnMissing);
        public static double GetDoubleNull(this DbDataReader reader, string columnName, bool returnDefaultOnMissing = false) => GetTNull(reader, (r, i) => r.GetDouble(i), columnName, returnDefaultOnMissing);
        public static byte[] GetBytesNull(this DbDataReader reader, string columnName, bool returnDefaultOnMissing = false) => GetTNull(reader, (r, i) => (byte[])r.GetValue(i), columnName, returnDefaultOnMissing);

        public static T GetTNull<T>(this DbDataReader reader, Func<DbDataReader, int, T> getter, string columnName, bool returnDefaultOnMissing = false)
        {
            int ordinal;

            try
            {
                ordinal = reader.GetOrdinal(columnName);
            }
            catch (IndexOutOfRangeException)
            {
                ordinal = -1;
            }

            if (ordinal < 0)
            {
                if (returnDefaultOnMissing)
                {
                    return default(T);
                }
                else
                {
                    throw new Exception($"Expected column missing in data reader: {columnName}");
                }
            }

            if (!DataTypeMap.ContainsKey(reader.GetDataTypeName(ordinal)))
            {
                throw new Exception($"Unexpected data type returned from database: {reader.GetDataTypeName(ordinal)}, it must be added to DataTypeMap in DbDataReaderExtensions");
            }

            if (!DataTypeMap[reader.GetDataTypeName(ordinal)].Equals(typeof(T)))
            {
                throw new Exception($"Data type mismatch on column {columnName}.  Model expects {typeof(T)}, data returns {DataTypeMap[reader.GetDataTypeName(ordinal)]}");
            }

            T value = default(T);
            try
            {
                value = !reader.IsDBNull(ordinal) ? getter.Invoke(reader, ordinal) : default(T);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return value;
        }
    }
}
