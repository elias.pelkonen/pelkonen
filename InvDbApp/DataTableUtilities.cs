﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace InvCommon
{
    public static class DataTableUtilities
    {
        /// <summary>
        /// Converts any generic IEnumerable collection to a data table
        /// </summary>
        /// <typeparam name = "T" > DataType to model the table after</typeparam>
        /// <returns>The new DataTable</returns>
        public static DataTable ToDataTable<T>(this IEnumerable<T> items)
        {
            DataTable table = CreateDataTable<T>();

            foreach(T item in items)
            {
                item.AddToDataTable(table);
            }

            return table;
        }

        /// <summary>
        /// Creates a DataTable schema containing a column for all public properties in the given type T that do not have the NotMapped attribute
        /// </summary>
        /// <typeparam name = "T" > DataType to model the table after</typeparam>
        /// <returns>The new DataTable</returns>
        public static DataTable CreateDataTable<T>()
        {
            DataTable table = new DataTable();

            foreach (PropertyInfo prop in typeof(T).GetProperties())
            {
                if (Attribute.GetCustomAttribute(prop, typeof(NotMapped)) == null)
                {
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                }
            }

            return table;
        }

        /// <summary>
        /// Creates a DataTable schema containing a column for all public properties in the given type that do not have the NotMapped attribute
        /// </summary>
        /// <param name="t">DataType to model the table after</param>
        /// <returns>The new DataTable</returns>
        public static DataTable CreateDataTable(Type t)
        {
            DataTable table = new DataTable();

            foreach (PropertyInfo prop in t.GetProperties())
            {
                if (Attribute.GetCustomAttribute(prop, typeof(NotMapped)) == null)
                {
                    table.Columns.Add(prop.Name, prop.PropertyType);
                }
            }

            return table;
        }

        /// <summary>
        /// Creates a DataTable schema containing a column for all public properties in the type of the given object that do not have the NotMapped attribute
        /// </summary>
        /// <param name="t">DataType to model the table after</param>
        /// <returns>The new DataTable</returns>
        public static DataTable CreateDataTable(object a)
        {
            return CreateDataTable(a.GetType());
        }

        /// <summary>
        /// Adds this object to a given DataTable using reflected property names
        /// </summary>
        /// <param name="a">The object to add to the table</param>
        /// <param name="table">The table to add the object to</param>
        /// <returns>The new row created within the table, the row is already added, do not call row.add.</returns>
        public static DataRow AddToDataTable(this object a, DataTable table)
        {
            DataRow row = table.NewRow();

            foreach (PropertyInfo prop in a.GetType().GetProperties())
            {
                if (table.Columns.Contains(prop.Name))
                {
                    row[prop.Name] = prop.GetValue(a) ?? DBNull.Value;
                }
            }

            table.Rows.Add(row);
            return row;
        }

        /// <summary>
        /// Converts the current row to a dictionary of objects by string column name
        /// </summary>
        /// <param name="reader">the reader to convert</param>
        /// <returns>Dictionary with a key of column name, value of the cell value as an object</returns>
        public static Dictionary<string, object> ToDictionary(this DataTableReader reader)
        {
            return Enumerable
                .Range(0, reader.FieldCount)
                .ToDictionary(i => reader.GetName(i), i => reader.GetValue(i));
        }
    }
}
