﻿using System;

namespace InvCommon
{
    [AttributeUsage(AttributeTargets.Property)]
    public class NotMapped : Attribute
    {
    }
}
