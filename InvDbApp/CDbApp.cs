using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

namespace InvCommon
{
	/// <summary>
	/// Summary description for CDb class.
	/// </summary>
	[Serializable]
	public class CDBApp : CApp, IDbApp
    {
		#region Member Variables

		[Instance]
		private bool m_bDisposed = false;
		private static bool m_bIsGetSpidRunning = false;
		protected bool m_bHandleExcOnDataSetUpdate;
		[NonSerialized]
		protected Hashtable commandCache = new Hashtable();
		//[NonSerialized]
		//protected IDbCommand command;
		//[NonSerialized]
		//protected IDbConnection connection;
		protected string m_sDbServerIniFileSection = CDefault.dbServerIniSection;
		protected int m_nDBCommandTimeout;
		protected int m_nDetailSqlTrace;
		private string m_sConnectionString;

		// Private variables introduced by CR from April 04, 2008 by Miroslav
		//private bool m_AttachSqlInfoMessageEventHandler;
		//private bool m_FireInfoMessageEventOnUserErrors;
		[SoftInstance]
		private SqlErrorCollection m_SqlErrorCollection;

		new protected bool? m_bStateInitialized = null;

		protected delegate void m_newInfoMessageEventHandler();
		protected event m_newInfoMessageEventHandler newInfoMessage;

		private static Dictionary<string, List<TableTypeColumns>> m_TableTypeDef = null;

		#endregion

		#region Constructors/Destructors

		protected CDBApp()
		{
		}

		public CDBApp( string ApplicationName )
			: this( ApplicationName, new ListDictionary() )
		{
		}

		public CDBApp( CApp capp )
			: base( capp )
		{
		}

		public CDBApp( CApp capp, bool bPassSoftInstance )
			: base( capp, bPassSoftInstance )
		{
		}

		public CDBApp( string ApplicationName, CApp capp )
			: base( ApplicationName, capp )
		{
		}

		public CDBApp( string ApplicationName, CApp capp, bool bPassSoftInstance )
			: base( ApplicationName, capp, bPassSoftInstance )
		{
		}

		public CDBApp( string ApplicationName, ListDictionary AdditionalInfo )
			: base( ApplicationName, AdditionalInfo )
		{
		}

		~CDBApp()
		{
			Dispose( false );
		}

		#endregion

		#region Properties

		//public IDbCommand LastExecutedCommand
		//{
		//	get { AssertNotDisposed(); return command; }
		//}

		// Parameter prefix for stored procedures
		public char ParameterToken
		{
			get { return '@'; }
		}

		// default name for the table in the DataSet
		public string TableToken
		{
			get { return "Table"; }
		}

		public int DBCommandTimeout
		{
			get
			{
				AssertNotDisposed();
				return m_nDBCommandTimeout;
			}

			set
			{
				m_nDBCommandTimeout = value;
				Log( eSeverity.Trace, string.Format( @"DBCommandTimeout set to: {0} seconds", m_nDBCommandTimeout ) );
			}
		}
		/// <summary>
		/// Introduced by CR from April 04, 2008 by Miroslav
		/// </summary>
		//public bool AttachSqlInfoMessageEventHandler
		//{
		//	get { return m_AttachSqlInfoMessageEventHandler; }
		//	set
		//	{
		//		m_AttachSqlInfoMessageEventHandler = value;
		//		if( connection != null )
		//		{
		//			if( value )
		//				( (SqlConnection) connection ).InfoMessage += new SqlInfoMessageEventHandler( InfoMessage );
		//			else
		//				( (SqlConnection) connection ).InfoMessage -= new SqlInfoMessageEventHandler( InfoMessage );
		//		}
		//	}
		//}

		/// <summary>
		/// Introduced by CR from April 04, 2008 by Miroslav
		/// </summary>
		//public bool FireInfoMessageEventOnUserErrors
		//{
		//	get { return m_FireInfoMessageEventOnUserErrors; }
		//	set
		//	{
		//		m_FireInfoMessageEventOnUserErrors = value;
		//		if( connection != null )
		//		{
		//			( (SqlConnection) connection ).FireInfoMessageEventOnUserErrors = value;
		//		}
		//	}
		//}

		/// <summary>
		/// Introduced by CR from April 04, 2008 by Miroslav
		/// </summary>
		public SqlErrorCollection GetSqlErrorCollection
		{
			get { return m_SqlErrorCollection; }
		}

		#endregion

		#region Overridden Methods

		// have to override since there is extra data
		protected override bool InitCommon()
		{
			string sErrorMsg = null;

			m_bStateChanged = base.InitCommon();

			if( m_bStateInitialized == null || m_bStateInitialized == false )
			{
				// on top of CApp specific data
				if( !ReadConfig( m_sAppName, "HandleExcOnDataSetUpdate", out m_bHandleExcOnDataSetUpdate, ref sErrorMsg ) )
					ReadConfig( "General", "HandleExcOnDataSetUpdate", out m_bHandleExcOnDataSetUpdate, false );
				// need to extract every property value from the application section first, then,
				// if it doesn't exist, get this value from general section in the INI file
				if( !ReadConfig( m_sAppName, "DBCommandTimeout", out m_nDBCommandTimeout, ref sErrorMsg ) )
					ReadConfig( "General", "DBCommandTimeout", out m_nDBCommandTimeout, 60 );

				if( !ReadConfig( m_sAppName, "DetailSqlTrace", out m_nDetailSqlTrace, ref sErrorMsg ) )
					ReadConfig( "General", "DetailSqlTrace", out m_nDetailSqlTrace, 0 );
				m_bStateInitialized = true;
			}
			return m_bStateChanged;
		}
		#endregion

		#region Virtual Methods

		//public virtual IDbCommand PrepareCommand(
		//	CommandType commandType,
		//	string commandText,
		//	params SqlParameterInfo[] ParameterInfo )
		//{
		//	return PrepareCommand( connection, commandType, commandText, ParameterInfo );
		//}

		public virtual IDbCommand PrepareCommand(
			IDbConnection connection,
			CommandType commandType,
			string commandText,
			params SqlParameterInfo[] ParameterInfo )
		{
			int i;
			string sHashKey;

			ArgumentValidation.CheckForEmptyString( commandText, "commandText" );
			ArgumentValidation.CheckForNullReference( connection, "connection" );

			IDbCommand command = new SqlCommand
			{
				CommandText = commandText,
				CommandType = commandType,
				Connection = connection as SqlConnection,
				CommandTimeout = m_nDBCommandTimeout
			};

			if( commandType != CommandType.StoredProcedure )
				return command;

			if( ParameterInfo.Length > 0 )
			{
				bool bHasParamInfo = false;
				foreach( SqlParameterInfo parameterInfo in ParameterInfo )	// figure out if parameter info provided
				{
					if( !IsEmpty( parameterInfo.ParameterName ) )	// is it just a value or ParamInfo as well
					{
						bHasParamInfo = true;
						break;
					}
				}
				if( bHasParamInfo )
				{
					for( int j = 0; j < ParameterInfo.Length; j++ )
					{
						SqlParameter parameter = ( (SqlCommand) command ).Parameters.Add(
							ParameterInfo[j].ParameterName,
							ParameterInfo[j].DBType,
							ParameterInfo[j].Size,
							ParameterInfo[j].SourceColumn );

						// how about Direction.Return/Output ???? you are responsible yourself
						parameter.Direction = ParameterInfo[j].Direction;
						if( ParameterInfo[j].Value != null )
							parameter.Value = ParameterInfo[j].Value;
					}
				}
				else	// no parameter info
				{
					sHashKey = CreateHashKey( connection.ConnectionString, commandText );
					if( !commandCache.ContainsKey( sHashKey ) )	// cached already
					{
						CacheCommandParameters( connection, command, commandText );
					}

					IDataParameter[] cloneParameterSet = (IDataParameter[]) commandCache[sHashKey];
					foreach( IDataParameter p in cloneParameterSet )
					{
						command.Parameters.Add( (IDataParameter) ( (ICloneable) p ).Clone() );
					}

					// assign parameter values
					if( command.Parameters.Count > 1 )
					{
						//if ((command.Parameters.Count - 1) != parameterInfo.Length)	// ReturnValue does not count
						//{
						//	throw new InvalidOperationException("The number of parameters does not match number of values for stored procedure.");
						//}
						i = 0;
						foreach( IDataParameter parameter in command.Parameters )
						{
							if( parameter.Direction == System.Data.ParameterDirection.Input )
							{
								if( ParameterInfo.Length - 1 < i || ParameterInfo.Length == 0 )
									throw new InvalidOperationException( "The number of parameters does not match number of values for stored procedure." );

								parameter.Value = ParameterInfo[i].Value ?? DBNull.Value;
								i++;
							}
							else if( parameter.Direction == System.Data.ParameterDirection.InputOutput
								|| parameter.Direction == System.Data.ParameterDirection.Output )
							{
								parameter.Value = DBNull.Value;
								i++;
							}
						}
					}
				}
			}

			return command;
		}

		protected virtual IDataReader DoExecuteReader(
			IDbConnection connection,
			IDbCommand command,
			CommandBehavior cmdBehavior,
			SqlTransaction tran = null )
		{
			try
			{
				FixDbComandParameters( ref command );

				if( tran == null )
					if( command.Transaction != null )
						tran = command.Transaction as SqlTransaction;

				if( !m_bIsGetSpidRunning )
				{
					if( m_nDetailSqlTrace == 1 )
						Log( eSeverity.Info, "\tDoExecuteReader\tSPID = " + GetSpid( connection, tran ) + "\tSQL:", command.CommandText );
					else
						Log( eSeverity.Debug, "\tDoExecuteReader\t\tSQL:", command.CommandText );
				}
				IDataReader reader = command.ExecuteReader( cmdBehavior );
				return reader;
			}
			catch( SqlException ex )
			{
				throw new CException( "DoExecuteReader failed\tSQL:" + command.CommandText, eExceptionType.Data, ex, eDetailType.Medium, ex.Number );
			}
			catch( Exception ex )
			{
				throw new CException( "DoExecuteReader failed\tSQL:" + command.CommandText, eExceptionType.Data, ex, eDetailType.Medium );
			}
		}

		protected virtual void DoExecuteDataSet(
			IDbConnection connection,
			IDbCommand command,
			DataSet dataSet,
			string[] tableNames,
			SqlTransaction tran = null )
		{
			ArgumentValidation.CheckForNullReference( command, "command" );
			ArgumentValidation.CheckForNullReference( tableNames, "tableNames" );

			if( tran == null )
				if( command.Transaction != null )
					tran = command.Transaction as SqlTransaction;

			if( tableNames.Length == 0 )
			{
				throw new ArgumentException( "The table name array used to map results to user-specified table names cannot be empty.", "tableNames" );
			}

			for( int i = 0; i < tableNames.Length; i++ )
			{
				ArgumentValidation.CheckForNullReference( tableNames[i], string.Concat( "tableNames[", i, "]" ) );
				ArgumentValidation.CheckForEmptyString( tableNames[i], string.Concat( "tableNames[", i, "]" ) );
			}

			if( m_nDetailSqlTrace == 1 )
				Log( eSeverity.Info, "\tDoExecuteDataSet\tSPID = " + GetSpid( connection, tran ) + "\tSQL:", command.CommandText );
			else
				Log( eSeverity.Debug, "\tDoExecuteDataSet\t\tSQL:", command.CommandText );

			try
			{
				using( DbDataAdapter adapter = GetDataAdapter( command.Connection, null, UpdateBehavior.Standard ) )
				{
					( (IDbDataAdapter) adapter ).SelectCommand = command;

					string systemCreatedTableNameRoot = TableToken;
					for( int i = 0; i < tableNames.Length; i++ )
					{
						string systemCreatedTableName = ( i == 0 ) ? systemCreatedTableNameRoot : systemCreatedTableNameRoot + i;
						adapter.TableMappings.Add( systemCreatedTableName, tableNames[i] );
					}

					adapter.Fill( dataSet );

					// keep the command, the dataset was populated from, so that we can update it later on
					lock( commandCache.SyncRoot )
					{
						foreach( string tableName in tableNames )
						{
							string hashKey = CreateHashKey( dataSet.GetHashCode(), tableName );
							if( !commandCache.ContainsKey( hashKey ) )
								commandCache.Add( hashKey, command );
						}
					}
				}
			}
			catch( SqlException ex )
			{
				throw new CException( "DoExecuteDataSet failed\tSQL:" + command.CommandText, eExceptionType.Data, ex, eDetailType.Medium, ex.Number );
			}
			catch( Exception ex )
			{
				throw new CException( "DoExecuteDataSet failed\tSQL:" + command.CommandText, eExceptionType.Data, ex, eDetailType.Medium );
			}
		}

		protected virtual int DoUpdateDataSet(
			IDbConnection connection,
			DataSet dataSet,
			string tableName,
			IDbCommand insertCommand,
			IDbCommand updateCommand,
			IDbCommand deleteCommand,
			UpdateBehavior behavior,
			SqlTransaction tran = null )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( dataSet, "dataSet" );
			//ArgumentValidation.CheckForEmptyString(tableName, "tableName");
			//ArgumentValidation.CheckForEmptyString(selectCommandText, "selectCommandText");

			try
			{
				if( tran == null )
					if( insertCommand.Transaction != null )
						tran = insertCommand.Transaction as SqlTransaction;
					else if( updateCommand.Transaction != null )
						tran = updateCommand.Transaction as SqlTransaction;
					else if( deleteCommand.Transaction != null )
						tran = deleteCommand.Transaction as SqlTransaction;

				if( connection.State == ConnectionState.Closed )
				{
					// Data adapter reopens connection if necessary but we do it here explicitly
					// to use context_info that is set during call to OpenConnection
					connection = OpenConnection();
					if( insertCommand != null )
						insertCommand.Connection = connection;
					if( updateCommand != null )
						updateCommand.Connection = connection;
					if( deleteCommand != null )
						deleteCommand.Connection = connection;
				}

				using( SqlDataAdapter adapter = GetDataAdapter( connection, "", behavior ) )
				{
					if( IsEmpty( tableName ) )
						tableName = dataSet.Tables[0].TableName;

					string hashKey = CreateHashKey( dataSet.GetHashCode(), tableName );
					if( commandCache.Contains( hashKey ) )
					{
						adapter.SelectCommand = (SqlCommand) commandCache[hashKey];
						// create default insert/update commands
						if( insertCommand == null && updateCommand == null && deleteCommand == null )
						{
							SqlCommandBuilder cmdBuilder = new SqlCommandBuilder( adapter );
						}
					}

					if( insertCommand != null )
					{
						( (IDbDataAdapter) adapter ).InsertCommand = insertCommand;
						if( m_nDetailSqlTrace == 1 )
							Log( eSeverity.Info, "\tDoUpdateDataSet\tSPID = " + GetSpid( connection, tran ), "SQL insert:  " + insertCommand.CommandText );
						else
							Log( eSeverity.Debug, "\tDoUpdateDataSet\t", "SQL insert:  " + insertCommand.CommandText );
					}
					if( updateCommand != null )
					{
						( (IDbDataAdapter) adapter ).UpdateCommand = updateCommand;
						if( m_nDetailSqlTrace == 1 )
							Log( eSeverity.Info, "\tDoUpdateDataSet\tSPID = " + GetSpid( connection, tran ), "SQL update:  " + updateCommand.CommandText );
						else
							Log( eSeverity.Debug, "\tDoUpdateDataSet\t", "SQL update:  " + updateCommand.CommandText );
					}
					if( deleteCommand != null )
					{
						( (IDbDataAdapter) adapter ).DeleteCommand = deleteCommand;
						if( m_nDetailSqlTrace == 1 )
							Log( eSeverity.Info, "\tDoUpdateDataSet\tSPID = " + GetSpid( connection, tran ), "SQL delete:  " + deleteCommand.CommandText );
						else
							Log( eSeverity.Debug, "\tDoUpdateDataSet\t", "SQL delete:  " + deleteCommand.CommandText );
					}

					try
					{
						DateTime startTime = DateTime.Now;
						int rows = adapter.Update( dataSet.Tables[tableName] );
						//int rows = adapter.Update(dataSet, tableName);
						Log( eSeverity.Debug, "\tTable updated : ", tableName );
						return rows;
					}
					catch( SqlException ex )
					{
						Log( eSeverity.Error, "Update or Insert failed : ", tableName );
						throw ex;				// will be caught at higher level...
					}
				}
			}
			catch( SqlException ex )
			{
				if( insertCommand == null &&
					updateCommand == null &&
					deleteCommand == null )
				{
					throw new CException( "DoUpdateDataSet failed\r\nSQL Error: " + ex.Message, eExceptionType.Data, ex, eDetailType.Medium, ex.Number );
				}
				else
				{
					throw new CException( "DoUpdateDataSet failed" +
						"\r\nSQL insert:  " +
							( ( insertCommand == null ) ? "" : insertCommand.CommandText ) +
						"\r\nSQL update:  " +
							( ( updateCommand == null ) ? "" : updateCommand.CommandText ) +
						"\r\nSQL delete:  " +
							( ( deleteCommand == null ) ? "" : deleteCommand.CommandText ) +
						"\r\nSQL Error: " +
							ex.Message,
						eExceptionType.Data, ex, eDetailType.Medium );
				}
			}
			catch( Exception ex )
			{
				if( insertCommand == null &&
					updateCommand == null &&
					deleteCommand == null )
				{
					throw new CException( "DoUpdateDataSet failed\r\nSQL Error: " + ex.Message, eExceptionType.Data, ex, eDetailType.Medium );
				}
				else
				{
					throw new CException( "DoUpdateDataSet failed" +
						"\r\nSQL insert:  " +
							( ( insertCommand == null ) ? "" : insertCommand.CommandText ) +
						"\r\nSQL update:  " +
							( ( updateCommand == null ) ? "" : updateCommand.CommandText ) +
						"\r\nSQL delete:  " +
							( ( deleteCommand == null ) ? "" : deleteCommand.CommandText ) +
						"\r\nSQL Error: " +
							ex.Message,
						eExceptionType.Data, ex, eDetailType.Medium );
				}
			}
			finally
			{
				CloseConnection( connection );
			}
		}

		protected virtual SqlDataAdapter GetDataAdapter(
			IDbConnection connection,
			string selectCommandText,
			UpdateBehavior updateBehavior )
		{
			//string queryStringToBeFilledInLater = String.Empty;
			SqlDataAdapter adapter = new SqlDataAdapter( selectCommandText ?? String.Empty, (SqlConnection) connection );

			/// if UpdateBehavior.Continue then skip failed record and continue update
			if( updateBehavior == UpdateBehavior.Continue )
			{
				adapter.RowUpdated += new SqlRowUpdatedEventHandler( OnSqlRowUpdated );
			}
			return adapter;
		}

		protected virtual void OnSqlRowUpdated(
			object sender,
			SqlRowUpdatedEventArgs rowThatCouldNotBeWritten )
		{
			// Listens for the RowUpdate event on a dataadapter to support UpdateBehavior.Continue
			if( rowThatCouldNotBeWritten.RecordsAffected == 0 )
			{
				if( rowThatCouldNotBeWritten.Errors != null )
				{
					StringBuilder ColumnList = new StringBuilder( "Failed to update row. Values (" );
					foreach( object field in rowThatCouldNotBeWritten.Row.ItemArray )
					{
						ColumnList.Append( " '" + field.ToString() + "', " );
					}
					ColumnList.Replace( ",", ")", ColumnList.Length - 2, 1 );

					rowThatCouldNotBeWritten.Row.RowError = ColumnList.ToString();
					rowThatCouldNotBeWritten.Status = UpdateStatus.SkipCurrentRow;

					if( m_bHandleExcOnDataSetUpdate )
					{
						if( ExceptionPoolSize == 0 ) ExceptionPoolSize = 1000;
						CException cex = new CException( ColumnList.ToString(), eExceptionType.Data, rowThatCouldNotBeWritten.Errors );
						HandleException( cex );
					}
				}
			}
		}

		/// <summary> 
		/// Function sets CONTEXT_INFO for a given connection.
		/// Information contains the logon user and the host name.
		/// To retrieve this information from CONTEXT_INFO call dbo.fn_sysGetUserAndHostFromContextInfo()
		/// </summary> 
		protected virtual void SetContext( IDbConnection Connection )
		{
			// disabled
			//try
			//{
			//	string sContextInfo = "DECLARE @ContextInfo VARBINARY (128); SELECT @ContextInfo = dbo.fn_sysFormatLogonUserAndHostForContext_Info('" + LogonUser + "', '" + UserHostName + "'); SET CONTEXT_INFO @ContextInfo;";
			//	ExecuteNonQuery( Connection, sContextInfo );
			//}
			//catch
			//{
			//	try
			//	{
			//		// if no format function defined try explicit format (less reliable)
			//		string sContextInfo = "DECLARE @ContextInfo VARBINARY (128); SELECT @ContextInfo = CONVERT (VARBINARY (128) ,'LogonUser=' + CAST ('" + LogonUser + "' AS VARCHAR (50)) + ';UserHostName=' + CAST ('" + UserHostName + "' AS VARCHAR (50)) + REPLICATE (' ',128)); SET CONTEXT_INFO @ContextInfo;";
			//		ExecuteNonQuery( Connection, sContextInfo );
			//	}
			//	catch
			//	{
			//		// we don't care - context is not critical
			//	}
			//}
		}

		protected virtual string CacheCommandParameters( IDbConnection connection, IDbCommand command, string commandText )
		{
			string sHashKey = CreateHashKey( connection.ConnectionString, commandText );

			// need to clone connection & command not to affect open transactions
			SqlConnection tempConnection = (SqlConnection) ( (ICloneable) connection ).Clone();
			SqlCommand tempCommand = new SqlCommand
			{
				CommandText = commandText,
				CommandType = CommandType.StoredProcedure
			};
			try
			{
				tempConnection.Open();
				tempCommand.Connection = tempConnection;
				SqlCommandBuilder.DeriveParameters( tempCommand );	// round trip to sql server
			}
			finally
			{
				if( tempConnection != null ) tempConnection.Close();
			}

			int i = 0;
			// what if Parameters == null ???
			IDataParameter[] cloneParameterSet = new IDataParameter[tempCommand.Parameters.Count];
			foreach( IDataParameter parameter in tempCommand.Parameters )
			{
				IDataParameter cloneParameter = (IDataParameter) ( (ICloneable) parameter ).Clone();
				cloneParameterSet[i++] = cloneParameter;
			}
			lock( commandCache.SyncRoot )
			{
				if( !commandCache.ContainsKey( sHashKey ) )
					commandCache.Add( sHashKey, cloneParameterSet );
			}

			return sHashKey;
		}

		#endregion

		#region Public Connection Wrappers

		public IDbConnection OpenConnection()
		{
			AssertNotDisposed();
			if( string.IsNullOrWhiteSpace( m_sConnectionString ) )
				m_sConnectionString = GetConnectionString( m_sDbServerIniFileSection );
			//if( connection != null && connection.State == ConnectionState.Open && connection.ConnectionString == m_sConnectionString )
			//	return connection;

			SqlConnection connection = new SqlConnection( m_sConnectionString );
			try
			{
				//if( m_AttachSqlInfoMessageEventHandler )
				//	( (SqlConnection) connection ).InfoMessage += new SqlInfoMessageEventHandler( InfoMessage );
				//if( m_FireInfoMessageEventOnUserErrors )
				//	( (SqlConnection) connection ).FireInfoMessageEventOnUserErrors = true;

				for( int i = 0; i < 3; i++ )
				{
					connection.Open();

					if( m_nDetailSqlTrace == 1 )
						Log( eSeverity.Info, "Connection opened\tSPID = " + GetSpid( connection ) + "\t", HidePassword( m_sConnectionString ), ";\tState = " + connection.State );
					else
						Log( eSeverity.Debug, "Connection opened\t\t", HidePassword( m_sConnectionString ), ";\tState = " + connection.State );
					if( connection.State == ConnectionState.Open || connection.State == ConnectionState.Connecting )
						break;
					else
						Thread.Sleep( 50 );
				}
				SetContext( connection );
			}
			catch
			{
				Log( eSeverity.Error, "Connection failed : ", HidePassword( m_sConnectionString ) );
				throw;
			}
			return connection;
		}

		public IDbConnection OpenConnection( string sIniFileSection )
		{
			if( m_sDbServerIniFileSection != sIniFileSection )
				m_sConnectionString = "";
			m_sDbServerIniFileSection = sIniFileSection;
			return OpenConnection();
		}

		public void SwitchConnection( string sIniFileSection, IDbConnection connection )
		{
			string sTempDbServerIniFileSection = m_sDbServerIniFileSection;

			m_sConnectionString = "";

			try
			{
				CloseConnection( connection );
			}
			catch
			{
				throw;
			}
			m_sDbServerIniFileSection = sIniFileSection;
			try
			{
				OpenConnection();
			}
			catch( SqlException sqle )
			{
				m_sDbServerIniFileSection = sTempDbServerIniFileSection;
				throw sqle;
			}
		}

		public void CloseConnection( IDbConnection connection )
		{
			AssertNotDisposed();
			try
			{
				if( connection != null && connection.State == ConnectionState.Open )
				{
					if( m_nDetailSqlTrace == 1 )
						Log( eSeverity.Info, "Connection closed\tSPID = " + GetSpid( connection ) + "\t", HidePassword( connection.ConnectionString ) );
					else
						Log( eSeverity.Debug, "Connection closed\t\t", HidePassword( connection.ConnectionString ) );

					connection.Close();
				}
			}
			catch
			{
				Log( eSeverity.Error, "CloseConnection() failed : ", HidePassword( connection.ConnectionString ) );
				throw;
			}
		}

		public string GetConnectionString( string IniFileSection )
		{
			bool bPersistSecurityInfo = false;
			//string sApplicationName;
			string sUserID = null;
			string sPassword = null;
			string sInitialCatalog = null;
			//string sProvider = null;
			bool bUserPresent = false;
			int nConnectionTimeout = 60;

			if( m_sConnectionString != null && m_sConnectionString != "" )
			{
				return m_sConnectionString;
			}

			//sApplicationName = StateInfo["ApplicationName"].ToString();

			ArgumentValidation.CheckForEmptyString( IniFileSection, "IniFileSection" );
			if( ReadConfig( IniFileSection, "DBServerName", out string sSQLServerName ) )
			{
				ReadConfig( IniFileSection, "DBInitCatalog", out sInitialCatalog );
				//ReadConfig(IniFileSection, "Provider", out sProvider);
				bUserPresent = ReadConfig( IniFileSection, "DBUserName", out sUserID, false );
				if( bUserPresent == true && ( sUserID.CompareTo( "False" ) == 0 ) | ( sUserID.CompareTo( "" ) == 0 ) ) bUserPresent = false;
				if( bUserPresent ) ReadConfig( IniFileSection, "DBPassword*", out sPassword );
				ReadConfig( IniFileSection, "ConnectionTimeout", out nConnectionTimeout, 120 );
			}
			if( bUserPresent )
				return String.Format( @"Persist Security Info={0};Data Source={1};User ID={2};Password={3};Initial Catalog={4};Application Name={5};Connection Timeout={6}",
					bPersistSecurityInfo,
					sSQLServerName,
					sUserID,
					sPassword,
					sInitialCatalog,
					ApplicationName,
					nConnectionTimeout );
			else
				return String.Format( @"Persist Security Info={0};Data Source={1};Initial Catalog={2};Application Name={3};Integrated Security=SSPI;Connection Timeout={4}",
					bPersistSecurityInfo,
					sSQLServerName,
					sInitialCatalog,
					ApplicationName,
					nConnectionTimeout );
		}

		#endregion

		#region Public ExecuteNonQuery Wrappers

		//public int ExecuteNonQuery( IDbCommand command )	// because command contains connection
		//{
		//	AssertNotDisposed();
		//	ArgumentValidation.CheckForNullReference( command, "command" );
		//	if( m_nDetailSqlTrace == 1 )
		//		Log( eSeverity.Info, "\tExecuteNonQuery\tSPID = " + GetSpid() + "\tSQL:", command.CommandText );
		//	else
		//		Log( eSeverity.Debug, "\tExecuteNonQuery\t\tSQL:", command.CommandText );
		//	command = command;
		//	return command.ExecuteNonQuery();
		//}

		//// simply execute sql statement
		//public int ExecuteNonQuery( string commandText )
		//{
		//	AssertNotDisposed();
		//	ArgumentValidation.CheckForEmptyString( commandText, "commandText" );
		//	if( m_nDetailSqlTrace == 1 )
		//		Log( eSeverity.Info, "\tExecuteNonQuery\tSPID = " + GetSpid() + "\tSQL:", commandText );
		//	else
		//		Log( eSeverity.Debug, "\tExecuteNonQuery\t\tSQL:", commandText );
		//	command = PrepareCommand( connection, CommandType.Text, commandText );
		//	return command.ExecuteNonQuery();
		//}

		// simply execute sql statement on a separate connection
		public int ExecuteNonQuery(
			IDbConnection connection,
			string commandText )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( commandText, "commandText" );
			if( m_nDetailSqlTrace == 1 )
				Log( eSeverity.Info, "\tExecuteNonQuery\tSPID = " + GetSpid( connection ) + "\tSQL:", commandText );
			else
				Log( eSeverity.Debug, "\tExecuteNonQuery\t\tSQL:", commandText );
			IDbCommand command = PrepareCommand( connection, CommandType.Text, commandText );
			return command.ExecuteNonQuery();
		}

		// stored procedure
		//public int ExecuteNonQuery(
		//	string storedProcedureName,
		//	params object[] parameterValues )
		//{
		//	AssertNotDisposed();
		//	ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
		//	SqlParameterInfo[] Parameters = null;
		//	if( parameterValues.Length > 0 )
		//	{
		//		Parameters = new SqlParameterInfo[parameterValues.Length];
		//		for( int i = 0; i < parameterValues.Length; i++ )
		//		{
		//			Parameters[i] = new SqlParameterInfo( parameterValues[i] );	// just values, no info
		//		}
		//	}
		//	string sParams = " ";
		//	foreach( object Param in parameterValues )
		//		sParams += Param == null ? "null," : Param.ToString() + ",";
		//	if( m_nDetailSqlTrace == 1 )
		//		Log( eSeverity.Info, "\tExecuteNonQuery\tSPID = " + GetSpid() + "\tSQL:", storedProcedureName + sParams );
		//	else
		//		Log( eSeverity.Debug, "\tExecuteNonQuery\t\tSQL:", storedProcedureName + sParams );
		//	command = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
		//	return command.ExecuteNonQuery();
		//}

		// stored procedure on a separate connection
		public int ExecuteNonQuery(
			IDbConnection connection,
			string storedProcedureName,
			params object[] parameterValues )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			SqlParameterInfo[] Parameters = null;
			if( parameterValues.Length > 0 )
			{
				Parameters = new SqlParameterInfo[parameterValues.Length];
				for( int i = 0; i < parameterValues.Length; i++ )
				{
					Parameters[i] = new SqlParameterInfo( parameterValues[i] );	// just values, no info
				}
			}
			string sParams = " ";
			foreach( object Param in parameterValues )
				sParams += Param == null ? "null," : Param.ToString() + ",";
			if( m_nDetailSqlTrace == 1 )
				Log( eSeverity.Info, "\tExecuteNonQuery\tSPID = " + GetSpid( connection ) + "\tSQL:", storedProcedureName + sParams );
			else
				Log( eSeverity.Debug, "\tExecuteNonQuery\t\tSQL:", storedProcedureName + sParams );
			IDbCommand command = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
			return command.ExecuteNonQuery();
		}

		public int ExecuteNonQuery(
			IDbConnection connection,
			string storedProcedureName,
			SqlTransaction tran = null,
			params object[] parameterValues )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			SqlParameterInfo[] Parameters = null;
			if( parameterValues.Length > 0 )
			{
				Parameters = new SqlParameterInfo[parameterValues.Length];
				for( int i = 0; i < parameterValues.Length; i++ )
				{
					Parameters[i] = new SqlParameterInfo( parameterValues[i] );	// just values, no info
				}
			}
			string sParams = " ";
			foreach( object Param in parameterValues )
				sParams += Param == null ? "null," : Param.ToString() + ",";
			if( m_nDetailSqlTrace == 1 )
				Log( eSeverity.Info, "\tExecuteNonQuery\tSPID = " + GetSpid( connection, tran ) + "\tSQL:", storedProcedureName + sParams );
			else
				Log( eSeverity.Debug, "\tExecuteNonQuery\t\tSQL:", storedProcedureName + sParams );
			IDbCommand command = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
			command.Transaction = tran;
			return command.ExecuteNonQuery();
		}

		#endregion

		#region Public ExecuteReader Wrappers

		public IDataReader ExecuteReader(
			IDbConnection connection,
			IDbCommand command )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( command, "command" );
			return DoExecuteReader( connection, command, CommandBehavior.Default );
		}

		public IDataReader ExecuteReader(
			IDbConnection connection,
			IDbCommand command,
			CommandBehavior cmdBehavior )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( command, "command" );
			return DoExecuteReader( connection, command, cmdBehavior );
		}

		//// simply execute a query, the command is not cached
		//public IDataReader ExecuteReader( string commandText )
		//{
		//	AssertNotDisposed();
		//	ArgumentValidation.CheckForEmptyString( commandText, "commandText" );
		//	command = PrepareCommand( connection, CommandType.Text, commandText );
		//	return DoExecuteReader( command, CommandBehavior.Default );
		//}

		// simply execute a query, the command is not cached
		//public IDataReader ExecuteReader( string commandText, CommandBehavior cmdBehavior, SqlTransaction tran = null )
		//{
		//	AssertNotDisposed();
		//	ArgumentValidation.CheckForEmptyString( commandText, "commandText" );
		//	IDbCommand command = PrepareCommand( connection, CommandType.Text, commandText );
		//	command.Transaction = tran;
		//	return DoExecuteReader( command, cmdBehavior );
		//}

		// simply execute a query on a separate connection, the command is not cached
		public IDataReader ExecuteReader(
			IDbConnection connection,
			string commandText,
			SqlTransaction tran = null
			)
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( commandText, "commandText" );
			IDbCommand command = PrepareCommand( connection, CommandType.Text, commandText );
			command.Transaction = tran;
			return DoExecuteReader( connection, command, CommandBehavior.Default );
		}

		// stored procedure 
		//public IDataReader ExecuteReader(
		//	string storedProcedureName,
		//	params object[] parameterValues )
		//{
		//	AssertNotDisposed();
		//	ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
		//	SqlParameterInfo[] Parameters = null;
		//	if( parameterValues.Length > 0 )
		//	{
		//		Parameters = new SqlParameterInfo[parameterValues.Length];
		//		for( int i = 0; i < parameterValues.Length; i++ )
		//		{
		//			Parameters[i] = new SqlParameterInfo( parameterValues[i] );	// just values, no info
		//		}
		//	}
		//	IDbCommand command = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
		//	return DoExecuteReader( command, CommandBehavior.Default );
		//}

		// stored procedure 
		public IDataReader ExecuteReader(
			IDbConnection connection,
			string storedProcedureName,
			params object[] parameterValues )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			SqlParameterInfo[] Parameters = null;
			if( parameterValues.Length > 0 )
			{
				Parameters = new SqlParameterInfo[parameterValues.Length];
				for( int i = 0; i < parameterValues.Length; i++ )
				{
					Parameters[i] = new SqlParameterInfo( parameterValues[i] ); // just values, no info
				}
			}
			IDbCommand command = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
			return DoExecuteReader( connection, command, CommandBehavior.Default );
		}

		public IDataReader ExecuteReader(
			IDbConnection connection,
			string storedProcedureName,
			params SqlParameterInfo[] Parameters )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			IDbCommand command = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
			return DoExecuteReader( connection, command, CommandBehavior.Default );
		}

		// stored procedure 
		public IDataReader ExecuteReader(
			IDbConnection connection,
			string storedProcedureName,
			SqlTransaction tran = null,
			params object[] parameterValues )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			SqlParameterInfo[] Parameters = null;
			if( parameterValues.Length > 0 )
			{
				Parameters = new SqlParameterInfo[parameterValues.Length];
				for( int i = 0; i < parameterValues.Length; i++ )
				{
					Parameters[i] = new SqlParameterInfo( parameterValues[i] );	// just values, no info
				}
			}
			IDbCommand command = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
			command.Transaction = tran;
			return DoExecuteReader( connection, command, CommandBehavior.Default );
		}

		#endregion

		#region Public ExecuteDataSet Wrappers

		//public DataSet ExecuteDataSet( IDbCommand command )
		//{
		//	AssertNotDisposed();
		//	ArgumentValidation.CheckForNullReference( command, "command" );
		//	DataSet dataSet = new DataSet();
		//	dataSet.Locale = CultureInfo.InvariantCulture;

		//	DoExecuteDataSet( command, dataSet, new string[] { TableToken } );
		//	return dataSet;
		//}

		//// simply execute sql query
		//public DataSet ExecuteDataSet( string commandText )
		//{
		//	AssertNotDisposed();
		//	ArgumentValidation.CheckForEmptyString( commandText, "commandText" );
		//	IDbCommand command = PrepareCommand( connection, CommandType.Text, commandText );
		//	DataSet dataSet = new DataSet();
		//	dataSet.Locale = CultureInfo.InvariantCulture;
		//	DoExecuteDataSet( command, dataSet, new string[] { TableToken } );
		//	return dataSet;
		//}

		// simply execute sql query on a separate connection
		public DataSet ExecuteDataSet(
			IDbConnection connection,
			string commandText )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( commandText, "commandText" );
			IDbCommand command = PrepareCommand( connection, CommandType.Text, commandText );
			DataSet dataSet = new DataSet
			{
				Locale = CultureInfo.InvariantCulture
			};
			DoExecuteDataSet( connection, command, dataSet, new string[] { TableToken } );
			return dataSet;
		}

		// simply execute sql query on a separate connection within a transaction
		public DataSet ExecuteDataSet(
			IDbConnection connection,
			string commandText,
			SqlTransaction tran )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( commandText, "commandText" );
			IDbCommand command = PrepareCommand( connection, CommandType.Text, commandText );
			command.Transaction = tran;
			DataSet dataSet = new DataSet
			{
				Locale = CultureInfo.InvariantCulture
			};
			DoExecuteDataSet( connection, command, dataSet, new string[] { TableToken }, tran );
			return dataSet;
		}

		// stored procedure
		//public DataSet ExecuteDataSet(
		//	string storedProcedureName,
		//	params object[] parameterValues )
		//{
		//	AssertNotDisposed();
		//	ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
		//	SqlParameterInfo[] Parameters = null;
		//	if( parameterValues.Length > 0 )
		//	{
		//		Parameters = new SqlParameterInfo[parameterValues.Length];
		//		for( int i = 0; i < parameterValues.Length; i++ )
		//		{
		//			Parameters[i] = new SqlParameterInfo( parameterValues[i] );	// just values, no info
		//		}
		//	}
		//	IDbCommand command = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
		//	DataSet dataSet = new DataSet();
		//	dataSet.Locale = CultureInfo.InvariantCulture;
		//	DoExecuteDataSet( command, dataSet, new string[] { TableToken } );
		//	return dataSet;
		//}

		// stored procedure on a separate connection
		public DataSet ExecuteDataSet(
			IDbConnection connection,
			string storedProcedureName,
			params object[] parameterValues )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			SqlParameterInfo[] Parameters = null;
			if( parameterValues.Length > 0 )
			{
				Parameters = new SqlParameterInfo[parameterValues.Length];
				for( int i = 0; i < parameterValues.Length; i++ )
				{
					Parameters[i] = new SqlParameterInfo( parameterValues[i] );	// just values, no info
				}
			}
			IDbCommand command = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
			DataSet dataSet = new DataSet
			{
				Locale = CultureInfo.InvariantCulture
			};
			DoExecuteDataSet( connection, command, dataSet, new string[] { TableToken } );
			return dataSet;
		}

		// stored procedure on a separate connection
		public DataSet ExecuteDataSet(
			IDbConnection connection,
			string storedProcedureName,
			SqlTransaction tran = null,
			params object[] parameterValues )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			SqlParameterInfo[] Parameters = null;
			if( parameterValues.Length > 0 )
			{
				Parameters = new SqlParameterInfo[parameterValues.Length];
				for( int i = 0; i < parameterValues.Length; i++ )
				{
					Parameters[i] = new SqlParameterInfo( parameterValues[i] );	// just values, no info
				}
			}
			IDbCommand command = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
			command.Transaction = tran;
			DataSet dataSet = new DataSet
			{
				Locale = CultureInfo.InvariantCulture
			};
			DoExecuteDataSet( connection, command, dataSet, new string[] { TableToken }, tran );
			return dataSet;
		}

		#endregion

		#region Public UpdateDataSet Wrappers

		//public int UpdateDataSet(
		//	DataSet dataSet,
		//	UpdateBehavior behavior )
		//{
		//	return DoUpdateDataSet( connection, dataSet, null, null, null, null, behavior );
		//}

		public int UpdateDataSet(
			IDbConnection connection,
			DataSet dataSet,
			UpdateBehavior behavior )
		{
			return DoUpdateDataSet( connection, dataSet, null, null, null, null, behavior );
		}

		//public int UpdateDataSet(
		//	DataSet dataSet,
		//	string tableName,
		//	UpdateBehavior behavior )
		//{
		//	return DoUpdateDataSet( connection, dataSet, tableName, null, null, null, behavior );
		//}

		public int UpdateDataSet(
			IDbConnection connection,
			DataSet dataSet,
			string tableName,
			UpdateBehavior behavior )
		{
			return DoUpdateDataSet( connection, dataSet, tableName, null, null, null, behavior );
		}

		public int UpdateDataSet(
			DataSet dataSet,
			string tableName,
			IDbCommand insertCommand,
			IDbCommand updateCommand,
			IDbCommand deleteCommand,
			UpdateBehavior behavior )
		{
			if( insertCommand == null && updateCommand == null && deleteCommand == null )
			{
				throw new ArgumentException( "At least one command must be initialized." );
			}

#pragma warning disable IDE0031 // Use null propagation
			IDbConnection connection = insertCommand != null
				? insertCommand.Connection : updateCommand != null
				? updateCommand.Connection : deleteCommand != null
				? deleteCommand.Connection : null;
#pragma warning restore IDE0031 // Use null propagation

			return DoUpdateDataSet( connection, dataSet, tableName, insertCommand, updateCommand, deleteCommand, behavior );
		}

		#endregion

		#region Public ExecuteScalar Wrappers

		//public object ExecuteScalar( IDbCommand command )
		//{
		//	AssertNotDisposed();
		//	ArgumentValidation.CheckForNullReference( command, "command" );
		//	if( m_nDetailSqlTrace == 1 )
		//		Log( eSeverity.Info, "\tExecuteScalar\tSPID = " + GetSpid() + "\tSQL:", command.CommandText );
		//	else
		//		Log( eSeverity.Debug, "\tExecuteScalar\t\tSQL:", command.CommandText );
		//	command = command;
		//	return command.ExecuteScalar();
		//}

		//public object ExecuteScalar( string commandText )
		//{
		//	AssertNotDisposed();
		//	ArgumentValidation.CheckForEmptyString( commandText, "commandText" );
		//	if( m_nDetailSqlTrace == 1 )
		//		Log( eSeverity.Info, "\tExecuteScalar\tSPID = " + GetSpid() + "\tSQL:", commandText );
		//	else
		//		Log( eSeverity.Debug, "\tExecuteScalar\t\tSQL:", commandText );
		//	command = PrepareCommand( connection, CommandType.Text, commandText );
		//	return command.ExecuteScalar();
		//}

		//public object ExecuteScalar(
		//	string storedProcedureName,
		//	params object[] parameterValues )
		//{
		//	AssertNotDisposed();
		//	ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
		//	SqlParameterInfo[] Parameters = null;
		//	if( parameterValues.Length > 0 )
		//	{
		//		Parameters = new SqlParameterInfo[parameterValues.Length];
		//		for( int i = 0; i < parameterValues.Length; i++ )
		//		{
		//			Parameters[i] = new SqlParameterInfo( parameterValues[i] );	// just values, no info
		//		}
		//	}
		//	string sParams = " ";
		//	foreach( object Param in parameterValues )
		//		sParams += Param == null ? "null," : Param.ToString() + ",";
		//	if( m_nDetailSqlTrace == 1 )
		//		Log( eSeverity.Info, "\tExecuteScalar\tSPID = " + GetSpid() + "\tSQL:", storedProcedureName + sParams );
		//	else
		//		Log( eSeverity.Debug, "\tExecuteScalar\t\tSQL:", storedProcedureName + sParams );
		//	command = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
		//	return command.ExecuteScalar();
		//}

		public object ExecuteScalar(
			IDbConnection connection,
			string storedProcedureName,
			params object[] parameterValues )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			SqlParameterInfo[] Parameters = null;
			if( parameterValues.Length > 0 )
			{
				Parameters = new SqlParameterInfo[parameterValues.Length];
				for( int i = 0; i < parameterValues.Length; i++ )
				{
					Parameters[i] = new SqlParameterInfo( parameterValues[i] );	// just values, no info
				}
			}
			string sParams = " ";
			foreach( object Param in parameterValues )
				sParams += Param == null ? "null," : Param.ToString() + ",";
			if( m_nDetailSqlTrace == 1 )
				Log( eSeverity.Info, "\tExecuteScalar\tSPID = " + GetSpid( connection ) + "\tSQL:", storedProcedureName + sParams );
			else
				Log( eSeverity.Debug, "\tExecuteScalar\t\tSQL:", storedProcedureName + sParams );
			IDbCommand command = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
			return command.ExecuteScalar();
		}

		#endregion

		#region Public Wrappers with Return Parameters

		// ExecuteNonQuery
		//public int ExecuteNonQuery(
		//	string storedProcedureName,
		//	out IDataParameterCollection parameterReturn,
		//	params SqlParameterInfo[] parameterInfo )
		//{
		//	return ExecuteNonQuery( connection, storedProcedureName, out parameterReturn, parameterInfo );
		//}
		public int ExecuteNonQuery(
			IDbConnection connection,
			string storedProcedureName,
			out IDataParameterCollection parameterReturn,
			params SqlParameterInfo[] parameterInfo )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			SqlParameterInfo[] Parameters = null;
			if( parameterInfo.Length > 0 )
			{
				Parameters = new SqlParameterInfo[parameterInfo.Length];
				for( int i = 0; i < parameterInfo.Length; i++ )
				{
					Parameters[i] = parameterInfo[i];	// values, and info
				}
			}
			string sParams = " ";
			foreach( SqlParameterInfo Param in parameterInfo )
				sParams += Param.Value == null ? "null," : Param.Value.ToString() + ",";
			if( m_nDetailSqlTrace == 1 )
				Log( eSeverity.Info, "\tExecuteNonQuery\tSPID = " + GetSpid( connection ) + "\tSQL:", storedProcedureName + sParams );
			else
				Log( eSeverity.Debug, "\tExecuteNonQuery\t\tSQL:", storedProcedureName + sParams );
			IDbCommand command = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );

			int nResult = command.ExecuteNonQuery();
			parameterReturn = command.Parameters;
			return nResult;
		}

		// ExecuteDataSet
		//public DataSet ExecuteDataSet(
		//	string storedProcedureName,
		//	out IDataParameterCollection parameterReturn,
		//	SqlTransaction tran = null,
		//	params SqlParameterInfo[] parameterInfo )
		//{
		//	return ExecuteDataSet( connection, storedProcedureName, out parameterReturn, tran, parameterInfo );
		//}
		public DataSet ExecuteDataSet(
			IDbConnection connection,
			string storedProcedureName,
			out IDataParameterCollection parameterReturn,
			SqlTransaction tran = null,
			params SqlParameterInfo[] parameterInfo )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			SqlParameterInfo[] Parameters = null;
			if( parameterInfo.Length > 0 )
			{
				Parameters = new SqlParameterInfo[parameterInfo.Length];
				for( int i = 0; i < parameterInfo.Length; i++ )
				{
					Parameters[i] = parameterInfo[i];	// just values, no info
				}
			}
			IDbCommand command = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
			command.Transaction = tran;
			DataSet dataSet = new DataSet
			{
				Locale = CultureInfo.InvariantCulture
			};
			DoExecuteDataSet( connection, command, dataSet, new string[] { TableToken } );
			parameterReturn = command.Parameters;
			return dataSet;
		}
		#endregion

		#region Public High Level Template Access Methods

		public T ReadRecordFromDB<T>( SqlConnection con, string procedure, object[] param, out string message, string fieldNames = null )
		{
			using( DbDataReader Data = (DbDataReader) ExecuteReader( con, procedure, param ) )
			{
				if( Data != null && Data.HasRows )
				{
					message = null;
					Data.Read();
					if( fieldNames == null )
						return (T) Activator.CreateInstance( typeof( T ), new object[] { Data } );
					else
						return (T) Activator.CreateInstance( typeof( T ), new object[] { Data, fieldNames } );
				}
				else
					message = "Query returned no data";
			}
			return default( T );
		}

		public T ReadRecordFromDB<T>( SqlConnection con, string procedure, object[] param, string fieldNames = null )
		{
			return ReadRecordFromDB<T>( con, procedure, param, out _, fieldNames );

			//using( DbDataReader Data = (DbDataReader) ExecuteReader( con, procedure, param ) )
			//{
			//	if( Data != null && Data.HasRows )
			//	{
			//		Data.Read();
			//		if( fieldNames == null )
			//			return (T) Activator.CreateInstance( typeof( T ), new object[] { Data } );
			//		else
			//			return (T) Activator.CreateInstance( typeof( T ), new object[] { Data, fieldNames } );
			//	}
			//}
			//return default( T );
		}

		public T ReadRecordFromDB<T>( string procedure, object[] param, out string message, string fieldNames = null )
		{
			using( SqlConnection con = OpenConnection( "SQLServer" ) as SqlConnection )
			{
				return ReadRecordFromDB<T>( con, procedure, param, out message, fieldNames );
			}
		}

		public T ReadRecordFromDB<T>( string procedure, object[] param, string fieldNames = null )
		{
			using( SqlConnection con = OpenConnection( "SQLServer" ) as SqlConnection )
			{
				return ReadRecordFromDB<T>( con, procedure, param, fieldNames );
			}
		}

		public T ReadRecordFromDBWithStatus<T>( SqlConnection con, string procedure, object[] param, ref DBResponse dbResponse, string fieldNames = null )
		{
			T rset = default( T );
			using( DbDataReader Data = (DbDataReader) ExecuteReader( con, procedure, param ) )
			{
				if( Data != null && Data.HasRows )
				{
					Data.Read();
					if( fieldNames == null )
						rset = (T) Activator.CreateInstance( typeof( T ), new object[] { Data } );
					else
						rset = (T) Activator.CreateInstance( typeof( T ), new object[] { Data, fieldNames } );

					Data.NextResult();
					Data.Read();
					if( Data.HasRows )
					{
						dbResponse = new DBResponse( Data );
					}
				}
			}
			return rset;
		}

		public T ReadRecordFromDBWithStatus<T>( string procedure, object[] param, ref DBResponse dbResponse, string fieldNames = null )
		{
			using( SqlConnection con = OpenConnection( "SQLServer" ) as SqlConnection )
			{
				return ReadRecordFromDBWithStatus<T>( con, procedure, param, ref dbResponse, fieldNames );
			}
		}

		public Tuple<T1, T2> ReadRecordFromDB<T1, T2>( SqlConnection con, string procedure, object[] param )
		{
			using( DbDataReader Data = (DbDataReader) ExecuteReader( con, procedure, param ) )
			{
				if( Data != null && Data.HasRows )
				{
					Data.Read();
					T1 t1 = (T1) Activator.CreateInstance( typeof( T1 ), new object[] { Data } );

					T2 t2 = default( T2 );
					Data.NextResult();
					if( Data.HasRows )
					{
						Data.Read();
						t2 = (T2) Activator.CreateInstance( typeof( T2 ), new object[] { Data } );
					}
					return new Tuple<T1, T2>( t1, t2 );
				}
			}
			return default( Tuple<T1, T2> );
		}

		public Tuple<T1, T2> ReadRecordFromDB<T1, T2>( string procedure, object[] param )
		{
			using( SqlConnection con = OpenConnection( "SQLServer" ) as SqlConnection )
			{
				return ReadRecordFromDB<T1, T2>( con, procedure, param );
			}
		}

		public Dictionary<string, T> ReadDictionaryFromDB<T>( SqlConnection con, string procedure, object[] param )
		{
			Dictionary<string, T> dictionary = null;
			using( DbDataReader Data = (DbDataReader) ExecuteReader( con, procedure, param ) )
			{
				if( Data != null && Data.HasRows )
				{
					dictionary = new Dictionary<string, T>();
					int n = 1;
					while( Data.Read() )
						dictionary.Add( n++.ToString(), (T) Activator.CreateInstance( typeof( T ), new object[] { Data } ) );
				}
			}
			return dictionary;
		}

		public Dictionary<string, T> ReadDictionaryFromDB<T>( string procedure, object[] param )
		{
			using( SqlConnection con = OpenConnection( "SQLServer" ) as SqlConnection )
			{
				return ReadDictionaryFromDB<T>( con, procedure, param );
			}
		}

		public List<T> ReadListFromDB<T>( SqlConnection con, string procedure, object[] param, out string message )
		{
			List<T> list = null;
			using( DbDataReader Data = (DbDataReader) ExecuteReader( con, procedure, param ) )
			{
				if( Data != null && Data.HasRows )
				{
					message = null;
					list = new List<T>();
					while( Data.Read() )
						list.Add( (T) Activator.CreateInstance( typeof( T ), new object[] { Data } ) );
				}
				else
					message = "Query returned no data";
			}
			return list;
		}

		public List<T> ReadListFromDB<T>( SqlConnection con, string procedure, object[] param )
		{
			return ReadListFromDB<T>( con, procedure, param, out _ );
			//List<T> list = null;
			//using( DbDataReader Data = (DbDataReader) ExecuteReader( con, procedure, param ) )
			//{
			//	if( Data != null && Data.HasRows )
			//	{
			//		list = new List<T>();
			//		while( Data.Read() )
			//			list.Add( (T) Activator.CreateInstance( typeof( T ), new object[] { Data } ) );
			//	}
			//}
			//return list;
		}

		public List<T> ReadListFromDB<T>( string procedure, object[] param, out string message )
		{
			using( SqlConnection con = OpenConnection( "SQLServer" ) as SqlConnection )
			{
				return ReadListFromDB<T>( con, procedure, param, out message );
			}
		}

		public List<T> ReadListFromDB<T>( string procedure, object[] param )
		{
			using( SqlConnection con = OpenConnection( "SQLServer" ) as SqlConnection )
			{
				return ReadListFromDB<T>( con, procedure, param );
			}
		}

		public List<T> ReadListFromDB<T>( SqlConnection con, string procedure, SqlParameterInfo[] paramInfo )
		{
			List<T> list = null;
			using( DbDataReader Data = (DbDataReader) ExecuteReader( con, procedure, paramInfo ) )
			{
				if( Data != null && Data.HasRows )
				{
					list = new List<T>();
					while( Data.Read() )
						list.Add( (T) Activator.CreateInstance( typeof( T ), new object[] { Data } ) );
				}
			}
			return list;
		}

		public List<T> ReadListFromDB<T>( string procedure, SqlParameterInfo[] paramInfo )
		{
			using( SqlConnection con = OpenConnection( "SQLServer" ) as SqlConnection )
			{
				return ReadListFromDB<T>( con, procedure, paramInfo );
			}
		}

		public List<T> ReadListFromData<T>( DbDataReader Data )
		{
			List<T> list = null;
			if( Data != null && Data.HasRows )
			{
				list = new List<T>();
				while( Data.Read() )
					list.Add( (T) Activator.CreateInstance( typeof( T ), new object[] { Data } ) );
			}
			return list;
		}

		public string[] ReadCharArrFromData( string field, DbDataReader Data, int length )
		{
			string[] list = null;
			if( Data != null && Data.HasRows )
			{
				list = new string[length];
				int n = 0;
				while( Data.Read() )
				{
					list[n++] = Data[field].ToString();
				}
			}
			return list;
		}

		public Tuple<string[], string[]> ReadCharArrFromData( string field1, string field2, DbDataReader Data, int length )
		{
			string[] list1 = null;
			string[] list2 = null;
			if( Data != null && Data.HasRows )
			{
				list1 = new string[length];
				list2 = new string[length];
				int n = 0;
				while( Data.Read() )
				{
					list1[n++] = Data[field1].ToString();
					list2[n++] = Data[field2].ToString();
				}
				return new Tuple<string[], string[]>( list1, list2 );
			}
			return default( Tuple<string[], string[]> );
		}

		public Dictionary<string, List<T>> ReadListFromDBWithKey<T>( SqlConnection con, string procedure, object[] param, string key, string stringValue = null )
		{
			Dictionary<string, List<T>> dict = null;
			List<T> list = null;
			using( DbDataReader Data = (DbDataReader) ExecuteReader( con, procedure, param ) )
			{
				if( Data != null && Data.HasRows )
				{
					dict = new Dictionary<string, List<T>>();
					list = new List<T>();
					string keyValue = null;
					while( Data.Read() )
					{
						if( keyValue == null )
							keyValue = Data[key].ToString();
						else if( keyValue != Data[key].ToString() )
						{
							dict.Add( keyValue, list );
							keyValue = Data[key].ToString();
							list = new List<T>();
						}
						if( typeof( T ) == typeof( string ) )
							list.Add( (T) Convert.ChangeType( Data[stringValue].ToString(), typeof( T ) ) );
						else
							list.Add( (T) Activator.CreateInstance( typeof( T ), new object[] { Data } ) );
					}
					dict.Add( keyValue, list );
				}
			}
			return dict;
		}

		public Dictionary<string, List<T>> ReadListFromDBWithKey<T>( string procedure, object[] param, string key, string stringValue = null )
		{
			using( SqlConnection con = OpenConnection( "SQLServer" ) as SqlConnection )
			{
				return ReadListFromDBWithKey<T>( con, procedure, param, key, stringValue );
			}
		}

		public Tuple<List<T1>, List<T2>> ReadTupleListFromDB<T1, T2>( string procedure, object[] param )
		{
			using( SqlConnection con = OpenConnection( "SQLServer" ) as SqlConnection )
			{
				return ReadTupleListFromDB<T1, T2>( con, procedure, param );
			}
		}

		public Tuple<List<T1>, List<T2>> ReadTupleListFromDB<T1, T2>( SqlConnection con, string procedure, object[] param )
		{
			Tuple<List<T1>, List<T2>> tuple = null;
			List<T1> list1 = null;
			List<T2> list2 = null;
			using( DbDataReader Data = (DbDataReader) ExecuteReader( con, procedure, param ) )
			{
				if( Data != null && Data.HasRows )
				{
					list1 = new List<T1>();
					list2 = new List<T2>();
					tuple = new Tuple<List<T1>, List<T2>>( list1, list2 );
					while( Data.Read() )
						list1.Add( (T1) Activator.CreateInstance( typeof( T1 ), new object[] { Data } ) );

					Data.NextResult();
					if( Data.HasRows )
						while( Data.Read() )
							list2.Add( (T2) Activator.CreateInstance( typeof( T2 ), new object[] { Data } ) );
				}
			}
			return tuple;
		}

		public Tuple<List<T1>, List<T2>, List<T3>> ReadTupleListFromDB<T1, T2, T3>( string procedure, object[] param )
		{
			using( SqlConnection con = OpenConnection( "SQLServer" ) as SqlConnection )
			{
				return ReadTupleListFromDB<T1, T2, T3>( con, procedure, param );
			}
		}

		public Tuple<List<T1>, List<T2>, List<T3>> ReadTupleListFromDB<T1, T2, T3>( SqlConnection con, string procedure, object[] param )
		{
			Tuple<List<T1>, List<T2>, List<T3>> tuple = null;
			List<T1> list1 = null;
			List<T2> list2 = null;
			List<T3> list3 = null;
			using( DbDataReader Data = (DbDataReader) ExecuteReader( con, procedure, param ) )
			{
				if( Data != null && Data.HasRows )
				{
					list1 = new List<T1>();
					list2 = new List<T2>();
					list3 = new List<T3>();
					tuple = new Tuple<List<T1>, List<T2>, List<T3>>( list1, list2, list3 );
					while( Data.Read() )
						list1.Add( (T1) Activator.CreateInstance( typeof( T1 ), new object[] { Data } ) );

					Data.NextResult();
					if( Data.HasRows )
						while( Data.Read() )
							list2.Add( (T2) Activator.CreateInstance( typeof( T2 ), new object[] { Data } ) );

					Data.NextResult();
					if( Data.HasRows )
						while( Data.Read() )
							list3.Add( (T3) Activator.CreateInstance( typeof( T3 ), new object[] { Data } ) );
				}
			}
			return tuple;
		}

		public bool SaveDataToDB( string storedProc, object[] Params )
		{
			string message = "";
			string validation = null;
			Tuple<string, string, string> outData = null;
			using( SqlConnection con = OpenConnection( "SQLServer" ) as SqlConnection )
			{
				return SaveDataToDB( con, null, storedProc, Params, ref outData, ref message, ref validation );
			}
		}

		public bool SaveDataToDB( string storedProc, object[] Params, ref string message )
		{
			string validation = null;
			Tuple<string, string, string> outData = null;
			using( SqlConnection con = OpenConnection( "SQLServer" ) as SqlConnection )
			{
				return SaveDataToDB( con, null, storedProc, Params, ref outData, ref message, ref validation );
			}
		}

		public bool SaveDataToDB( string storedProc, object[] Params, ref string message, ref string validation )
		{
			Tuple<string, string, string> outData = null;
			using( SqlConnection con = OpenConnection( "SQLServer" ) as SqlConnection )
			{
				return SaveDataToDB( con, null, storedProc, Params, ref outData, ref message, ref validation );
			}
		}

		public bool SaveDataToDB( string storedProc, object[] Params, ref Tuple<string, string, string> outData, ref string message )
		{
			string validation = null;
			using( SqlConnection con = OpenConnection( "SQLServer" ) as SqlConnection )
			{
				return SaveDataToDB( con, null, storedProc, Params, ref outData, ref message, ref validation );
			}
		}

		public bool SaveDataToDB( string storedProc, object[] Params, ref Tuple<string, string, string> outData, ref string message, ref string validation )
		{
			using( SqlConnection con = OpenConnection( "SQLServer" ) as SqlConnection )
			{
				return SaveDataToDB( con, null, storedProc, Params, ref outData, ref message, ref validation );
			}
		}

		public bool SaveDataToDB( SqlConnection con, string storedProc, object[] Params )
		{
			string message = "";
			string validation = null;
			Tuple<string, string, string> outData = null;
			return SaveDataToDB( con, null, storedProc, Params, ref outData, ref message, ref validation );
		}

		public bool SaveDataToDB( SqlConnection con, string storedProc, object[] Params, ref string message )
		{
			string validation = null;
			Tuple<string, string, string> outData = null;
			return SaveDataToDB( con, null, storedProc, Params, ref outData, ref message, ref validation );
		}

		public bool SaveDataToDB( SqlConnection con, string storedProc, object[] Params, ref string message, ref string validation )
		{
			Tuple<string, string, string> outData = null;
			return SaveDataToDB( con, null, storedProc, Params, ref outData, ref message, ref validation );
		}

		public bool SaveDataToDB( SqlConnection con, string storedProc, object[] Params, ref Tuple<string, string, string> outData, ref string message )
		{
			string validation = null;
			return SaveDataToDB( con, null, storedProc, Params, ref outData, ref message, ref validation );
		}

		public bool SaveDataToDB( SqlConnection con, string storedProc, object[] Params, ref Tuple<string, string, string> outData, ref string message, ref string validation )
		{
			return SaveDataToDB( con, null, storedProc, Params, ref outData, ref message, ref validation );
		}

		public bool SaveDataToDB( SqlConnection con, SqlTransaction tran, string storedProc, object[] Params )
		{
			string message = "";
			string validation = null;
			Tuple<string, string, string> outData = null;
			return SaveDataToDB( con, tran, storedProc, Params, ref outData, ref message, ref validation );
		}

		public bool SaveDataToDB( SqlConnection con, SqlTransaction tran, string storedProc, object[] Params, ref string message )
		{
			string validation = null;
			Tuple<string, string, string> outData = null;
			return SaveDataToDB( con, tran, storedProc, Params, ref outData, ref message, ref validation );
		}

		public bool SaveDataToDB( SqlConnection con, SqlTransaction tran, string storedProc, object[] Params, ref string message, ref string validation )
		{
			Tuple<string, string, string> outData = null;
			return SaveDataToDB( con, tran, storedProc, Params, ref outData, ref message, ref validation );
		}

		public bool SaveDataToDB( SqlConnection con, SqlTransaction tran, string storedProc, object[] Params, ref Tuple<string, string, string> outData, ref string message )
		{
			string validation = null;
			return SaveDataToDB( con, tran, storedProc, Params, ref outData, ref message, ref validation );
		}

		public bool SaveDataToDB( SqlConnection con, SqlTransaction tran, string storedProc, object[] Params, ref Tuple<string, string, string> outData, ref string message, ref string validation )
		{
			bool result = true;
			string msg = "";

			using( var data = ExecuteReader( con, storedProc, tran, Params ) as DbDataReader )
			{
				if( data != null && data.HasRows )
				{
					data.Read();
					msg = data["Message"].ToString();
					if( validation != null && Enumerable.Range( 0, data.FieldCount ).Any( i => data.GetName( i ).Equals( "Validation", StringComparison.InvariantCultureIgnoreCase ) ) )
						validation += data["Validation"].ToString();
					if( !string.IsNullOrWhiteSpace( validation ) )
					{
						HandleException( new CException( "Validation error in " + storedProc + " procedure:\r\n" + validation, eExceptionType.Process, eDetailType.Brief ) );
						result = false;
					}
					else if( outData != null )
					{
						data.NextResult();
						if( data.HasRows )
						{
							data.Read();
							outData = new Tuple<string, string, string>( outData.Item1 == null ? "" : data[outData.Item1].ToString(), outData.Item2 == null ? "" : data[outData.Item2].ToString(), outData.Item3 == null ? "" : data[outData.Item3].ToString() );
						}
					}
				}
				else
				{
					msg += "No data returned from " + storedProc + " procedure";
				}
				Log( eSeverity.Trace, msg );
				message += msg;
			}

			return result;
		}

		public bool SaveDataToDB( SqlConnection con, SqlTransaction tran, string storedProc, object[] Params, (string, string, string) inData, out List<(string, string, string)> outData, ref string message )
		{
			string validation = null;
			return SaveDataToDB( con, tran, storedProc, Params, inData, out outData, ref message, ref validation );
		}

		public bool SaveDataToDB( SqlConnection con, SqlTransaction tran, string storedProc, object[] Params, (string, string, string) inData, out List<(string, string, string)> outData, ref string message, ref string validation )
		{
			bool result = true;
			string msg = "";
			outData = null;

			using( var data = ExecuteReader( con, storedProc, tran, Params ) as DbDataReader )
			{
				if( data != null && data.HasRows )
				{
					data.Read();
					msg = data["Message"].ToString();
					if( validation != null && Enumerable.Range( 0, data.FieldCount ).Any( i => data.GetName( i ).Equals( "Validation", StringComparison.InvariantCultureIgnoreCase ) ) )
						validation += data["Validation"].ToString();
					if( !string.IsNullOrWhiteSpace( validation ) )
					{
						HandleException( new CException( "Validation error in " + storedProc + " procedure:\r\n" + validation, eExceptionType.Process, eDetailType.Brief ) );
						result = false;
					}
					else if( inData.Item1 != null )
					{
						data.NextResult();
						outData = new List<(string, string, string)>();
						if( data.HasRows )
							while( data.Read() )
								outData.Add( (inData.Item1 == null ? "" : data[inData.Item1].ToString(), inData.Item2 == null ? "" : data[inData.Item2].ToString(), inData.Item3 == null ? "" : data[inData.Item3].ToString()) );
					}
				}
				else
				{
					msg += "No data returned from " + storedProc + " procedure";
				}
				Log( eSeverity.Trace, msg );
				message += msg;
			}

			return result;
		}

		//public bool SaveDataToDB( SqlConnection con, SqlTransaction tran, string storedProc, object[] Params, ref (string Item1, string Item2, string Item3) outData, ref string message, ref string validation )
		//{
		//	bool result = true;
		//	string msg = "";
		//	using( var data = ExecuteReader( con, storedProc, tran, Params ) as DbDataReader )
		//	{
		//		if( data != null && data.HasRows )
		//		{
		//			data.Read();
		//			msg = data["Message"].ToString();
		//			if( validation != null && Enumerable.Range( 0, data.FieldCount ).Any( i => data.GetName( i ).Equals( "Validation", StringComparison.InvariantCultureIgnoreCase ) ) )
		//				validation += data["Validation"].ToString();
		//			if( !string.IsNullOrWhiteSpace( validation ) )
		//			{
		//				HandleException( new CException( "Validation error in " + storedProc + " procedure:\r\n" + validation, eExceptionType.Process, eDetailType.Brief ) );
		//				result = false;
		//			}
		//			else if( outData.Item1 != "" && outData.Item2 != "" && outData.Item3 != "" )
		//			{
		//				data.NextResult();
		//				if( data.HasRows )
		//				{
		//					data.Read();
		//					outData = (outData.Item1 == null ? "" : data[outData.Item1].ToString(), outData.Item2 == null ? "" : data[outData.Item2].ToString(), outData.Item3 == null ? "" : data[outData.Item3].ToString());
		//				}
		//			}
		//		}
		//		else
		//		{
		//			msg += "No data returned from " + storedProc + " procedure";
		//		}
		//		Log( eSeverity.Trace, msg );
		//		message += msg;
		//	}

		//	return result;
		//}

		public DataTable CreateDataTable( string TableTypeName )
		{
			if( m_TableTypeDef == null )
				m_TableTypeDef = new Dictionary<string, List<TableTypeColumns>>();
			if( !m_TableTypeDef.ContainsKey( TableTypeName ) )
			{
				var columns = ReadListFromDB<TableTypeColumns>( "dbo.up_GetTableTypeColumns", new SqlParameterInfo[] { new SqlParameterInfo( "@TableTypeName", SqlDbType.NVarChar, 128, ParameterDirection.Input, (object) TableTypeName ) } );
				if( columns == null )
					throw new CException( "SQL table type '" + TableTypeName + "' is not defined", eExceptionType.Config, eDetailType.Brief );
				m_TableTypeDef.Add( TableTypeName, columns );
			}
			var table = new DataTable();
			table.Clear();
			foreach( var item in m_TableTypeDef[TableTypeName] )
			{
				table.Columns.Add( item.Column );
			}

			return table;
		}

		public class DBResponse
		{
			public string Message { get; set; }
			public string Validation { get; set; }

			public DBResponse()
			{
			}

			public DBResponse( DbDataReader data )
			{
				if( data.HasRows )
				{
					Message = data["Message"].ToString();
					Validation = data["Validation"].ToString();
				}
			}
		}

		#endregion

		#region Protected Methods

		protected string CreateHashKey( string connectionString, string storedProcedure )
		{
			return storedProcedure + ":" + connectionString;
		}

		protected string CreateHashKey( int hashCode, string tableName )
		{
			return hashCode.ToString() + ":" + tableName;
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Get the spid of the process on SQL Server
		/// If it fails, just log the message
		/// </summary>
		private short GetSpid( IDbConnection connection, SqlTransaction tran = null )
		{
			short nSPID = 0;
			IDataReader dr = null;
			try
			{
				if( connection != null && connection.State == ConnectionState.Open )
				{
					m_bIsGetSpidRunning = true;
					dr = ExecuteReader( connection, "SELECT SPID = @@SPID", tran );
					m_bIsGetSpidRunning = false;
					if( !dr.IsClosed )
					{
						dr.Read();
						nSPID = dr.GetInt16( 0 );
					}
				}
			}
			catch( SqlException ex )
			{
				Log( eSeverity.Error, "GetSpid 'SELECT SPID = @@SPID' failed:\t" + ex.Message );
			}
			finally
			{
				if( dr != null && !dr.IsClosed )
					dr.Close();
			}
			return nSPID;
		}

		/// <summary>
		/// Introduced by CR from April 04, 2008 by Miroslav
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		private void InfoMessage( object sender, SqlInfoMessageEventArgs args )
		// attached to connection to retrieve print statement from stored procedure
		// also this retrieves custom raised warnings - level 0-10, e.g. raiserror ('test msg', 10, 2)
		// to retrieve custom raised exceptions - level 11-16 set FireInfoMessageEventOnUserErrors to true
		// level 17 and above will still raise exception, and will not be passed to InfoMessage 
		{
			m_SqlErrorCollection = args.Errors;

			newInfoMessage?.Invoke();
		}

		private string HidePassword( string sSource )
		{
			string sToHide = "password=";
			int p1 = sSource.ToLower().IndexOf( sToHide );
			if( p1 >= 0 )
			{
				int p2 = sSource.IndexOf( ";", p1 );
				sSource = sSource.Remove( p1 + sToHide.Length, p2 - p1 - sToHide.Length ).Insert( p1 + sToHide.Length, "********" );
			}
			return sSource;
		}

		private void FixDbComandParameters( ref IDbCommand dbCommand )
		{
			foreach( SqlParameter parameter in dbCommand.Parameters )
			{
				if( parameter.SqlDbType != SqlDbType.Structured )
				{
					continue;
				}
				string name = parameter.TypeName;
				int index = name.IndexOf( "." );
				if( index == -1 )
				{
					continue;
				}
				name = name.Substring( index + 1 );
				if( name.Contains( "." ) )
				{
					parameter.TypeName = name;
				}
			}
		}

		#endregion

		#region IDisposable Members

		protected override void Dispose( bool disposing )
		{
			lock( this )
			{
				if( !this.m_bDisposed )
				{
					try
					{
						if( disposing )
						{
							// Release the managed resources added in this derived class
							if( commandCache != null )
							{
								foreach( object oValue in commandCache.Values )
								{
									if( typeof( SqlCommand ) == oValue.GetType() )
										( (SqlCommand) oValue ).Dispose();
								}
							}
							//if( commandCache != null && commandCache.Count > 0 && command != null ) command.Dispose();
							////							if (m_StateChangeEventHandler!=null) base.StateChange -= m_StateChangeEventHandler;
							//if( connection != null ) connection.Close();
						}
						// Release the native unmanaged resources
						this.m_bDisposed = true;
					}
					finally
					{
						// Call Dispose on the base class
						base.Dispose( disposing );
					}
				}
			}
		}

		#endregion
	}

	#region Enumerations - UpdateBehavior
	/// <summary>
	/// Used with the Database.UpdateDataSet method.  Provides control over behavior when the Data
	/// Adapter's update command encounters an error.
	/// </summary>
	public enum UpdateBehavior
	{
		/// <summary>
		/// No interference with the DataAdapter's Update command.  If Update encounters
		/// an error, the update stops.  Additional rows in the Datatable are uneffected.
		/// </summary>
		Standard,
		/// <summary>
		/// If the DataAdapter's Update command encounters an error, the update will
		/// continue.  The Update command will try to update the remaining rows. 
		/// </summary>
		Continue,
		/// <summary>
		/// If the DataAdapter encounters an error, all updated rows will be rolled back
		/// </summary>
		Transactional
	}
	#endregion

	// wrapper class for describing parameters for the PrepareCommand method
	[Serializable]
	public class SqlParameterInfo
	{
	#region Public Member Variables

		public string ParameterName;
		public SqlDbType DBType;
		public int Size;
		public ParameterDirection Direction;
		public string SourceColumn;
		public object Value;

		#endregion

		#region Constructors

		public SqlParameterInfo( string parameterName,
								 SqlDbType dbType,
								 int size,
								 ParameterDirection direction,
								 string sourceColumn )
		{
			ParameterName = parameterName;
			DBType = dbType;
			Size = size;
			Direction = direction;
			SourceColumn = sourceColumn;
		}

		public SqlParameterInfo( string parameterName,
								 SqlDbType dbType,
								 int size,
								 ParameterDirection direction,
								 object value )
		{
			ParameterName = parameterName;
			DBType = dbType;
			Size = size;
			Direction = direction;
			Value = value;
		}

		public SqlParameterInfo( object value )
		{
			Value = value;
		}

		public SqlParameterInfo( SqlParameter param )
		{
			ParameterName = param.ParameterName;
			DBType = param.SqlDbType;
			Size = param.Size;
			Direction = param.Direction;
			Value = param.Value;
		}

		/*
		public SqlParameterInfo( string parameterName,
								 SqlDbType dbType,
								 int size,
								 ParameterDirection direction )
		{
			ParameterName = parameterName;
			DBType = dbType;
			Size = size;
			Direction = direction;
		}
		*/

		#endregion
	}

	public class TableTypeColumns
	{
		public string Column { get; set; }
		public string Type { get; set; }
		public string Length { get; set; }
		public string Precision { get; set; }
		public string Scale { get; set; }

		public TableTypeColumns()
		{
		}
		public TableTypeColumns( DbDataReader data )
		{
			if( data.HasRows )
			{
				Column = data["Column"].ToString();
				Type = data["Type"].ToString();
				Length = data["Length"].ToString();
				Precision = data["Precision"].ToString();
				Scale = data["Scale"].ToString();
			}
		}
    }
}
