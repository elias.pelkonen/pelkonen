using System;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace InvCommon
{
	/// <summary>
	/// Summary description for CDb class.
	/// </summary>
	[Serializable]
	public class CMySQLDB : CApp
	{
		#region Member Variables

		[Instance]
		private bool m_bDisposed = false;
		private static bool m_bIsGetSpidRunning = false;
		protected bool m_bHandleExcOnDataSetUpdate;
		[NonSerialized]
		protected Hashtable m_CommandCache = new Hashtable();
		[NonSerialized]
		protected IDbCommand m_Command;
		[NonSerialized]
		protected IDbConnection m_Connection;
		protected string m_sDbServerIniFileSection = CDefault.dbServerIniSection;
		protected int m_nDBCommandTimeout;
		protected int m_nDetailMySqlTrace;
		private string m_sConnectionString;

		private bool m_AttachMySqlInfoMessageEventHandler;
		//private bool m_FireInfoMessageEventOnUserErrors;
		[SoftInstance]
		private MySql.Data.MySqlClient.MySqlError[] m_MySqlError;

		new protected bool? m_bStateInitialized = null;

		protected delegate void m_newInfoMessageEventHandler();
		protected event m_newInfoMessageEventHandler newInfoMessage;

		#endregion

		#region Constructors/Destructors

		protected CMySQLDB()
		{
		}

		public CMySQLDB( string ApplicationName )
			: this( ApplicationName, new ListDictionary() )
		{
		}

		public CMySQLDB( CApp capp )
			: base( capp )
		{
		}

		public CMySQLDB( CApp capp, bool bPassSoftInstance )
			: base( capp, bPassSoftInstance )
		{
		}

		public CMySQLDB( string ApplicationName, CApp capp )
			: base( ApplicationName, capp )
		{
		}

		public CMySQLDB( string ApplicationName, CApp capp, bool bPassSoftInstance )
			: base( ApplicationName, capp, bPassSoftInstance )
		{
		}

		public CMySQLDB( string ApplicationName, ListDictionary AdditionalInfo )
			: base( ApplicationName, AdditionalInfo )
		{
		}

		~CMySQLDB()
		{
			Dispose( false );
		}

		#endregion

		#region Properties

		public IDbCommand LastExecutedCommand
		{
			get { AssertNotDisposed(); return m_Command; }
		}

		// Parameter prefix for stored procedures
		public char ParameterToken
		{
			get { return '@'; }
		}

		// default name for the table in the DataSet
		public string TableToken
		{
			get { return "Table"; }
		}

		public int DBCommandTimeout
		{
			get
			{
				AssertNotDisposed();
				return m_nDBCommandTimeout;
			}

			set
			{
				m_nDBCommandTimeout = value;
				Log( eSeverity.Trace, string.Format( @"DBCommandTimeout set to: {0} seconds", m_nDBCommandTimeout ) );
			}
		}
		/// <summary>
		/// </summary>
		public bool AttachMySqlInfoMessageEventHandler
		{
			get { return m_AttachMySqlInfoMessageEventHandler; }
			set
			{
				m_AttachMySqlInfoMessageEventHandler = value;
				if( m_Connection != null )
				{
					if( value )
						( (MySqlConnection) m_Connection ).InfoMessage += new MySqlInfoMessageEventHandler( InfoMessage );
					else
						( (MySqlConnection) m_Connection ).InfoMessage -= new MySqlInfoMessageEventHandler( InfoMessage );
				}
			}
		}

		/// <summary>
		/// </summary>
		public MySqlError[] GetMySqlError
		{
			get { return m_MySqlError; }
		}

		#endregion

		#region Overridden Methods

		// have to override since there is extra data
		protected override bool InitCommon()
		{
			string sErrorMsg = null;

			m_bStateChanged = base.InitCommon();

			if( m_bStateInitialized == null || m_bStateInitialized == false )
			{
				// on top of CApp specific data
				if( !ReadConfig( m_sAppName, "HandleExcOnDataSetUpdate", out m_bHandleExcOnDataSetUpdate, ref sErrorMsg ) )
					ReadConfig( "General", "HandleExcOnDataSetUpdate", out m_bHandleExcOnDataSetUpdate, false );
				// need to extract every property value from the application section first, then,
				// if it doesn't exist, get this value from general section in the INI file
				if( !ReadConfig( m_sAppName, "DBCommandTimeout", out m_nDBCommandTimeout, ref sErrorMsg ) )
					ReadConfig( "General", "DBCommandTimeout", out m_nDBCommandTimeout, 60 );

				if( !ReadConfig( m_sAppName, "DetailMySqlTrace", out m_nDetailMySqlTrace, ref sErrorMsg ) )
					ReadConfig( "General", "DetailMySqlTrace", out m_nDetailMySqlTrace, 0 );
				m_bStateInitialized = true;
			}
			return m_bStateChanged;
		}
		#endregion

		#region Virtual Methods

		public virtual IDbCommand PrepareCommand(
			CommandType commandType,
			string commandText,
			params MySqlParameterInfo[] ParameterInfo )
		{
			return PrepareCommand( m_Connection, commandType, commandText, ParameterInfo );
		}

		public virtual IDbCommand PrepareCommand(
			IDbConnection connection,
			CommandType commandType,
			string commandText,
			params MySqlParameterInfo[] ParameterInfo )
		{
			int i;
			string sHashKey;

			ArgumentValidation.CheckForEmptyString( commandText, "commandText" );
			ArgumentValidation.CheckForNullReference( connection, "connection" );

			IDbCommand command = new MySqlCommand
			{
				CommandText = commandText,
				CommandType = commandType,
				Connection = connection as MySqlConnection,
				CommandTimeout = m_nDBCommandTimeout
			};

			if( commandType != CommandType.StoredProcedure )
				return command;

			if( ParameterInfo.Length > 0 )
			{
				bool bHasParamInfo = false;
				foreach( MySqlParameterInfo parameterInfo in ParameterInfo )	// figure out if parameter info provided
				{
					if( !IsEmpty( parameterInfo.ParameterName ) )	// is it just a value or ParamInfo as well
					{
						bHasParamInfo = true;
						break;
					}
				}
				if( bHasParamInfo )
				{
					for( int j = 0; j < ParameterInfo.Length; j++ )
					{
						MySqlParameter parameter = ( (MySqlCommand) command ).Parameters.Add(
							ParameterInfo[j].ParameterName,
							ParameterInfo[j].DBType,
							ParameterInfo[j].Size,
							ParameterInfo[j].SourceColumn );

						// how about Direction.Return/Output ???? you are responsible yourself
						parameter.Direction = ParameterInfo[j].Direction;
						if( ParameterInfo[j].Value != null )
							parameter.Value = ParameterInfo[j].Value;
					}
				}
				else	// no parameter info
				{
					sHashKey = CreateHashKey( connection.ConnectionString, commandText );
					if( !m_CommandCache.ContainsKey( sHashKey ) )	// cached already
					{
						CacheCommandParameters( connection, command, commandText );
					}

					IDataParameter[] cloneParameterSet = (IDataParameter[]) m_CommandCache[sHashKey];
					foreach( IDataParameter p in cloneParameterSet )
					{
						command.Parameters.Add( (IDataParameter) ( (ICloneable) p ).Clone() );
					}

					// assign parameter values
					if( command.Parameters.Count > 0 )
					{
						//if ((m_Command.Parameters.Count - 1) != parameterInfo.Length)	// ReturnValue does not count
						//{
						//	throw new InvalidOperationException("The number of parameters does not match number of values for stored procedure.");
						//}
						i = 0;
						foreach( IDataParameter parameter in command.Parameters )
						{
							if( parameter.Direction == System.Data.ParameterDirection.Input )
							{
								if( ParameterInfo.GetUpperBound(0) < i || ParameterInfo.Length == 0 )
									throw new InvalidOperationException( "The number of parameters does not match number of values for stored procedure." );

								parameter.Value = ParameterInfo[i].Value ?? DBNull.Value;
								i++;
							}
							else if( parameter.Direction == System.Data.ParameterDirection.InputOutput
								|| parameter.Direction == System.Data.ParameterDirection.Output )
							{
								parameter.Value = DBNull.Value;
								i++;
							}
						}
					}
				}
			}

			return command;
		}

		protected virtual IDataReader DoExecuteReader(
			IDbCommand command,
			CommandBehavior cmdBehavior )
		{
			try
			{
				if( !m_bIsGetSpidRunning )
				{
					if( m_nDetailMySqlTrace == 1 )
						Log( eSeverity.Info, "\tDoExecuteReader\tSPID = " + GetSpid() + "\tMySql:", command.CommandText );
					else
						Log( eSeverity.Debug, "\tDoExecuteReader\t\tMySql:", command.CommandText );
				}
				IDataReader reader = command.ExecuteReader( cmdBehavior );
				return reader;
			}
			catch( Exception ex )
			{
				throw new CException( "DoExecuteReader failed\tMySql:" + command.CommandText, eExceptionType.Data, ex, eDetailType.Medium );
			}
		}

		protected virtual void DoExecuteDataSet(
			IDbCommand command,
			DataSet dataSet,
			string[] tableNames )
		{
			ArgumentValidation.CheckForNullReference( command, "command" );
			ArgumentValidation.CheckForNullReference( tableNames, "tableNames" );

			if( tableNames.Length == 0 )
			{
				throw new ArgumentException( "The table name array used to map results to user-specified table names cannot be empty.", "tableNames" );
			}

			for( int i = 0; i < tableNames.Length; i++ )
			{
				ArgumentValidation.CheckForNullReference( tableNames[i], string.Concat( "tableNames[", i, "]" ) );
				ArgumentValidation.CheckForEmptyString( tableNames[i], string.Concat( "tableNames[", i, "]" ) );
			}

			if( m_nDetailMySqlTrace == 1 )
				Log( eSeverity.Info, "\tDoExecuteDataSet\tSPID = " + GetSpid() + "\tMySql:", command.CommandText );
			else
				Log( eSeverity.Debug, "\tDoExecuteDataSet\t\tMySql:", command.CommandText );

			try
			{
				using( DbDataAdapter adapter = GetDataAdapter( command.Connection, null, UpdateBehavior.Standard ) )
				{
					( (IDbDataAdapter) adapter ).SelectCommand = command;

					string systemCreatedTableNameRoot = TableToken;
					for( int i = 0; i < tableNames.Length; i++ )
					{
						string systemCreatedTableName = ( i == 0 ) ? systemCreatedTableNameRoot : systemCreatedTableNameRoot + i;
						adapter.TableMappings.Add( systemCreatedTableName, tableNames[i] );
					}

					adapter.Fill( dataSet );

					// keep the command, the dataset was populated from, so that we can update it later on
					lock( m_CommandCache.SyncRoot )
					{
						foreach( string tableName in tableNames )
						{
							string hashKey = CreateHashKey( dataSet.GetHashCode(), tableName );
							if( !m_CommandCache.ContainsKey( hashKey ) )
								m_CommandCache.Add( hashKey, command );
						}
					}
				}
			}
			catch( Exception ex )
			{
				throw new CException( "DoExecuteDataSet failed\tMySql:" + command.CommandText, eExceptionType.Data, ex, eDetailType.Medium );
			}
		}

		protected virtual int DoUpdateDataSet(
			IDbConnection connection,
			DataSet dataSet,
			string tableName,
			IDbCommand insertCommand,
			IDbCommand updateCommand,
			IDbCommand deleteCommand,
			UpdateBehavior behavior )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( dataSet, "dataSet" );
			//ArgumentValidation.CheckForEmptyString(tableName, "tableName");
			//ArgumentValidation.CheckForEmptyString(selectCommandText, "selectCommandText");

			try
			{
				if( connection.State == ConnectionState.Closed )
				{
					// Data adapter reopens connection if necessary but we do it here explicitly
					// to use context_info that is set during call to OpenConnection
					connection = OpenConnection();
					if( insertCommand != null )
						insertCommand.Connection = connection;
					if( updateCommand != null )
						updateCommand.Connection = connection;
					if( deleteCommand != null )
						deleteCommand.Connection = connection;
				}

				using( MySqlDataAdapter adapter = GetDataAdapter( connection, "", behavior ) )
				{
					if( IsEmpty( tableName ) )
						tableName = dataSet.Tables[0].TableName;

					string hashKey = CreateHashKey( dataSet.GetHashCode(), tableName );
					if( m_CommandCache.Contains( hashKey ) )
					{
						adapter.SelectCommand = (MySqlCommand) m_CommandCache[hashKey];
						// create default insert/update commands
						if( insertCommand == null && updateCommand == null && deleteCommand == null )
						{
							MySqlCommandBuilder cmdBuilder = new MySqlCommandBuilder( adapter );
						}
					}

					if( insertCommand != null )
					{
						( (IDbDataAdapter) adapter ).InsertCommand = insertCommand;
						if( m_nDetailMySqlTrace == 1 )
							Log( eSeverity.Info, "\tDoUpdateDataSet\tSPID = " + GetSpid(), "MySql insert:  " + insertCommand.CommandText );
						else
							Log( eSeverity.Debug, "\tDoUpdateDataSet\t", "MySql insert:  " + insertCommand.CommandText );
					}
					if( updateCommand != null )
					{
						( (IDbDataAdapter) adapter ).UpdateCommand = updateCommand;
						if( m_nDetailMySqlTrace == 1 )
							Log( eSeverity.Info, "\tDoUpdateDataSet\tSPID = " + GetSpid(), "MySql update:  " + updateCommand.CommandText );
						else
							Log( eSeverity.Debug, "\tDoUpdateDataSet\t", "MySql update:  " + updateCommand.CommandText );
					}
					if( deleteCommand != null )
					{
						( (IDbDataAdapter) adapter ).DeleteCommand = deleteCommand;
						if( m_nDetailMySqlTrace == 1 )
							Log( eSeverity.Info, "\tDoUpdateDataSet\tSPID = " + GetSpid(), "MySql delete:  " + deleteCommand.CommandText );
						else
							Log( eSeverity.Debug, "\tDoUpdateDataSet\t", "MySql delete:  " + deleteCommand.CommandText );
					}

					try
					{
						DateTime startTime = DateTime.Now;
						int rows = adapter.Update( dataSet.Tables[tableName] );
						//int rows = adapter.Update(dataSet, tableName);
						Log( eSeverity.Debug, "\tTable updated : ", tableName );
						return rows;
					}
					catch( Exception ex )
					{
						Log( eSeverity.Error, "Update or Insert failed : ", tableName );
						throw ex;				// will be cought at higher level...
					}
				}
			}
			catch( Exception ex )
			{
				if( insertCommand == null &&
					updateCommand == null &&
					deleteCommand == null )
				{
					throw new CException( "DoUpdateDataSet failed\r\nMySql Error: " + ex.Message,
						eExceptionType.Data, ex, eDetailType.Medium );
				}
				else
				{
					throw new CException( "DoUpdateDataSet failed" +
						"\r\nMySql insert:  " +
							( ( insertCommand == null ) ? "" : insertCommand.CommandText ) +
						"\r\nMySql update:  " +
							( ( updateCommand == null ) ? "" : updateCommand.CommandText ) +
						"\r\nMySql delete:  " +
							( ( deleteCommand == null ) ? "" : deleteCommand.CommandText ) +
						"\r\nMySql Error: " +
							ex.Message,
						eExceptionType.Data, ex, eDetailType.Medium );
				}
			}
			finally
			{
				CloseConnection();
			}
		}

		protected virtual MySqlDataAdapter GetDataAdapter(
			IDbConnection connection,
			string selectCommandText,
			UpdateBehavior updateBehavior )
		{
			//string queryStringToBeFilledInLater = String.Empty;
			MySqlDataAdapter adapter = new MySqlDataAdapter( selectCommandText ?? String.Empty, (MySqlConnection) connection );

			/// if UpdateBehavior.Continue then skip failed record and continue update
			if( updateBehavior == UpdateBehavior.Continue )
			{
				adapter.RowUpdated += new MySqlRowUpdatedEventHandler( OnMySqlRowUpdated );
			}
			return adapter;
		}

		protected virtual void OnMySqlRowUpdated(
			object sender,
			MySqlRowUpdatedEventArgs rowThatCouldNotBeWritten )
		{
			// Listens for the RowUpdate event on a dataadapter to support UpdateBehavior.Continue
			if( rowThatCouldNotBeWritten.RecordsAffected == 0 )
			{
				if( rowThatCouldNotBeWritten.Errors != null )
				{
					StringBuilder ColumnList = new StringBuilder( "Failed to update row. Values (" );
					foreach( object field in rowThatCouldNotBeWritten.Row.ItemArray )
					{
						ColumnList.Append( " '" + field.ToString() + "', " );
					}
					ColumnList.Replace( ",", ")", ColumnList.Length - 2, 1 );

					rowThatCouldNotBeWritten.Row.RowError = ColumnList.ToString();
					rowThatCouldNotBeWritten.Status = UpdateStatus.SkipCurrentRow;

					if( m_bHandleExcOnDataSetUpdate )
					{
						if( ExceptionPoolSize == 0 ) ExceptionPoolSize = 1000;
						CException cex = new CException( ColumnList.ToString(), eExceptionType.Data, rowThatCouldNotBeWritten.Errors );
						HandleException( cex );
					}
				}
			}
		}

		/// <summary> 
		/// Function sets CONTEXT_INFO for a given connection.
		/// Information contains the logon user and the host name.
		/// To retrieve this information from CONTEXT_INFO call dbo.fn_sysGetUserAndHostFromContextInfo()
		/// </summary> 
		protected virtual void SetContext( IDbConnection Connection )
		{
			try
			{
				string sContextInfo = "DECLARE @ContextInfo VARBINARY (128); SELECT @ContextInfo = dbo.fn_sysFormatLogonUserAndHostForContext_Info('" + LogonUser + "', '" + UserHostName + "'); SET CONTEXT_INFO @ContextInfo;";
				ExecuteNonQuery( Connection, sContextInfo );
			}
			catch
			{
				try
				{
					// if no format function defined try explicit format (less reliable)
					string sContextInfo = "DECLARE @ContextInfo VARBINARY (128); SELECT @ContextInfo = CONVERT (VARBINARY (128) ,'LogonUser=' + CAST ('" + LogonUser + "' AS VARCHAR (50)) + ';UserHostName=' + CAST ('" + UserHostName + "' AS VARCHAR (50)) + REPLICATE (' ',128)); SET CONTEXT_INFO @ContextInfo;";
					ExecuteNonQuery( Connection, sContextInfo );
				}
				catch
				{
					// we don't care - context is not critical
				}
			}
		}

		protected virtual string CacheCommandParameters( IDbConnection connection, IDbCommand command, string commandText )
		{
			string sHashKey = CreateHashKey( connection.ConnectionString, commandText );

			// need to clone connection & command not to affect open transactions
			MySqlConnection tempConnection = (MySqlConnection) ( (ICloneable) connection ).Clone();
			MySqlCommand tempCommand = new MySqlCommand
			{
				CommandText = commandText,
				CommandType = CommandType.StoredProcedure
			};
			try
			{
				tempConnection.Open();
				tempCommand.Connection = tempConnection;
				MySqlCommandBuilder.DeriveParameters( tempCommand );	// round trip to MySql server
			}
			finally
			{
				if( tempConnection != null ) tempConnection.Close();
			}

			int i = 0;
			// what if Parameters == null ???
			IDataParameter[] cloneParameterSet = new IDataParameter[tempCommand.Parameters.Count];
			foreach( IDataParameter parameter in tempCommand.Parameters )
			{
				IDataParameter cloneParameter = (IDataParameter) ( (ICloneable) parameter ).Clone();
				cloneParameterSet[i++] = cloneParameter;
			}
			lock( m_CommandCache.SyncRoot )
			{
				if( !m_CommandCache.ContainsKey( sHashKey ) )
					m_CommandCache.Add( sHashKey, cloneParameterSet );
			}

			return sHashKey;
		}

		#endregion

		#region Public Connection Wrappers

		public IDbConnection OpenConnection()
		{
			AssertNotDisposed();
			m_sConnectionString = GetConnectionString( m_sDbServerIniFileSection );
			if( m_Connection != null && m_Connection.State == ConnectionState.Open && m_Connection.ConnectionString == m_sConnectionString )
				return m_Connection;

			m_Connection = new MySqlConnection( m_sConnectionString );
			try
			{
				if( m_AttachMySqlInfoMessageEventHandler )
					( (MySqlConnection) m_Connection ).InfoMessage += new MySqlInfoMessageEventHandler( InfoMessage );

				m_Connection.Open();

				if( m_nDetailMySqlTrace == 1 )
					Log( eSeverity.Info, "Connection opened\tSPID = " + GetSpid() + "\t", HidePassword( m_sConnectionString ) );
				else
					Log( eSeverity.Debug, "Connection opened\t\t", HidePassword( m_sConnectionString ) );

				//SetContext(m_Connection);
			}
			catch( Exception ex )
			{
				Log( eSeverity.Error, "Connection failed : ", HidePassword( m_sConnectionString ) + "\r\nException: " + ex.Message );
				throw new CException( "Connection failed : " + m_sConnectionString + "\r\nException: " + ex.Message, eExceptionType.Config );
			}
			return m_Connection;
		}

		public IDbConnection OpenConnection( string sIniFileSection )
		{
			if( m_sDbServerIniFileSection != sIniFileSection )
				m_sConnectionString = "";
			m_sDbServerIniFileSection = sIniFileSection;
			return OpenConnection();
		}

		public void SwitchConnection( string sIniFileSection )
		{
			string sTempDbServerIniFileSection = m_sDbServerIniFileSection;

			m_sConnectionString = "";

			try
			{
				CloseConnection();
			}
			catch
			{
				throw;
			}
			m_sDbServerIniFileSection = sIniFileSection;
			try
			{
				OpenConnection();
			}
			catch( MySqlException MySqle )
			{
				m_sDbServerIniFileSection = sTempDbServerIniFileSection;
				throw MySqle;
			}
		}

		public void CloseConnection()
		{
			AssertNotDisposed();
			try
			{
				if( m_Connection != null && m_Connection.State == ConnectionState.Open )
				{
					if( m_nDetailMySqlTrace == 1 )
						Log( eSeverity.Info, "Connection closed\tSPID = " + GetSpid() + "\t", HidePassword( m_Connection.ConnectionString ) );
					else
						Log( eSeverity.Debug, "Connection closed\t\t", HidePassword( m_Connection.ConnectionString ) );

					m_Connection.Close();
				}
			}
			catch
			{
				Log( eSeverity.Error, "CloseConnection() failed : ", HidePassword( m_Connection.ConnectionString ) );
				throw;
			}
		}

		public string GetConnectionString( string IniFileSection )
		{
			string sDriver = null;
			string sPort = "3306";  // default port for MySQL
			string sUserID = null;
			string sPassword = null;
			string sInitialCatalog = null;
			//string sProvider = null;
			bool bUserPresent = false;

			if( m_sConnectionString != null && m_sConnectionString != "" )
			{
				return m_sConnectionString;
			}

			ArgumentValidation.CheckForEmptyString( IniFileSection, "IniFileSection" );
			if( ReadConfig( IniFileSection, "DBServerName", out string sMySqlServerName ) )
			{
				ReadConfig( IniFileSection, "DBDriver", out sDriver );
				ReadConfig( IniFileSection, "DBPort", out sPort, sPort );
				ReadConfig( IniFileSection, "DBInitCatalog", out sInitialCatalog );
				bUserPresent = ReadConfig( IniFileSection, "DBUserName", out sUserID, false );
				if( bUserPresent == true && ( sUserID.CompareTo( "False" ) == 0 ) | ( sUserID.CompareTo( "" ) == 0 ) ) bUserPresent = false;
				if( bUserPresent ) ReadConfig( IniFileSection, "DBPassword*", out sPassword );
			}
			return String.Format( "server={0};user id={1}; password={2}; database={3}; pooling=true",
				sMySqlServerName, sUserID, sPassword, sInitialCatalog );
		}

		#endregion

		#region Public ExecuteNonQuery Wrappers

		public int ExecuteNonQuery( IDbCommand command )	// because command contains connection
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( command, "command" );
			if( m_nDetailMySqlTrace == 1 )
				Log( eSeverity.Info, "\tExecuteNonQuery\tSPID = " + GetSpid() + "\tMySql:", command.CommandText );
			else
				Log( eSeverity.Debug, "\tExecuteNonQuery\t\tMySql:", command.CommandText );
			m_Command = command;
			return m_Command.ExecuteNonQuery();
		}

		// simply execute MySql statement
		public int ExecuteNonQuery( string commandText )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForEmptyString( commandText, "commandText" );
			if( m_nDetailMySqlTrace == 1 )
				Log( eSeverity.Info, "\tExecuteNonQuery\tSPID = " + GetSpid() + "\tMySql:", commandText );
			else
				Log( eSeverity.Debug, "\tExecuteNonQuery\t\tMySql:", commandText );
			m_Command = PrepareCommand( m_Connection, CommandType.Text, commandText );
			return m_Command.ExecuteNonQuery();
		}

		// simply execute MySql statement on a separate connection
		public int ExecuteNonQuery(
			IDbConnection connection,
			string commandText )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( commandText, "commandText" );
			if( m_nDetailMySqlTrace == 1 )
				Log( eSeverity.Info, "\tExecuteNonQuery\tSPID = " + GetSpid() + "\tMySql:", commandText );
			else
				Log( eSeverity.Debug, "\tExecuteNonQuery\t\tMySql:", commandText );
			m_Command = PrepareCommand( connection, CommandType.Text, commandText );
			return m_Command.ExecuteNonQuery();
		}

		// stored procedure
		public int ExecuteNonQuery(
			string storedProcedureName,
			params object[] parameterValues )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			MySqlParameterInfo[] Parameters = null;
			if( parameterValues.Length > 0 )
			{
				Parameters = new MySqlParameterInfo[parameterValues.Length];
				for( int i = 0; i < parameterValues.Length; i++ )
				{
					Parameters[i] = new MySqlParameterInfo( parameterValues[i] );	// just values, no info
				}
			}
			string sParams = " ";
			foreach( object Param in parameterValues )
				sParams += Param == null ? "''" : "'" + Param.ToString() + "',";
			sParams = sParams.Substring( 0, sParams.Length - 2 );
			if( m_nDetailMySqlTrace == 1 )
				Log( eSeverity.Info, "\tExecuteNonQuery\tSPID = " + GetSpid() + "\tMySql:", storedProcedureName + sParams );
			else
				Log( eSeverity.Debug, "\tExecuteNonQuery\t\tMySql:", storedProcedureName + sParams );
			m_Command = PrepareCommand( m_Connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
			return m_Command.ExecuteNonQuery();
		}

		// stored procedure on a separate connection
		public int ExecuteNonQuery(
			IDbConnection connection,
			string storedProcedureName,
			params object[] parameterValues )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			MySqlParameterInfo[] Parameters = null;
			if( parameterValues.Length > 0 )
			{
				Parameters = new MySqlParameterInfo[parameterValues.Length];
				for( int i = 0; i < parameterValues.Length; i++ )
				{
					Parameters[i] = new MySqlParameterInfo( parameterValues[i] );	// just values, no info
				}
			}
			string sParams = " ";
			foreach( object Param in parameterValues )
				sParams += Param == null ? "''" : "'" + Param.ToString() + "',";
			sParams = sParams.Substring( 0, sParams.Length - 2 );
			if( m_nDetailMySqlTrace == 1 )
				Log( eSeverity.Info, "\tExecuteNonQuery\tSPID = " + GetSpid() + "\tMySql:", storedProcedureName + sParams );
			else
				Log( eSeverity.Debug, "\tExecuteNonQuery\t\tMySql:", storedProcedureName + sParams );
			m_Command = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
			return m_Command.ExecuteNonQuery();
		}

		#endregion

		#region Public ExecuteReader Wrappers

		public IDataReader ExecuteReader( IDbCommand command )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( command, "command" );
			m_Command = command;
			return DoExecuteReader( command, CommandBehavior.Default );
		}

		public IDataReader ExecuteReader( IDbCommand command, CommandBehavior cmdBehavior )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( command, "command" );
			m_Command = command;
			return DoExecuteReader( command, cmdBehavior );
		}

		// simply execute a query, the command is not cached
		public IDataReader ExecuteReader( string commandText )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForEmptyString( commandText, "commandText" );
			m_Command = PrepareCommand( m_Connection, CommandType.Text, commandText );
			return DoExecuteReader( m_Command, CommandBehavior.Default );
		}

		// simply execute a query, the command is not cached
		public IDataReader ExecuteReader( string commandText, CommandBehavior cmdBehavior )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForEmptyString( commandText, "commandText" );
			m_Command = PrepareCommand( m_Connection, CommandType.Text, commandText );
			return DoExecuteReader( m_Command, cmdBehavior );
		}

		// simply execute a query on a separate connection, the command is not cached
		public IDataReader ExecuteReader(
			IDbConnection connection,
			string commandText )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( commandText, "commandText" );
			m_Command = PrepareCommand( connection, CommandType.Text, commandText );
			return DoExecuteReader( m_Command, CommandBehavior.Default );
		}

		// stored procedure 
		public IDataReader ExecuteReader(
			string storedProcedureName,
			params object[] parameterValues )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			MySqlParameterInfo[] Parameters = null;
			if( parameterValues.Length > 0 )
			{
				Parameters = new MySqlParameterInfo[parameterValues.Length];
				for( int i = 0; i < parameterValues.Length; i++ )
				{
					Parameters[i] = new MySqlParameterInfo( parameterValues[i] );	// just values, no info
				}
			}
			m_Command = PrepareCommand( m_Connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
			return DoExecuteReader( m_Command, CommandBehavior.Default );
		}

		// stored procedure 
		public IDataReader ExecuteReader(
			string storedProcedureName,
			params MySqlParameterInfo[] parameterInfo )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			MySqlParameterInfo[] Parameters = null;
			if( parameterInfo.Length > 0 )
			{
				Parameters = new MySqlParameterInfo[parameterInfo.Length];
				for( int i = 0; i < parameterInfo.Length; i++ )
				{
					Parameters[i] = parameterInfo[i];	// just values, no info
				}
			}
			m_Command = PrepareCommand( m_Connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
			return DoExecuteReader( m_Command, CommandBehavior.Default );
		}

		// stored procedure 
		public IDataReader ExecuteReader(
			IDbConnection connection,
			string storedProcedureName,
			params object[] parameterValues )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			MySqlParameterInfo[] Parameters = null;
			if( parameterValues.Length > 0 )
			{
				Parameters = new MySqlParameterInfo[parameterValues.Length];
				for( int i = 0; i < parameterValues.Length; i++ )
				{
					Parameters[i] = new MySqlParameterInfo( parameterValues[i] );	// just values, no info
				}
			}
			m_Command = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
			return DoExecuteReader( m_Command, CommandBehavior.Default );
		}

		public IDataReader ExecuteReader(
			IDbConnection connection,
			string storedProcedureName,
			params MySqlParameterInfo[] parameterValues )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			MySqlParameterInfo[] Parameters = null;
			if( parameterValues.Length > 0 )
			{
				Parameters = new MySqlParameterInfo[parameterValues.Length];
				for( int i = 0; i < parameterValues.Length; i++ )
				{
					Parameters[i] = parameterValues[i];	// just values, no info
				}
			}
			m_Command = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
			return DoExecuteReader( m_Command, CommandBehavior.Default );
		}

		#endregion

		#region Public ExecuteDataSet Wrappers

		public DataSet ExecuteDataSet( IDbCommand command )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( command, "command" );
			DataSet dataSet = new DataSet
			{
				Locale = CultureInfo.InvariantCulture
			};

			DoExecuteDataSet( command, dataSet, new string[] { TableToken } );
			return dataSet;
		}

		// simply execute MySql query
		public DataSet ExecuteDataSet( string commandText )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForEmptyString( commandText, "commandText" );
			IDbCommand oCommand = PrepareCommand( m_Connection, CommandType.Text, commandText );
			DataSet dataSet = new DataSet
			{
				Locale = CultureInfo.InvariantCulture
			};
			DoExecuteDataSet( oCommand, dataSet, new string[] { TableToken } );
			return dataSet;
		}

		// simply execute MySql query on a separate connection
		public DataSet ExecuteDataSet(
			IDbConnection connection,
			string commandText )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( commandText, "commandText" );
			IDbCommand oCommand = PrepareCommand( connection, CommandType.Text, commandText );
			DataSet dataSet = new DataSet
			{
				Locale = CultureInfo.InvariantCulture
			};
			DoExecuteDataSet( oCommand, dataSet, new string[] { TableToken } );
			return dataSet;
		}

		// stored procedure
		public DataSet ExecuteDataSet(
			string storedProcedureName,
			params object[] parameterValues )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			MySqlParameterInfo[] Parameters = null;
			if( parameterValues.Length > 0 )
			{
				Parameters = new MySqlParameterInfo[parameterValues.Length];
				for( int i = 0; i < parameterValues.Length; i++ )
				{
					Parameters[i] = new MySqlParameterInfo( parameterValues[i] );	// just values, no info
				}
			}
			IDbCommand oCommand = PrepareCommand( m_Connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
			DataSet dataSet = new DataSet
			{
				Locale = CultureInfo.InvariantCulture
			};
			DoExecuteDataSet( oCommand, dataSet, new string[] { TableToken } );
			return dataSet;
		}

		// stored procedure on a separate connection
		public DataSet ExecuteDataSet(
			IDbConnection connection,
			string storedProcedureName,
			params object[] parameterValues )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			MySqlParameterInfo[] Parameters = null;
			if( parameterValues.Length > 0 )
			{
				Parameters = new MySqlParameterInfo[parameterValues.Length];
				for( int i = 0; i < parameterValues.Length; i++ )
				{
					Parameters[i] = new MySqlParameterInfo( parameterValues[i] );	// just values, no info
				}
			}
			IDbCommand oCommand = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
			DataSet dataSet = new DataSet
			{
				Locale = CultureInfo.InvariantCulture
			};
			DoExecuteDataSet( oCommand, dataSet, new string[] { TableToken } );
			return dataSet;
		}

		#endregion

		#region Public UpdateDataSet Wrappers

		public int UpdateDataSet(
			DataSet dataSet,
			UpdateBehavior behavior )
		{
			return DoUpdateDataSet( m_Connection, dataSet, null, null, null, null, behavior );
		}

		public int UpdateDataSet(
			IDbConnection connection,
			DataSet dataSet,
			UpdateBehavior behavior )
		{
			return DoUpdateDataSet( connection, dataSet, null, null, null, null, behavior );
		}

		public int UpdateDataSet(
			DataSet dataSet,
			string tableName,
			UpdateBehavior behavior )
		{
			return DoUpdateDataSet( m_Connection, dataSet, tableName, null, null, null, behavior );
		}

		public int UpdateDataSet(
			IDbConnection connection,
			DataSet dataSet,
			string tableName,
			UpdateBehavior behavior )
		{
			return DoUpdateDataSet( connection, dataSet, tableName, null, null, null, behavior );
		}

		public int UpdateDataSet(
			DataSet dataSet,
			string tableName,
			IDbCommand insertCommand,
			IDbCommand updateCommand,
			IDbCommand deleteCommand,
			UpdateBehavior behavior )
		{
			if( insertCommand == null && updateCommand == null && deleteCommand == null )
			{
				throw new ArgumentException( "At least one command must be initialized." );
			}

#pragma warning disable IDE0031 // Use null propagation
			IDbConnection connection = insertCommand != null
				? insertCommand.Connection : updateCommand != null
				? updateCommand.Connection : deleteCommand != null
				? deleteCommand.Connection : null;
#pragma warning restore IDE0031 // Use null propagation

			return DoUpdateDataSet( connection, dataSet, tableName, insertCommand, updateCommand, deleteCommand, behavior );
		}

		#endregion

		#region Public ExecuteScalar Wrappers

		public object ExecuteScalar( IDbCommand command )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( command, "command" );
			if( m_nDetailMySqlTrace == 1 )
				Log( eSeverity.Info, "\tExecuteScalar\tSPID = " + GetSpid() + "\tMySql:", command.CommandText );
			else
				Log( eSeverity.Debug, "\tExecuteScalar\t\tMySql:", command.CommandText );
			m_Command = command;
			return m_Command.ExecuteScalar();
		}

		public object ExecuteScalar( string commandText )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForEmptyString( commandText, "commandText" );
			if( m_nDetailMySqlTrace == 1 )
				Log( eSeverity.Info, "\tExecuteScalar\tSPID = " + GetSpid() + "\tMySql:", commandText );
			else
				Log( eSeverity.Debug, "\tExecuteScalar\t\tMySql:", commandText );
			m_Command = PrepareCommand( m_Connection, CommandType.Text, commandText );
			return m_Command.ExecuteScalar();
		}

		public object ExecuteScalar(
			string storedProcedureName,
			params object[] parameterValues )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			MySqlParameterInfo[] Parameters = null;
			if( parameterValues.Length > 0 )
			{
				Parameters = new MySqlParameterInfo[parameterValues.Length];
				for( int i = 0; i < parameterValues.Length; i++ )
				{
					Parameters[i] = new MySqlParameterInfo( parameterValues[i] );	// just values, no info
				}
			}
			string sParams = " ";
			foreach( object Param in parameterValues )
				sParams += Param == null ? "''" : "'" + Param.ToString() + "',";
			sParams = sParams.Substring( 0, sParams.Length - 2 );
			if( m_nDetailMySqlTrace == 1 )
				Log( eSeverity.Info, "\tExecuteScalar\tSPID = " + GetSpid() + "\tMySql:", storedProcedureName + sParams );
			else
				Log( eSeverity.Debug, "\tExecuteScalar\t\tMySql:", storedProcedureName + sParams );
			m_Command = PrepareCommand( m_Connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
			return m_Command.ExecuteScalar();
		}

		public object ExecuteScalar(
			IDbConnection connection,
			string storedProcedureName,
			params object[] parameterValues )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			MySqlParameterInfo[] Parameters = null;
			if( parameterValues.Length > 0 )
			{
				Parameters = new MySqlParameterInfo[parameterValues.Length];
				for( int i = 0; i < parameterValues.Length; i++ )
				{
					Parameters[i] = new MySqlParameterInfo( parameterValues[i] );	// just values, no info
				}
			}
			string sParams = " ";
			foreach( object Param in parameterValues )
				sParams += Param == null ? "''" : "'" + Param.ToString() + "',";
			sParams = sParams.Substring( 0, sParams.Length - 2 );
			if( m_nDetailMySqlTrace == 1 )
				Log( eSeverity.Info, "\tExecuteScalar\tSPID = " + GetSpid() + "\tMySql:", storedProcedureName + sParams );
			else
				Log( eSeverity.Debug, "\tExecuteScalar\t\tMySql:", storedProcedureName + sParams );
			m_Command = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
			return m_Command.ExecuteScalar();
		}

		#endregion

		#region Public Wrappers with Return Parameters

		// ExecuteNonQuery
		public int ExecuteNonQuery(
			string storedProcedureName,
			out IDataParameterCollection parameterReturn,
			params MySqlParameterInfo[] parameterInfo )
		{
			return ExecuteNonQuery( m_Connection, storedProcedureName, out parameterReturn, parameterInfo );
		}
		public int ExecuteNonQuery(
			IDbConnection connection,
			string storedProcedureName,
			out IDataParameterCollection parameterReturn,
			params MySqlParameterInfo[] parameterInfo )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			MySqlParameterInfo[] Parameters = null;
			if( parameterInfo.Length > 0 )
			{
				Parameters = new MySqlParameterInfo[parameterInfo.Length];
				for( int i = 0; i < parameterInfo.Length; i++ )
				{
					Parameters[i] = parameterInfo[i];	// values, and info
				}
			}
			string sParams = " ";
			foreach( MySqlParameterInfo Param in parameterInfo )
				sParams += Param.Value == null ? "null," : Param.Value.ToString() + ",";
			if( m_nDetailMySqlTrace == 1 )
				Log( eSeverity.Info, "\tExecuteNonQuery\tSPID = " + GetSpid() + "\tMySql:", storedProcedureName + sParams );
			else
				Log( eSeverity.Debug, "\tExecuteNonQuery\t\tMySql:", storedProcedureName + sParams );
			IDbCommand oCommand = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );

			int nResult = oCommand.ExecuteNonQuery();
			parameterReturn = oCommand.Parameters;
			return nResult;
		}

		// ExecuteDataSet
		public DataSet ExecuteDataSet(
			string storedProcedureName,
			out IDataParameterCollection parameterReturn,
			params MySqlParameterInfo[] parameterInfo )
		{
			return ExecuteDataSet( m_Connection, storedProcedureName, out parameterReturn, parameterInfo );
		}
		public DataSet ExecuteDataSet(
			IDbConnection connection,
			string storedProcedureName,
			out IDataParameterCollection parameterReturn,
			params MySqlParameterInfo[] parameterInfo )
		{
			AssertNotDisposed();
			ArgumentValidation.CheckForNullReference( connection, "connection" );
			ArgumentValidation.CheckForEmptyString( storedProcedureName, "storedProcedureName" );
			MySqlParameterInfo[] Parameters = null;
			if( parameterInfo.Length > 0 )
			{
				Parameters = new MySqlParameterInfo[parameterInfo.Length];
				for( int i = 0; i < parameterInfo.Length; i++ )
				{
					Parameters[i] = parameterInfo[i];	// just values, no info
				}
			}
			IDbCommand oCommand = PrepareCommand( connection, CommandType.StoredProcedure, storedProcedureName, Parameters );
			DataSet dataSet = new DataSet
			{
				Locale = CultureInfo.InvariantCulture
			};
			DoExecuteDataSet( oCommand, dataSet, new string[] { TableToken } );
			parameterReturn = oCommand.Parameters;
			return dataSet;
		}
		#endregion

		#region Protected Methods

		protected string CreateHashKey( string connectionString, string storedProcedure )
		{
			return storedProcedure + ":" + connectionString;
		}

		protected string CreateHashKey( int hashCode, string tableName )
		{
			return hashCode.ToString() + ":" + tableName;
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Get the spid of the process on MySql Server
		/// If it failes, just log the message
		/// </summary>
		private short GetSpid()
		{
			short nSPID = 0;
			IDataReader dr = null;
			try
			{
				if( m_Connection != null && m_Connection.State == ConnectionState.Open )
				{
					m_bIsGetSpidRunning = true;
					dr = ExecuteReader( "SELECT SPID = @@SPID", CommandBehavior.SingleRow );
					m_bIsGetSpidRunning = false;
					if( !dr.IsClosed )
					{
						dr.Read();
						nSPID = dr.GetInt16( 0 );
					}
				}
			}
			catch( Exception ex )
			{
				Log( eSeverity.Error, "GetSpid 'SELECT SPID = @@SPID' failed:\t" + ex.Message );
			}
			finally
			{
				if( dr != null && !dr.IsClosed )
					dr.Close();
			}
			return nSPID;
		}

		/// <summary>
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		private void InfoMessage( object sender, MySql.Data.MySqlClient.MySqlInfoMessageEventArgs args )
		// attached to connection to retrieve print statement from stored procedure
		// also this retrieves custom raised warnings - level 0-10, e.g. raiserror ('test msg', 10, 2)
		// to retrieve custom raised exceptions - level 11-16 set FireInfoMessageEventOnUserErrors to true
		// level 17 and above will still raise exception, and will not be passed to InfoMessage 
		{
			m_MySqlError = args.errors;

			newInfoMessage?.Invoke();
		}

		private string HidePassword( string sSource )
		{
			string sToHide = "password=";
			int p1 = sSource.ToLower().IndexOf( sToHide );
			if( p1 >= 0 )
			{
				int p2 = sSource.IndexOf( ";", p1 );
				sSource = sSource.Remove( p1 + sToHide.Length, p2 - p1 - sToHide.Length ).Insert( p1 + sToHide.Length, "********" );
			}
			return sSource;
		}

		#endregion

		#region IDisposable Members

		protected override void Dispose( bool disposing )
		{
			lock( this )
			{
				if( !this.m_bDisposed )
				{
					try
					{
						if( disposing )
						{
							// Release the managed resources added in this derived class
							if( m_CommandCache != null )
							{
								foreach( object oValue in m_CommandCache.Values )
								{
									if( typeof( MySqlCommand ) == oValue.GetType() )
										( (MySqlCommand) oValue ).Dispose();
								}
							}
							if( m_CommandCache != null && m_CommandCache.Count > 0 && m_Command != null ) m_Command.Dispose();
							//							if (m_StateChangeEventHandler!=null) base.StateChange -= m_StateChangeEventHandler;
							if( m_Connection != null ) m_Connection.Close();
						}
						// Release the native unmanaged resources
						this.m_bDisposed = true;
					}
					finally
					{
						// Call Dispose on the base class
						base.Dispose( disposing );
					}
				}
			}
		}

		#endregion
	}

	// wrapper class for describing parameters for the PrepareCommand method
	[Serializable]
	public class MySqlParameterInfo
	{
		#region Public Member Variables

		public string ParameterName;
		public MySqlDbType DBType;
		public int Size;
		public ParameterDirection Direction;
		public string SourceColumn;
		public object Value;

		#endregion

		#region Constructors

		public MySqlParameterInfo( string parameterName,
								 MySqlDbType dbType,
								 int size,
								 ParameterDirection direction,
								 string sourceColumn )
		{
			ParameterName = parameterName;
			DBType = dbType;
			Size = size;
			Direction = direction;
			SourceColumn = sourceColumn;
		}

		public MySqlParameterInfo( string parameterName,
								 MySqlDbType dbType,
								 int size,
								 ParameterDirection direction,
								 object value )
		{
			ParameterName = parameterName;
			DBType = dbType;
			Size = size;
			Direction = direction;
			Value = value;
		}

		public MySqlParameterInfo( object value )
		{
			Value = value;
		}

		public MySqlParameterInfo( MySqlParameter param )
		{
			ParameterName = param.ParameterName;
			DBType = param.MySqlDbType;
			Size = param.Size;
			Direction = param.Direction;
			Value = param.Value;
		}

		/*
		public MySqlParameterInfo( string parameterName,
								 MySqlDbType dbType,
								 int size,
								 ParameterDirection direction )
		{
			ParameterName = parameterName;
			DBType = dbType;
			Size = size;
			Direction = direction;
		}
		*/

		#endregion
	}
}
