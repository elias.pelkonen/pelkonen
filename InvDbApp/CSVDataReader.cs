﻿using System;

using System.IO;
using System.Data;
using System.Linq;
using System.Text;

using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace InvCommon.CSV
{
    /// <summary>
    /// Implementation of IDataReader for CSV files
    /// </summary>
    public class CSVDataReader : IDataReader, IDisposable, IDataRecord
    {
        private static readonly char[] EndOfLineChars = new char[] { (char)10, (char)13 };
        private static readonly char[] CellDelimiters = new char[] { ',' };

        public string Name { get; private set; } = "Unnamed CSVDataReader";


        public List<string> Headings { get; private set; } = new List<string>();
        public bool IsClosed { get; private set; } = false;
        public bool IsDisposed { get; private set; } = false;

        /// <summary>Parse a boolean value from a string, replace this function to suit the expected true/false pattern in the input file</summary>
        public Func<string, bool> BooleanParser { get; set; } = new Func<string, bool>(s => s.ToLower().StartsWith("t") || s.ToLower().StartsWith("y"));

        public int FieldCount => Headings.Count;
        public int Depth => 0;
        public int RecordsAffected => 0;

        public object this[int i] => GetValue(i);
        public object this[string name] => GetValue(GetOrdinal(name));

        private string[] data;
        private string[] rowData;
        private bool headingsRead = false;
        public int currentRow = 0;

        public static CSVDataReader FromFile(string path)
        {
            return FromFile(path, Encoding.Default);
        }

        public static CSVDataReader FromFile(string path, Encoding encoding)
        {
            return FromText((new FileInfo(path)).Name, File.ReadAllText(path, encoding));
        }

        public static CSVDataReader FromText(string name, string text)
        {
            CSVDataReader reader = new CSVDataReader();
            reader.data = ParseCSVFile(text);
            reader.Name = name;

            return reader;
        }

        public void Close()
        {
            IsClosed = true;
        }

        public void Dispose()
        {
            IsDisposed = true;
        }

        public IDataReader GetData(int i)
        {
            return this;
        }

        public string GetDataTypeName(int i)
        {
            return "string";
        }

        public int GetOrdinal(string name)
        {
            if (Headings.Contains(name))
            {
                return Headings.IndexOf(name);
            }
            else
            {
                throw new IndexOutOfRangeException();
            }
        }

        private static string[] ParseCSVFile(string text)
        {
            return ParseCSV(text, EndOfLineChars, true);
        }

        private static string[] ParseCSVRow(string text)
        {
            return ParseCSV(text, CellDelimiters);
        }

        private static string[] ParseCSV(string text, char[] delimiters, bool ignoreBlanks = false)
        {
            bool inQuote = false;

            List<int> splits = new List<int>();

            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] == '"')
                {
                    inQuote = !inQuote;
                }
                else if (!inQuote && delimiters.Contains(text[i]))
                {
                    splits.Add(i);
                }
            }

            int cellStart = -1;
            List<string> cells = new List<string>();

            for (int i = 0; i < splits.Count; i++)
            {
                string cell = text.Substring(cellStart + 1, splits[i] - cellStart - 1).Replace("\"\"", "\"");
                if(!ignoreBlanks || !string.IsNullOrWhiteSpace(cell))
                {
                    cells.Add(cell);
                }
                cellStart = splits[i];
            }
            
            string lastCell = text.Substring(cellStart + 1);
            if (!ignoreBlanks || !string.IsNullOrWhiteSpace(lastCell))
            {
                cells.Add(lastCell);
            }

            return cells.ToArray();
        }

        private void ReadHeadings()
        {
            if (!headingsRead)
            {
                Headings = ParseCSVRow(data[0]).ToList();
                headingsRead = true;
            }
        }

        public bool Read()
        {
            ReadHeadings();
            currentRow++;

            if (data.GetUpperBound(0) < currentRow)
            {
                rowData = null;
                return false;
            }

            rowData = ParseCSVRow(data[currentRow]);
            return true;
        }

        public string GetString(int i)
        {
            if (rowData.GetUpperBound(0) >= i)
            {
                return rowData[i];
            }
            else
            {
                throw new IndexOutOfRangeException();
            }
        }

        public bool GetBoolean(int i)
        {
            return BooleanParser.Invoke(GetString(i));
        }

        public byte GetByte(int i)
        {
            return byte.Parse(GetString(i));
        }

        public char GetChar(int i)
        {
            return GetString(i)[0];
        }

        public DateTime GetDateTime(int i)
        {
            throw new NotImplementedException();
        }

        public decimal GetDecimal(int i)
        {
            return decimal.Parse(GetString(i));
        }

        public double GetDouble(int i)
        {
            return double.Parse(GetString(i));
        }

        public Type GetFieldType(int i)
        {
            return typeof(string);
        }

        public float GetFloat(int i)
        {
            return float.Parse(GetString(i));
        }

        public Guid GetGuid(int i)
        {
            return new Guid(GetString(i));
        }

        public short GetInt16(int i)
        {
            return short.Parse(GetString(i));
        }

        public int GetInt32(int i)
        {
            return int.Parse(GetString(i));
        }

        public long GetInt64(int i)
        {
            return long.Parse(GetString(i));
        }

        public string GetName(int i)
        {
            return Headings.Count > i ? Headings[i] : null;
        }

        public object GetValue(int i)
        {
            return GetString(i);
        }

        public bool IsDBNull(int i)
        {
            return rowData.GetUpperBound(0) > i;
        }

        public bool NextResult()
        {
            return false;
        }

        public void AssertHeadingExists(string headerName)
        {
            ReadHeadings();
            if (!Headings.Contains(headerName))
            {
                throw new Exception($"Expected CSV heading '{headerName}' does not exist ({Name})");
            }
        }

        /// <summary>
        /// Performs a thorough check against the headings, requires all headings match exactly and no extras are present.  Case sensitive.
        /// </summary>
        /// <param name="expectedHeadings">A string list of expected headings</param>
        public void AssertHeadingsMatch(List<string> expectedHeadings)
        {
            List<Exception> exceptions = new List<Exception>();
            ReadHeadings();

            if (expectedHeadings.Count != Headings.Count)
            {
                exceptions.Add(new Exception($"Heading count mismatch, {expectedHeadings.Count} expected {Headings.Count} found in {Name}"));
            }

            foreach (string expectedHeading in expectedHeadings)
            {
                if (!Headings.Contains(expectedHeading))
                {
                    exceptions.Add(new Exception($"Expected CSV heading '{expectedHeading}' does not exist ({Name})"));
                }
            }

            foreach (string heading in Headings)
            {
                if (!expectedHeadings.Contains(heading))
                {
                    exceptions.Add(new Exception($"CSV heading '{heading}' was not expected in ({Name})"));
                }
            }

            if (exceptions.Count > 0)
            {
                throw new AggregateException($"CSV heading mismatch in {Name}", exceptions);
            }
        }

        public override string ToString()
        {
            return Name;
        }

        public DataTable GetSchemaTable()
        {
            ReadHeadings();

            DataTable table = new DataTable();

            foreach (string column in Headings)
            {
                table.Columns.Add(column);
            }

            return table;
        }

        [Obsolete("This method is not implemented.")]
        public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        [Obsolete("This method is not implemented.")]
        public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public int GetValues(object[] values)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Converts the current row to a dictionary of objects by string column name
        /// </summary>
        /// <param name="reader">the reader to convert</param>
        /// <returns>Dictionary with a key of column name, value of the cell value as an object</returns>
        public Dictionary<string, object> ToDictionary()
        {
            return Headings.ToDictionary(h => h, h => (object)GetString(GetOrdinal(h)));
        }
    }
}
