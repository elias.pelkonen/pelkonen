using System;
using System.Text;
using System.Data;
//using System.Data.MySQL;
using MySql.Data.MySqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace InvCommon
{
    /// <summary>
    /// Summary description for CDb class.
    /// </summary>
    [Serializable]
    public class CMySQLDB : CApp
    {

        private IDbConnection m_Connection = null;
        private IDbCommand m_Command = null;
        private string m_sConnectionString = null;
        private string m_sDbServerIniFileSection = null;
        private bool m_AttachMySQLInfoMessageEventHandler = false;
        private int m_nDetailMySQLTrace = 0;

        public CMySQLDB(string ApplicationName)
			: this( ApplicationName, new ListDictionary() )
		{
		}

        public CMySQLDB(string ApplicationName, ListDictionary AdditionalInfo)
			: base( ApplicationName, AdditionalInfo )
		{
		}

        public IDbConnection OpenConnection()
        {
            string sPort = "3306";
            string sServer = "localhost";
            string sDatabase = "InvisorDB";
            string sUser = "root";
            string sPassword = "2Rycho1407";
            string ConnectionString = String.Format("server={0};user id={1}; password={2}; database={3}; pooling=false",
                sServer, sUser, sPassword, sDatabase);
            IDbConnection oConn = null;
            try
            {
                using (oConn = new MySql.Data.MySqlClient.MySqlConnection(ConnectionString))
                {
                    oConn.Open();
                }
            }

            catch (Exception ex)
            {
                Log(eSeverity.Error, ex.Message + "\r\n" + "Connection failed : ", m_sConnectionString);
            }
            return oConn;
        }

        //public IDbConnection OpenConnection()
        //{
        //    AssertNotDisposed();
        //    m_sConnectionString = GetConnectionString(m_sDbServerIniFileSection);
        //    if (m_Connection != null && m_Connection.State == ConnectionState.Open && m_Connection.ConnectionString == m_sConnectionString)
        //        return m_Connection;

        //    //m_Connection = new MySQLConnection(m_sConnectionString);
        //    try
        //    {
        //        using (IDbConnection oConn = new MySQLConnection(m_sConnectionString))
        //        {
        //            if (m_AttachMySQLInfoMessageEventHandler)
        //                ((MySQLConnection)m_Connection).InfoMessage += new MySQLInfoMessageEventHandler(InfoMessage);
        //            oConn.Open();
        //            m_Connection = oConn;
        //            if (m_nDetailMySQLTrace == 1)
        //                Log(eSeverity.Info, "Connection opened\tSPID = " + "\t", m_sConnectionString);
        //            else
        //                Log(eSeverity.Debug, "Connection opened\t\t", m_sConnectionString);
        //        }
        //    }
        //    catch
        //    {
        //        Log(eSeverity.Error, "Connection failed : ", m_sConnectionString);
        //        throw;
        //    }
        //    return m_Connection;
        //}

        //public IDbConnection OpenConnection(string sIniFileSection)
        //{
        //    if (m_sDbServerIniFileSection != sIniFileSection)
        //        m_sConnectionString = "";
        //    m_sDbServerIniFileSection = sIniFileSection;
        //    return OpenConnection();
        //}

        public void CloseConnection()
        {
            AssertNotDisposed();
            try
            {
                if (m_Connection != null && m_Connection.State == ConnectionState.Open)
                {
                    if (m_nDetailMySQLTrace == 1)
                        Log(eSeverity.Info, "Connection closed\tSPID = " + "\t", m_Connection.ConnectionString);
                    else
                        Log(eSeverity.Debug, "Connection closed\t\t", m_Connection.ConnectionString);

                    m_Connection.Close();
                }
            }
            catch
            {
                Log(eSeverity.Error, "CloseConnection() failed : ", m_Connection.ConnectionString);
                throw;
            }
        }

        public string GetConnectionString(string IniFileSection)
        {
            bool bPersistSecurityInfo = false;
            //string sApplicationName;
            string sDriver = null;
            string sMySQLServerName = null;
            string sPort = "3306";  // default port for MySQL
            string sUserID = null;
            string sPassword = null;
            string sInitialCatalog = null;
            //string sProvider = null;
            bool bUserPresent = false;

            if (m_sConnectionString != null && m_sConnectionString != "")
            {
                return m_sConnectionString;
            }

            //sApplicationName = StateInfo["ApplicationName"].ToString();

            ArgumentValidation.CheckForEmptyString(IniFileSection, "IniFileSection");
            if (ReadConfig(IniFileSection, "DBServerName", out sMySQLServerName))
            {
                ReadConfig(IniFileSection, "DBDriver", out sDriver);
                ReadConfig(IniFileSection, "DBPort", out sPort, sPort);
                ReadConfig(IniFileSection, "DBInitCatalog", out sInitialCatalog);
                bUserPresent = ReadConfig(IniFileSection, "DBUserName", out sUserID, false);
                if (bUserPresent == true && (sUserID.CompareTo("False") == 0) | (sUserID.CompareTo("") == 0)) bUserPresent = false;
                if (bUserPresent) ReadConfig(IniFileSection, "DBPassword*", out sPassword);
            }
            return String.Format("Provider=MSDASQL;Driver={0};Port={1};Server={2};Database={3};UID={4};Password={5};Option=3", sDriver, sPort, sMySQLServerName, sInitialCatalog, sUserID, sPassword);
        }

        // simply execute a query, the command is not cached
        public IDataReader ExecuteReader(string commandText)
        {
            AssertNotDisposed();
            ArgumentValidation.CheckForEmptyString(commandText, "commandText");
            //m_Command = PrepareCommand(m_Connection, CommandType.Text, commandText);
            //return DoExecuteReader(m_Command, CommandBehavior.Default);
            return null;
        }


        // wrapper class for describing parameters for the PrepareCommand method
        [Serializable]
        public class MySQLParameterInfo
        {
            #region Public Member Variables

            public string ParameterName;
            public MySqlDbType DBType;
            public int Size;
            public ParameterDirection Direction;
            public string SourceColumn;
            public object Value;

            #endregion

            #region Constructors

            public MySQLParameterInfo(string parameterName,
                                     MySqlDbType dbType,
                                     int size,
                                     ParameterDirection direction,
                                     string sourceColumn)
            {
                ParameterName = parameterName;
                DBType = dbType;
                Size = size;
                Direction = direction;
                SourceColumn = sourceColumn;
            }

            public MySQLParameterInfo(string parameterName,
                                     MySqlDbType dbType,
                                     int size,
                                     ParameterDirection direction,
                                     object value)
            {
                ParameterName = parameterName;
                DBType = dbType;
                Size = size;
                Direction = direction;
                Value = value;
            }

            public MySQLParameterInfo(object value)
            {
                Value = value;
            }

            public MySQLParameterInfo(MySql.Data.MySqlClient.MySqlParameter param)
            {
                ParameterName = param.ParameterName;
                DBType = param.MySqlDbType;
                Size = param.Size;
                Direction = param.Direction;
                Value = param.Value;
            }

            /*
            public MySQLParameterInfo( string parameterName,
                                     MySQLDbType dbType,
                                     int size,
                                     ParameterDirection direction )
            {
                ParameterName = parameterName;
                DBType = dbType;
                Size = size;
                Direction = direction;
            }
            */

            #endregion
        }
    }
}