﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvisorEmailSchedular
{
    #region Email Message Queue Services

    public enum MailMessageStatus
    {
        [Description("New")]
        New = 1,

        [Description("Processing")]
        Processing = 2,

        [Description("Sent")]
        Sent = 3,

        [Description("Failed")]
        Failed = 4
    }

    public class MailMessagePriority
    {
        private static Dictionary<int, MailMessagePriority> all;

        public static readonly MailMessagePriority Low = new MailMessagePriority("Low", 1, System.Net.Mail.MailPriority.Low);
        public static readonly MailMessagePriority Normal = new MailMessagePriority("Normal", 2, System.Net.Mail.MailPriority.Normal);
        public static readonly MailMessagePriority High = new MailMessagePriority("High", 3, System.Net.Mail.MailPriority.High);

        public string Name { get; private set; }
        public int ID { get; private set; }
        public System.Net.Mail.MailPriority NativePriority { get; private set; }

        static MailMessagePriority()
        {
            init();
        }

        private static void init()
        {
            all = new Dictionary<int, MailMessagePriority>();
            all.Add(Low.ID, Low);
            all.Add(Normal.ID, Normal);
            all.Add(High.ID, High);
        }

        private MailMessagePriority(string name, int ID, System.Net.Mail.MailPriority nativePriority)
        {
            Name = name;
            this.ID = ID;
            NativePriority = nativePriority;
        }

        public static MailMessagePriority GetByID(int ID)
        {
            if (all == null)
            {
                init();
            }

            return all.ContainsKey(ID) ? all[ID] : null;
        }

    }

    public class MailMessage
    {
        public int ID { get; set; }
        public int TenantID { get; set; }
        public string Subject { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
        public string Body { get; set; }
        public byte[] Attachments { get; set; }
        public MailMessageStatus Status { get; set; }
        public string StatusText => Status.ToString();
        public string Errors { get; set; }
        public string Warnings { get; set; }
        public bool IsHTML { get; set; }
        public MailMessagePriority Priority { get; set; }
        public string PriorityText => Priority?.Name ?? string.Empty;

        public MailMessage()
        {
        }

        public MailMessage(DbDataReader data)
        {
            ID = data.GetInt32(data.GetOrdinal("ID"));
            TenantID = data.GetInt32(data.GetOrdinal("TenantID"));
            Subject = data["Subject"].ToString();
            From = data["From"].ToString();
            To = data["To"].ToString();
            CC = data["CC"].ToString();
            BCC = data["BCC"].ToString();
            Body = data["Body"].ToString();

            int attachmentsOrdinal = data.GetOrdinal("Attachments");
            Attachments = data.IsDBNull(attachmentsOrdinal) ? default(byte[]) : (byte[])data.GetValue(attachmentsOrdinal);

            Status = (MailMessageStatus)data.GetInt32(data.GetOrdinal("MailMessageStatusID"));
            Errors = data["Errors"].ToString();
            Warnings = data["Warnings"].ToString();
            IsHTML = data.GetBoolean(data.GetOrdinal("IsHTML"));
            Priority = MailMessagePriority.GetByID(data.GetInt32(data.GetOrdinal("PriorityID")));
        }
    }
    #endregion
}

