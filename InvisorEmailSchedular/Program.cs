﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvCommon;

namespace InvisorEmailSchedular
{
    class Program
    {
        private static readonly string _AppName = "Invisor Task to send automated email";
        private static CDBApp _app = new CDBApp(_AppName);

        static int Main(string[] args)
        {
            // return args.Length == 0 || !int.TryParse(args[0], out int TenantID) ? 1 : SendAllPendingMessages(TenantID);
            return SendAllPendingMessages();            
        }

        public static int SendAllPendingMessages()
        {
            try
            {
                object[] spParameters = new object[] { 0 };

                List<MailMessage> mailMessages = _app.ReadListFromDB<MailMessage>("dbo.up_MailMessage_Create_Select", spParameters, out string message);

                if (mailMessages == null || mailMessages.Count == 0)
                {
                    message = "No new unsent mail messages were found";
                    _app.Log("\r\n" + message);
                }
                else
                {
                    message = "Total unsent mail messages found: " + mailMessages.Count;
                    _app.Log("\r\n" + message);
                    foreach (MailMessage mailMessage in mailMessages.Where(m => m.Priority == MailMessagePriority.High))
                    {
                        TakeAndSendMailMessage(mailMessage.ID);
                    }

                    foreach (MailMessage mailMessage in mailMessages.Where(m => m.Priority == MailMessagePriority.Normal))
                    {
                        TakeAndSendMailMessage(mailMessage.ID);
                    }

                    foreach (MailMessage mailMessage in mailMessages.Where(m => m.Priority == MailMessagePriority.Low))
                    {
                        TakeAndSendMailMessage(mailMessage.ID);
                    }
                }
                return 0;
            }
            catch (Exception ex)
            {
                _app.HandleException(ex, false);
                _app.Log(eSeverity.Debug, "Invisor Task to send automated email processed with errors: SendAllPendingMessages");
                return 1;
            }        
        }

        private static void TakeAndSendMailMessage(int ID)
        {
            StringBuilder messageBuilder = new StringBuilder();

            MailMessage mailMessage = _app.ReadRecordFromDB<MailMessage>("dbo.up_MailMessage_Take", new object[] { ID, null, null }, out string takeMessage);
            if (!string.IsNullOrWhiteSpace(takeMessage))
            {
                messageBuilder.Append($"While taking message ID {ID}, {mailMessage}");
            }

            SendMailMessageAndComplete(mailMessage, messageBuilder);
            _app.Log("\r\n" + messageBuilder.ToString());
        }


        private static void SendMailMessageAndComplete(MailMessage mailMessage, StringBuilder messageBuilder)
        {
            StringBuilder errorsBuilder = new StringBuilder();
            StringBuilder warningsBuilder = new StringBuilder();

            string attachements = null;

            if (mailMessage.Attachments != null && mailMessage.Attachments.Length > 0)
            {
                attachements = Encoding.ASCII.GetString(mailMessage.Attachments, 0, mailMessage.Attachments.Length);
            }

            string mailServerName = CDefault.defaultSMTPServer;
            string getMailServerSettingErrorMessage = string.Empty;

            if (!_app.ReadConfig("ClientMail", "MailServerName", out mailServerName, ref getMailServerSettingErrorMessage))
            {
                throw new CException(getMailServerSettingErrorMessage, eExceptionType.Config, eDetailType.Brief);
            }

            bool exceptionWhileSending = false;           

            try
            {
                _app.SendMail(
                    mailMessage.From,
                    mailMessage.To,
                    mailMessage.CC,
                    mailMessage.BCC,
                    mailMessage.Subject,
                    mailMessage.Body,
                    attachements,
                    mailServerName,
                    mailMessage.IsHTML,
                    mailMessage.Priority.NativePriority);
            }
            catch (Exception ex)
            {
                exceptionWhileSending = true;
                messageBuilder.Append($"While sending email {mailMessage.ID}, {ex.Message}");
                _app.Log(eSeverity.Error, ex.Message);
                errorsBuilder.AppendLine(ex.ToString());
            }

            object newTennantID = null;
            object newSubject = null;
            object newFrom = null;
            object newTo = null;
            object newCC = null;
            object newBCC = null;
            object newBody = null;
            object newAttachments = null;
            MailMessageStatus newMailMessageStatusID = exceptionWhileSending ? MailMessageStatus.Failed : MailMessageStatus.Sent;
            string newErrors = errorsBuilder.ToString();
            object newWarnings = warningsBuilder.ToString();
            object newIsHTML = null;
            object newPriority = null;

            object[] spParameters = new object[] {
                mailMessage.ID,
                newTennantID,
                newSubject,
                newFrom,
                newTo,
                newCC,
                newBCC,
                newBody,
                newAttachments,
                newMailMessageStatusID,
                newErrors,
                newWarnings,
                newIsHTML,
                newPriority
            };

            string saveStatusVaildation = string.Empty;
            string saveStatusMessage = string.Empty;

            bool status = _app.SaveDataToDB("dbo.up_MailMessage_Synch", spParameters, ref saveStatusMessage, ref saveStatusVaildation);

            if (!string.IsNullOrWhiteSpace(saveStatusMessage))
            {
                messageBuilder.Append($"While marking message ID {mailMessage.ID} as sent, {saveStatusMessage}");
            }

            if (!string.IsNullOrWhiteSpace(saveStatusVaildation))
            {
                messageBuilder.Append($"While marking message ID {mailMessage.ID} as sent, {saveStatusVaildation}");
            }
        }
    }
}
