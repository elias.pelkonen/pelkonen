﻿using System;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace InvCommon
{
	public static class CStringExtension
	{
		public static string StripNumberFormat( this string number )
		{
			return string.IsNullOrWhiteSpace( number ) ? null : number.Replace( "$", "" ).Replace( ",", "" );
		}

		public static object StripNumberFormat4DB( this string number )
		{
			return string.IsNullOrWhiteSpace( number ) ? (object) DBNull.Value : number.Replace( "$", "" ).Replace( ",", "" );
		}

		public static object StripWhiteSpace4DB( this string str )
		{
			return string.IsNullOrWhiteSpace( str ) ? (object) DBNull.Value : str;
		}

		public static bool ToBool( this string value )
		{
			string[] trueStrings = { "1", "y", "yes", "true" };
			string[] falseStrings = { "0", "n", "no", "false" };

			if( string.IsNullOrWhiteSpace( value ) )
				return false;
			if( trueStrings.Contains( value, StringComparer.OrdinalIgnoreCase ) )
				return true;
			if( falseStrings.Contains( value, StringComparer.OrdinalIgnoreCase ) )
				return false;
			throw new InvalidCastException( "Failed converting string: '" + value + "' to boolean." );
		}

		public static bool? ToBoolN( this string value )
		{
			string[] trueStrings = { "1", "y", "yes", "true" };
			string[] falseStrings = { "0", "n", "no", "false" };

			if( string.IsNullOrWhiteSpace( value ) )
				return null;
			if( trueStrings.Contains( value, StringComparer.OrdinalIgnoreCase ) )
				return true;
			if( falseStrings.Contains( value, StringComparer.OrdinalIgnoreCase ) )
				return false;
			throw new InvalidCastException( "Failed converting string: '" + value + "' to boolean." );
		}

		public static DateTime ToDateTime( this string value )
		{
			return DateTime.TryParse( value, out DateTime dt ) ? dt : default( DateTime );
		}

		public static object ToDBDateTime( this string value, bool nowIfNoDate = false )
		{
			if( string.IsNullOrWhiteSpace( value ) )
			{
				if( nowIfNoDate )
					return DateTime.Now.ToString();
				else
					return DBNull.Value;
			}
			else
				return DateTime.TryParse( value, out DateTime dt ) ? dt.ToString() : (object) DBNull.Value;
		}

		public static bool IsNumeric( this string value )
		{
			return float.TryParse( value, out float f );
		}

		public static string FormatIfNumeric( this string value )
		{
			if( value != null && value.IsNumeric() )
			{
				int dec = value.IndexOf( "." );
				var n = dec == -1 ? 0 : value.Length - dec - 1;
				value = decimal.Parse( value ).ToString( "N" + n.ToString() );
			}
			return value;
		}

		public static string NullIfEmpty( this string value )
		{
			return string.IsNullOrWhiteSpace( value ) ? null : value;
		}

		public static bool IsNullOrEmpty( this string value )
		{
			return string.IsNullOrWhiteSpace( value );
		}

        public static string GetMD5Hash(this string text)
        {
            MD5 md5 = MD5.Create();
            byte[] hash = md5.ComputeHash(Encoding.ASCII.GetBytes(text));

            StringBuilder hashString = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                hashString.Append(hash[i].ToString("X2"));
            }

            return hashString.ToString();
        }

        /// <summary>
        /// Retrieves a substring from this instance.  The substring starts after [start] and ends before [end]
        /// </summary>
        /// <param name="startText"></param>
        /// <param name="endText"></param>
        /// <returns></returns>
        public static string Substring(this string text, string startText, string endText)
        {
            if(text.Contains(startText) && text.Contains(endText))
            {
                int startIndex = text.IndexOf(startText) + startText.Length;
                int endIndex = text.IndexOf(endText, startIndex);
                return text.Substring(startIndex, endIndex - startIndex);
            }
            else
            {
                return string.Empty;
            }
        }


        public static string Truncate(this string text, int length)
        {
            return text.Length < length ? text : text.Substring(0, length);
        }
    }
}
