﻿using System.Linq;
using System.Collections.Generic;

namespace InvCommon.DiagnosticAnalytics
{
    public class TrackedValueGroup<T> : ITrackedValueGroup
    {
        public static List<ITrackedValueGroup> All { get; private set; } = new List<ITrackedValueGroup>();

        public string Name { get; private set; }

        public List<TrackedValue<T>> Values { get; private set; } = new List<TrackedValue<T>>();
        public List<T> AllUniqueValues => Values.SelectMany(v => v.AllUniqueValues).Distinct().ToList();

        public int Cardinality => AllUniqueValues.Count;

        public int Longest
        {
            get
            {
                if (typeof(T) == typeof(string))
                {
                    return AllUniqueValues.Max(v => v.ToString().Length);
                }
                else
                {
                    return 0;
                }
            }
        }

        public int Shortest
        {
            get
            {
                if (typeof(T) == typeof(string))
                {
                    return AllUniqueValues.Min(v => v.ToString().Length);
                }
                else
                {
                    return 0;
                }
            }
        }

        public TrackedValueGroup(string name)
        {
            Name = name;
            All.Add(this);
        }

        public void Add(TrackedValue<T> value)
        {
            Values.Add(value);
        }
    }
}
