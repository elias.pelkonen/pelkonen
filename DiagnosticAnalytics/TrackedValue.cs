﻿using System.Linq;
using System.Collections.Generic;

namespace InvCommon.DiagnosticAnalytics
{
    public class TrackedValue<T>
    {
        private T currentValue;
        public List<T> allValues = new List<T>();
        public List<T> AllUniqueValues => allValues.Distinct().ToList();
        public TrackedValueGroup<T> Group { get; private set; }

        public T Value
        {
            set
            {
                allValues.Add(value);
                currentValue = value;
            }
            get
            {
                return currentValue;
            }
        }

        public TrackedValue(TrackedValueGroup<T> group)
        {
            Group = group;
            Group.Add(this);
        }

        public override string ToString()
        {
            return currentValue.ToString();
        }
    }
}
