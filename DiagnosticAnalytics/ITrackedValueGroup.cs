﻿namespace InvCommon.DiagnosticAnalytics
{
    public interface ITrackedValueGroup
    {
        string Name { get; }
    }
}
